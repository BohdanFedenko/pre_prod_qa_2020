package com.epam.test.HomeWork4;

public class Main {
    public static void main(String[] args) {
        //Task1
        System.out.println(WarmupTwo.getNStringConcat("Hi",2));

        //Task2
        System.out.println(WarmupTwo.getNFrontStringConcat("Chocolate", 2));

        //Task3
        System.out.println(WarmupTwo.countXX("xxx"));

        //Task4
        System.out.println(WarmupTwo.isDoubleX("axxbb"));

        //Task5
        System.out.println(WarmupTwo.getStringDelEachSecondChars("Heeololeo"));

        //Task6
        System.out.println(WarmupTwo.getStringSplosion("Code"));

        //Task7
        System.out.println(WarmupTwo.countReapTwoSameChars("axxxaaxx") );

        //Task8
        System.out.println(WarmupTwo.countAmountNineInArray(new int [] {1,2,3,9,9,8,9}));

        //Task9
        System.out.println(WarmupTwo.isNineInFrontArray(new int[]{1, 2, 3, 9, 5}));

        //Task10
        System.out.println(WarmupTwo.isNumbersOneOrTwoOrThreeInArray(new int[]{1, 1, 2, 3, 1}));

        //Task11
        System.out.println(WarmupTwo.compareString("abc","abc"));

        //Task12
        System.out.println(WarmupTwo.removeXFromString("xxHxix"));

        //Task13
        System.out.println(WarmupTwo.removeIndexPairs("kitten"));

        //Task14
        System.out.println(WarmupTwo.removeYakFromString("pakyak"));

        //Task15
        System.out.println(WarmupTwo.countAmountPair6(new int [] {6,7,2,6}));

        //Task16
        System.out.println(WarmupTwo.isArrayNotContainTriples(new int[]{1, 1, 2, 2, 1}));

        //Task17
        System.out.println(WarmupTwo.isTemplate271InArray(new int [] {1,2,7,1}));
    }
}
