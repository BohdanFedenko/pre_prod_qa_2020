package com.epam.test.HomeWork4;

import java.util.Arrays;

public class WarmupTwo {
    //Task1
    /**Given a string and a non-negative int n, return a larger
     * string that is n copies of the original string
     * */
    public static String getNStringConcat(String str, int n) {
        String newStr="";
        for(int i=0;i<n;i++){
            newStr+=str;
        }
        return newStr;
    }

    //Task2
    /**Given a string and a non-negative int n, we'll say that
     * the front of the string is the first 3 chars, or
     * whatever is there if the string is less than length 3.
     * Return n copies of the front;
     * */
    public static String getNFrontStringConcat(String str, int n) {
        int length=str.length();
        String newStr="";
        if(length>=3){
            str=str.substring(0,3);
        }
        for(int i=0; i<n; i++){
            newStr+=str;
        }
        return newStr;
    }

    //Task3
    /**Count the number of "xx" in the given string. We'll say
     * that overlapping is allowed, so "xxx" contains 2 "xx".
     */
    public static int countXX(String str) {
        int pairCount=0;
        int indexFirstX=0;
        if(str.length()<2){
            return pairCount;
        }
        for(int i=0;i<str.length();i++){
            if (str.charAt(i) == 'x') {
                if (i == indexFirstX+1) {
                    pairCount++;
                    indexFirstX = i;
                } else{
                    indexFirstX = i;
                }
            }
        }
        return pairCount;
    }

    //Task4
    /**Given a string, return true if the first
     * instance of "x" in the string is immediately
     * followed by another "x".*/
    public static boolean isDoubleX(String str) {
        int nextXIndex=str.indexOf('x');
        if(nextXIndex==str.length()-1){
            return false;
        }else {
            if (str.charAt(nextXIndex + 1) == 'x') {
                return true;
            } else {
                return false;
            }
        }
    }

    //Task5
    /** Given a string, return a new string made of
     * every other char starting with the first, so "Hello" yields "Hlo".
     * */
    public static String getStringDelEachSecondChars(String str) {
        String newString="";
        for (int i = 0; i <str.length() ; i+=2) {
            newString+=str.charAt(i);
        }
        return newString;
    }

    //Task6
    /**Given a non-empty string like "Code" return a string like "CCoCodCode".
     * */
    public static String getStringSplosion(String str) {
        String newStrin="";
        for (int i = 0; i <str.length(); i++) {
            if(i<str.length()-1) {
                newStrin += str.substring(i, i + 1) + str.substring(0, i + 1);
            }else{
                newStrin+=str.substring(i,i+1);
            }
        }
        return newStrin;
    }

    //Task7
    /**
     Given a string, return the count of the number of times that a substring
     length 2 appears in the string and also as the last 2 chars of the string,
     so "hixxxhi" yields 1 (we won't count the end substring).*/
    public static int countReapTwoSameChars(String str) {
        if(str.length()<3){
            return 0;
        }
        String last2Substring=str.substring(str.length()-2);
        int substringCounter=0;
        for (int i = 0; i <str.length()-2 ; i++) {
            if(str.substring(i,i+2).equals(last2Substring)){
                substringCounter++;
            }
        }
        return substringCounter;
    }

    //Task8
    /**Given an array of ints, return the number of 9's in the array.
     * */
    public static int countAmountNineInArray(int[] nums) {
        int amountOfNine=0;
        for (int i = 0; i <nums.length; i++) {
            if(nums[i]==9){
                amountOfNine++;
            }
        }
        return amountOfNine;
    }

    //Task9
    /** Given an array of ints, return true if one of the first 4
     * elements in the array is a 9. The array length may be less than 4.
     * */
    public static boolean isNineInFrontArray(int[] nums) {
        int iterationLimit=4;
        boolean isNineInFourRange=false;
        if(nums.length<4){
            iterationLimit=nums.length;
        }
        for (int i = 0; i <iterationLimit; i++) {
            if (nums[i] == 9) {
                isNineInFourRange = true;
            }
        }
        return isNineInFourRange;
    }

    //Task10
    /**Given an array of ints, return true if the sequence
     * of numbers 1, 2, 3 appears in the array somewhere.
     * */
    public static boolean isNumbersOneOrTwoOrThreeInArray(int[] nums) {
        Arrays.sort(nums);
        int isOne = Arrays.binarySearch(nums,1);
        int isTwo = Arrays.binarySearch(nums,2);
        int isThree = Arrays.binarySearch(nums,3);
        if(isOne>=0 && isTwo>=0 && isThree>=0){
            return true;
        }
        return false;
    }


    //Task11
    /**Given 2 strings, a and b, return the number
     *  of the positions where they contain the same
     *  length 2 substring. So "xxcaazz" and "xxbaaz"
     *  yields 3, since the "xx", "aa", and "az" substrings
     *  appear in the same place in both strings.
     *  */
    public static int compareString(String a, String b) {
        int countPosition=0;
        int endIteration=0;
        if(a.length()>b.length()){
            endIteration=b.length();
        }else{
            endIteration=a.length();
        }
        for (int i = 0; i <endIteration-1 ; i++) {
            if(a.substring(i,i+2).equals(b.substring(i,i+2))){
                countPosition++;
            }
        }
        return countPosition;
    }

    //Task12
    /**Given a string, return a version where
     * all the "x" have been removed. Except an "x"
     * at the very start or end should not be removed.
     */
    public static String removeXFromString(String str) {
        if(str.length()<=1){
            return str;
        }
        String newString=str.substring(0,1);
        for(int i=1; i<str.length()-1; i++){
            if(str.charAt(i)!='x'){
                newString+=str.charAt(i);
            }
        }
        newString+=str.substring(str.length()-1);
        return newString;
    }

    //Task13
    /**Given a string, return a string made of the chars
     * at indexes 0,1, 4,5, 8,9 ... so "kittens" yields "kien".
     */
    public static String removeIndexPairs(String str) {
        String newString="";
        for (int i = 0; i <str.length(); i+=4) {
            newString+=str.charAt(i);
            if(i<str.length()-1){
                newString+=str.charAt(i+1);
            }
        }
        return newString;
    }

    //Task14
    /**Suppose the string "yak" is unlucky. Given a string,
     * return a version where all the "yak" are removed,
     * but the "a" can be any char.
     * The "yak" strings will not overlap.
     * */
    public static String removeYakFromString(String str) {
        for (int i = 0; i <str.length()-2; i++) {
            if(str.substring(i,i+3).equals("yak")){
                str=str.replace(str.substring(i,i+3),"");
            }
        }
        return str;
    }

    //Task15
    /**Given an array of ints, return the number of times that two
     * 6's are next to each other in the array. Also count instances
     * where the second "6" is actually a 7.
     * */
    public static int countAmountPair6(int[] nums) {
        int countNumber=0;
        for (int i = 0; i <nums.length-1 ; i++) {
            if(nums[i]==6 && (nums[i+1]==6 || nums[i+1]==7)){
                countNumber++;
            }
        }
        return countNumber;
    }

    //Task16
    /**Given an array of ints, we'll say that a triple
     * is a value appearing 3 times in a row in the array.
     * Return true if the array does not contain any triples.
     * */
    public static boolean isArrayNotContainTriples(int[] nums) {
        boolean isTriple=true;
        for (int i = 0; i < nums.length-2; i++) {
            if(nums[i]==nums[i+1] && nums[i]==nums[i+2]){
                isTriple=false;
            }
        }
        return isTriple;
    }

    //Task17
    /**Given an array of ints, return true if it contains
     * a 2, 7, 1 pattern: a value, followed by the value plus 5,
     * followed by the value minus 1.
     * Additionally the 271 counts even if the "1" differs by 2
     * or less from the correct value.
     */
    public static boolean isTemplate271InArray(int[] nums) {
        boolean isTemplate271 = false;
        for (int i = 0; i < nums.length-2; i++) {
            int value = nums[i];
            if(nums[i+1] == value + 5 && Math.abs(nums[i+2] - (value-1)) <=2){
                isTemplate271 = true;
            }
        }
        return isTemplate271;
    }
}
