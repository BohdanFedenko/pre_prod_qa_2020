package com.epam.test.HomeWork22.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class OlxHomePage {

    private static final String URL = "https://www.olx.ua/";

    @FindBy(xpath = "//a[@id = 'headerLogo']")
    private SelenideElement header;

    @FindBy(xpath = "//input[@id = 'headerSearch']")
    private SelenideElement searchField;

    @FindBy(xpath = "//input[@id = 'submit-searchmain']")
    private SelenideElement searchButton;

    @FindBy(xpath = "//a[@id = 'postNewAdLink']")
    private SelenideElement putOfferButton;

    @FindBy(xpath = "//a[@class='x-normal']")
    private SelenideElement langSwitchButton;

    @FindBy(xpath = "//div[@class = 'maincategories']//h3")
    private SelenideElement categoryHeader;

    @FindBy(xpath = "//a[contains(@href, 'favorites/search')]")
    private SelenideElement wishListIcon;

    @FindBy(xpath = "//a[@id = 'topLoginLink']")
    private SelenideElement profileButton;

    public boolean isHeader() {
        return header.isDisplayed();
    }

    public OlxResultSearchPage search(String product) {
        searchField.clear();
        searchField.val(product);
        searchButton.click();
        return page(OlxResultSearchPage.class);
    }

    public OlxRegistrationPage putOffer() {
        putOfferButton.click();
        return page(OlxRegistrationPage.class);
    }

    public OlxHomePage switchLang() {
        langSwitchButton.click();
        return page(this);
    }

    public String getCategoryHeader() {
        return categoryHeader.getText();
    }

    public OlxWishListPage openWishListPage() {
        wishListIcon.click();
        return page(OlxWishListPage.class);
    }

    public OlxRegistrationPage opemProfile() {
        profileButton.click();
        return page(OlxRegistrationPage.class);
    }

    public void open() {
        Selenide.open(URL);
    }
}
