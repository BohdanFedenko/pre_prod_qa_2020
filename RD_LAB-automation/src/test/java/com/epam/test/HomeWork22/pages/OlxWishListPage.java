package com.epam.test.HomeWork22.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

public class OlxWishListPage {

    @FindBy(xpath = "//h2[contains(@class, 'x-large fbold lheight20')]")
    private SelenideElement messageHeader;

    public boolean isMeassgeHeader() {
        return messageHeader.isDisplayed();
    }
}
