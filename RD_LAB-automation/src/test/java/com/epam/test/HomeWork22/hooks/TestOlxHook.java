package com.epam.test.HomeWork22.hooks;

import com.epam.test.HomeWork22.utils.TestConfigurator;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class TestOlxHook {

    TestConfigurator testConfigurator;

    @Before
    public void setUp() {
        testConfigurator = new TestConfigurator();
        testConfigurator.openDriver();
    }

    @After
    public void tearDown() {
        testConfigurator.closeDriver();
    }
}
