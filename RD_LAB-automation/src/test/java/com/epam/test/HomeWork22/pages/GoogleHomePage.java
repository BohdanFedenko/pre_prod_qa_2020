package com.epam.test.HomeWork22.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class GoogleHomePage {

    private static final String URL = "https://www.google.com/";

    @FindBy(name = "q")
    private SelenideElement googleSearchField;

    public GoogleResultSearchPage search(String searchValue) {
        googleSearchField.val(searchValue).pressEnter();
        return page(GoogleResultSearchPage.class);
    }

    public void open() {
        Selenide.open(URL);
    }
}
