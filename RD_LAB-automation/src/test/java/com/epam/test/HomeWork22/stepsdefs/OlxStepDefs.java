package com.epam.test.HomeWork22.stepsdefs;

import com.epam.test.HomeWork22.pages.*;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.page;

public class OlxStepDefs {

    String language;

    GoogleHomePage googleHomePage = page(GoogleHomePage.class);

    GoogleResultSearchPage googleResultSearchPage = page(GoogleResultSearchPage.class);

    OlxHomePage olxHomePage = page(OlxHomePage.class);

    OlxResultSearchPage olxResultSearchPage = page(OlxResultSearchPage.class);

    OlxRegistrationPage olxRegistrationPage = page(OlxRegistrationPage.class);

    OlxWishListPage olxWishListPage = page(OlxWishListPage.class);

    @Given("I navigate to google")
    public void navigateToGoogle() {
        googleHomePage.open();
    }

    @And("enter {string} to search field")
    public void search(String value) {
        googleHomePage.search(value);
    }

    @When("I open fist item of the result list")
    public void openOlxResult() {
        googleResultSearchPage.getOlx();
    }

    @Then("olx home page is opened")
    public void isOlxHomePage() {
        olxHomePage.isHeader();
    }

    @Given("I navigate to olx")
    public void navigateToOlx() {
    }

    @When("I enter {string} to search field and submit")
    public void searchOnOlx(String productName) {
        olxHomePage.search(productName);
    }

    @Then("result is displayed")
    public void isSearchResult() {
        Assertions.assertTrue(olxResultSearchPage.isResultSearchBlock());
    }

    @When("I click put offer button")
    public void putOfferButton() {
        olxHomePage.putOffer();
    }

    @Then("registration page is opened")
    public void isOlxRegistrationPage() {
        Assertions.assertTrue(olxRegistrationPage.isRegistrationPage());
    }

    @When("I changing language of web site")
    public void changeLanguageOne() {
        language = olxHomePage.getCategoryHeader();
        olxHomePage.switchLang();
    }

    @Then("web site language is changed")
    public void isPageLanguage() {
        Assertions.assertNotEquals(language, olxHomePage.getCategoryHeader());
    }

    @And("switch language to default")
    public void changeLanguageTwo() {
        olxHomePage.switchLang();
    }

    @When("I clicking on heart icon of the header")
    public void openWishListPage() {
        olxHomePage.openWishListPage();
    }

    @Then("wish list page is opened")
    public void isOlxWishListPage() {
        Assertions.assertTrue(olxWishListPage.isMeassgeHeader());
    }

    @And("open Мой профиль page")
    public void openMyProfilePage() {
        olxHomePage.opemProfile();
    }

    @When("I clicking enter button of the displayed form")
    public void clickOnEnterButton() {
        olxRegistrationPage.clickEnterButton();
    }

    @Then("error message about incorrect email is displayed")
    public void isErrorMessage() {
        Assertions.assertTrue(olxRegistrationPage.isErrorEmailMessage());
    }

    @And("search {string}")
    public void openMyProfilePage(String productName) {
        olxHomePage.search(productName);
    }

    @When("I clicking add to wish list icon of the one of the result list")
    public void addToWishList() {
        olxResultSearchPage.addToWishList();
    }

    @Then("message about successful operation is displayed")
    public void isSuccessfulMessage() {
        Assertions.assertTrue(olxResultSearchPage.isAddToWishListMessage());
    }

    @Given("I navigate to product list page")
    public void navigateToProductListPage() {
        olxResultSearchPage.open();
    }

    @Then("product list page is opened")
    public void isProductListPage() {
        Assertions.assertTrue(olxResultSearchPage.isCurrentPage());
    }
}
