package com.epam.test.HomeWork22.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class OlxRegistrationPage {

    @FindBy(xpath = "//section[@class = 'login-page has-animation']//button[@id='se_userLogin']")
    private SelenideElement enterButton;

    @FindBy(xpath = "//div[@class = 'errorboxContainer']/label[@for = 'userEmail']")
    private SelenideElement errorEmailMessage;

    public boolean isRegistrationPage() {
        return enterButton.isDisplayed();
    }

    public OlxRegistrationPage clickEnterButton() {
        enterButton.click();
        return page(OlxRegistrationPage.class);
    }

    public boolean isErrorEmailMessage() {
        return errorEmailMessage.isDisplayed();
    }
}
