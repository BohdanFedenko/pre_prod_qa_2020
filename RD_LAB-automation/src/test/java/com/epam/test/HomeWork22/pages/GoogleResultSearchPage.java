package com.epam.test.HomeWork22.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class GoogleResultSearchPage {

    @FindBy(xpath = "//a[contains(@href,'https://www.olx.ua/')]/h3")
    private SelenideElement olxLink;

    public OlxHomePage getOlx() {
        olxLink.click();
        return page(OlxHomePage.class);
    }
}
