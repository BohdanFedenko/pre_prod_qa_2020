package com.epam.test.HomeWork22.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"json:target/olxtests/homework22/cucumber-report.json"},
        features = {"src/test/resources/olx/features"},
        glue = {"com/epam/test/HomeWork22/stepsdefs", "com/epam/test/HomeWork22/hooks"}
)
public class OlxTestsRunner {


}
