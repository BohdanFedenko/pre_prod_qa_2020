package com.epam.test.HomeWork22.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.WebDriverRunner.url;

public class OlxResultSearchPage {

    private static final String URL = "https://www.olx.ua/list/";

    @FindBy(xpath = "//div[@class = 'offer-wrapper']")
    private SelenideElement resultSearchBlock;

    @FindBy(xpath = "//span[@data-icon = 'star']")
    private ElementsCollection addToWishListButton;

    @FindBy(xpath = "//*[@id='fancybox-title-inside']/p")
    private SelenideElement addToWishListMessage;

    public boolean isResultSearchBlock() {
        return resultSearchBlock.isDisplayed();
    }

    public OlxResultSearchPage addToWishList() {
        addToWishListButton.get(0).click();
        return page(this);
    }

    public boolean isAddToWishListMessage() {
        return addToWishListMessage.isDisplayed();
    }

    public void open() {
        Selenide.open(URL);
    }

    public boolean isCurrentPage() {
        return url().contains(URL);
    }
}
