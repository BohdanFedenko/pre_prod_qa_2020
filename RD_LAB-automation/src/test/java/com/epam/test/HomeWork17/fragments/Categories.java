package com.epam.test.HomeWork17.fragments;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextBlock;

import java.util.List;

@FindBy(className = "maincategories")
public class Categories extends HtmlElement {

    @FindBy(css = "div[class='li fleft'] a:first-child")
    private List<HtmlElement> categoriesItems;

    @FindBy(xpath = "//div[@class = 'subcategories-title']")
    private List<HtmlElement> categoryItems;

    @FindBy(xpath = "//h3")
    private TextBlock categoriesBlockTitle;

    public String getTitle() {
        return categoriesBlockTitle.getText();
    }

    public void openCategory(String categoryName) {
        for (HtmlElement element : categoriesItems) {
            if (element.getText().equals(categoryName)) {
                element.click();
                openCategoryItem(categoryName);
                break;
            }
        }
    }

    private void openCategoryItem(String categoryItemName) {
        for (HtmlElement element : categoryItems) {
            if (element.getText().contains(categoryItemName)) {
                element.click();
                break;
            }
        }
    }
}
