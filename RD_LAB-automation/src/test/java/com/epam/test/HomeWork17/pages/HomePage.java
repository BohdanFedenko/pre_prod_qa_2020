package com.epam.test.HomeWork17.pages;

import com.epam.test.HomeWork17.fragments.Categories;
import com.epam.test.HomeWork17.fragments.Header;
import com.epam.test.HomeWork17.fragments.SearchForm;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextBlock;

public class HomePage extends BasePage {

    @Name("Categories")
    @FindBy(xpath = "//div[@class = 'li fleft']")
    private HtmlElement mainCategorySection;

    @FindBy(xpath = "//*[@id='cityFieldError']/p")
    private TextBlock cityErrorMessage;

    private Header header;

    private SearchForm searchForm;

    private Categories categories;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void putOffer() {
        header.putOffer();
    }

    public void switchLang() {
        header.switchLang();
    }

    public String getCategoriesTitle() {
        return categories.getTitle();
    }

    public void search(String text) {
        searchForm.inputTextToSearchField(text);
        searchForm.clickSearchButton();
    }

    public void search(String text, String cityName) {
        searchForm.inputTextToSearchField(text);
        searchForm.inputCityToCityField(cityName);
        searchForm.clickSearchButton();
    }

    public void openCategory(String categoryName) {
        categories.openCategory(categoryName);
    }

    public boolean isCityErrorMessage() {
        return cityErrorMessage.isDisplayed();
    }

    public WebElement getLoadableElement() {
        return mainCategorySection;
    }
}
