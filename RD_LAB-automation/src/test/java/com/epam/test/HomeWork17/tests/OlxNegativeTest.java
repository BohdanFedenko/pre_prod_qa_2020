package com.epam.test.HomeWork17.tests;

import com.epam.test.HomeWork17.pages.HomePage;
import com.epam.test.HomeWork17.pages.ProductPage;
import com.epam.test.HomeWork17.pages.RegistrationPage;
import com.epam.test.HomeWork17.pages.SearchResultPage;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class OlxNegativeTest {

    private WebDriver driver;

    private HomePage homePage;

    private SearchResultPage searchResultPage;

    private ProductPage productPage;

    private RegistrationPage registrationPage;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        homePage = new HomePage(driver);
        searchResultPage = new SearchResultPage(driver);
        productPage = new ProductPage(driver);
        registrationPage = new RegistrationPage(driver);
    }

    @Test(expectedExceptions = NoSuchElementException.class)
    public void checkSearchUseIncorrectCityTest() {
        homePage.openPage();
        homePage.search("Очки", "City");
        Assertions.assertTrue(homePage.isCityErrorMessage());
    }

    @Test
    public void checkMakeComplainTest() {
        homePage.openPage();
        homePage.search("Очки");
        searchResultPage.openProduct();
        productPage.putComplain();
        productPage.submitComplainForm();
        Assertions.assertTrue(productPage.isErrorMessage());
    }

    @Test
    public void checkSendMessageToSellerWithEmptyEmailTest() {
        homePage.openPage();
        homePage.search("Очки");
        searchResultPage.openProduct();
        productPage.sendMessageToSeller();
        Assertions.assertTrue(productPage.isSentWithEmptyEmailErrorMessage());
    }

    @Test
    public void checkSendMessageToSellerWithEmptyTextBoxTest() {
        homePage.openPage();
        homePage.search("Очки");
        searchResultPage.openProduct();
        productPage.sendMessageToSeller();
        Assertions.assertTrue(productPage.isSentWithEmptyTextBoxErrorMessage());
    }

    @Test
    public void checkLogInWithEmptyEmailAndPasswordTest() {
        homePage.openPage();
        homePage.putOffer();
        registrationPage.enter();
        Assertions.assertTrue(registrationPage.isErrorMessage());
    }

    @AfterClass
    public void tearDown() {
        driver.close();
    }
}
