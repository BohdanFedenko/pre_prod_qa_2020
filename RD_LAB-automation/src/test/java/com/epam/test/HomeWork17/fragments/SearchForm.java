package com.epam.test.HomeWork17.fragments;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;

@FindBy(xpath = "//form[@id='searchmain']//div[@class='wrapper']")
public class SearchForm extends HtmlElement {

    @FindBy(name = "q")
    private TextInput searchField;

    @FindBy(id = "cityField")
    private TextInput cityField;

    @FindBy(xpath = "//div[@class='rel fleft input-container']//li[2]//a[1]")
    private Button firstCityOfCitiesList;

    @FindBy(xpath = "//input[@type ='submit']")
    private Button searchButton;

    public void inputTextToSearchField(String text) {
        searchField.clear();
        searchField.sendKeys(text);
    }

    public void inputCityToCityField(String city) {
        cityField.clear();
        cityField.sendKeys(city);
        firstCityOfCitiesList.click();
    }

    public void clickSearchButton() {
        searchButton.click();
    }
}
