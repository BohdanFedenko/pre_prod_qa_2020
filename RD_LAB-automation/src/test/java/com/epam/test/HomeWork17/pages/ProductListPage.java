package com.epam.test.HomeWork17.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.TextBlock;

public class ProductListPage extends BasePage {

    @FindBy(xpath = "//h1[@class = 'small fnormal inline lheight18']")
    private TextBlock categoryName;

    public ProductListPage(WebDriver driver) {
        super(driver);
    }

    public String getCategoryName() {
        return categoryName.getText();
    }

    public WebElement getLoadableElement() {
        return categoryName;
    }
}
