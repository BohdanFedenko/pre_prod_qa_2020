package com.epam.test.HomeWork17.fragments;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

@FindBy(xpath = "//div[@class = 'offer-wrapper']")
public class ResultSearchBlock extends HtmlElement {

    @FindBy(xpath = "//a[contains(@class, 'detailsLink')]/strong")
    private List<HtmlElement> productCards;

    public void openProduct() {
        productCards.get(10).click();
    }
}
