package com.epam.test.HomeWork17.pages;

import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;

public abstract class BasePage implements Page {

    protected String PAGE_URL = "https://www.olx.ua/";

    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        HtmlElementLoader.populate(this, this.driver);
    }

    public boolean isOnPage() {
        return driver.getCurrentUrl().equals(PAGE_URL);
    }

    public boolean isPageLoaded() {
        return getLoadableElement().isDisplayed();
    }

    public void openPage() {
        driver.get(PAGE_URL);
    }
}
