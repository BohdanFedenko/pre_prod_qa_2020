package com.epam.test.HomeWork17.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.TextBlock;

public class SellerPage extends BasePage {

    @FindBy(xpath = "//h2[@class='search-user-profile__user-name']")
    private TextBlock pageTitle;

    public SellerPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getLoadableElement() {
        return pageTitle;
    }
}
