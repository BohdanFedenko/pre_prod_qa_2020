package com.epam.test.HomeWork17.pages;

import org.openqa.selenium.WebElement;

public interface Page {

    boolean isOnPage();

    boolean isPageLoaded();

    void openPage();

    WebElement getLoadableElement();
}
