package com.epam.test.HomeWork17.pages;

import com.epam.test.HomeWork17.fragments.ResultSearchBlock;
import com.epam.test.HomeWork17.fragments.SearchForm;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

public class SearchResultPage extends BasePage {

    @FindBy(xpath = "//table[@id='offers_table']")
    private HtmlElement resultList;

    @FindBy(xpath = "//div[@id='withshowbox']")
    private SearchForm searchForm;

    private ResultSearchBlock resultSearchBlock;

    public SearchResultPage(WebDriver driver) {
        super(driver);
        super.PAGE_URL += "list/";
    }

    public void search(String text) {
        searchForm.inputTextToSearchField(text);
        searchForm.clickSearchButton();
    }

    public void openProduct() {
        resultSearchBlock.openProduct();
    }

    public WebElement getLoadableElement() {
        return resultList;
    }
}
