package com.epam.test.HomeWork17.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextBlock;

public class ProductPage extends BasePage {

    @FindBy(xpath = "//div[@class='descriptioncontent__headline']")
    private TextBlock productDescription;

    @FindBy(xpath = "//div[@class='offer-user__actions']//h4//a")
    private Button seller;

    @FindBy(xpath = "//a[@id = 'reportMe']")
    private Button complain;

    @FindBy(xpath = "//input[@id = 'report-submit']")
    private Button submitComplain;

    @FindBy(xpath = "//*[@id=\"message_system\"]/div/div/p/span")
    private TextBlock errorMessage;

    @FindBy(xpath = "//div[@class = 'fblock fblock--submit']//input")
    private Button sendMessageToSellerButton;

    @FindBy(xpath = "//p[@class = 'desc errorboxContainer']/small[@id = 'se_emailError']")
    private TextBlock sentWithEmptyEmailErrorMessage;

    @FindBy(xpath = "//p[@class = 'desc errorboxContainer']/small[@id = 'se_messageError']")
    private TextBlock sentWithEmptyTextBoxErrorMessage;

    @FindBy(xpath = "//button[contains(@class, 'Button AdPageBox')]")
    private WebElement buyWithDeliveryButton;

    @FindBy(xpath = "//div[contains(@class, 'mandatory-login ')]")
    private WebElement buyWithDeliveryForm;

    public void openBuyWithDeliveryForm() {
        new WebDriverWait(driver, 50)
                .until(ExpectedConditions.elementToBeClickable(buyWithDeliveryButton));
        buyWithDeliveryButton.click();
    }

    public boolean isBuyWithDeliveryForm() {
        return buyWithDeliveryForm.isDisplayed();
    }

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public void sendMessageToSeller() {
        sendMessageToSellerButton.click();
    }

    public void putComplain() {
        complain.click();
    }

    public void submitComplainForm() {
        submitComplain.click();
    }

    public boolean isErrorMessage() {
        return errorMessage.isDisplayed();
    }

    public boolean isSentWithEmptyEmailErrorMessage() {
        return sentWithEmptyEmailErrorMessage.isDisplayed();
    }

    public boolean isSentWithEmptyTextBoxErrorMessage() {
        return sentWithEmptyTextBoxErrorMessage.isDisplayed();
    }

    public void openSellerPage() {
        seller.click();
    }

    public WebElement getLoadableElement() {
        return productDescription;
    }
}
