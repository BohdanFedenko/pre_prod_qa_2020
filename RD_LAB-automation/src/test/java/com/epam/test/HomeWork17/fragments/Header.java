package com.epam.test.HomeWork17.fragments;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@FindBy(xpath = "//div[@class='navi']")
public class Header extends HtmlElement {

    @FindBy(xpath = "//a[@class='x-normal']")
    private Button anotherLang;

    @FindBy(xpath = "//a[@id='postNewAdLink']")
    private Button putOffer;

    public void putOffer() {
        putOffer.click();
    }

    public void switchLang() {
        anotherLang.click();
    }
}
