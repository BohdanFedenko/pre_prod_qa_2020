package com.epam.test.HomeWork17.tests;

import com.epam.test.HomeWork17.pages.*;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class OlxPositiveTest {

    private WebDriver driver;

    private HomePage homePage;

    private SearchResultPage searchResultPage;

    private ProductPage productPage;

    private SellerPage sellerPage;

    private ProductListPage productListPage;

    private RegistrationPage registrationPage;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        homePage = new HomePage(driver);
        searchResultPage = new SearchResultPage(driver);
        productPage = new ProductPage(driver);
        sellerPage = new SellerPage(driver);
        productListPage = new ProductListPage(driver);
        registrationPage = new RegistrationPage(driver);
    }

    @Test
    public void checkOpenHomePageTest() {
        homePage.openPage();
        Assertions.assertTrue(homePage.isPageLoaded());
    }

    @Test
    public void checkLangSwitchTest() {
        homePage.openPage();
        String beforeTitle = homePage.getCategoriesTitle();
        homePage.switchLang();
        String afterTitle = homePage.getCategoriesTitle();
        Assertions.assertNotEquals(beforeTitle, afterTitle);
    }

    @Test
    public void checkOpenResultSearchPageAfterQueryWithoutCityTest() {
        homePage.openPage();
        homePage.search("солнцезащитные очки");
        Assertions.assertTrue(searchResultPage.isPageLoaded());
    }

    @Test
    public void checkOpenResultSearchPageAfterQueryWithCityTest() {
        homePage.openPage();
        homePage.search("Очки", "Днепр");
        Assertions.assertTrue(searchResultPage.isPageLoaded());
    }

    @Test
    public void checkSearchFromResultSearchPageTest() {
        searchResultPage.openPage();
        searchResultPage.search("Наушники");
        Assertions.assertTrue(searchResultPage.isPageLoaded());
    }


    @Test
    public void checkOpenProductPageTest() {
        homePage.openPage();
        homePage.search("Очки");
        searchResultPage.openProduct();
        Assertions.assertTrue(productPage.isPageLoaded());
    }

    @Test
    public void checkOpenBuyWithDelivery() {
        homePage.openPage();
        homePage.search("Наушники");
        searchResultPage.openProduct();
        productPage.openBuyWithDeliveryForm();
        Assertions.assertTrue(productPage.isBuyWithDeliveryForm());
    }

    @Test
    public void checkOpenSellerPageTest() {
        homePage.openPage();
        homePage.search("Очки");
        searchResultPage.openProduct();
        productPage.openSellerPage();
        Assertions.assertTrue(sellerPage.isPageLoaded());
    }

    @Test
    public void checkOpenCategoryPageTest() {
        homePage.openPage();
        homePage.openCategory("Обмен");
        Assertions.assertEquals("Обмен", productListPage.getCategoryName());
    }

    @Test
    public void checkPutOfferWithoutLogInTest() {
        homePage.openPage();
        homePage.putOffer();
        registrationPage.getLoadableElement();
        Assertions.assertTrue(driver.getCurrentUrl().contains("account"));
    }

    @AfterClass
    public void tearDown() {
        driver.close();
    }
}
