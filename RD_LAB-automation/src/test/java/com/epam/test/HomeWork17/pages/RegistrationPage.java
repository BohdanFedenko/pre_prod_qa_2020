package com.epam.test.HomeWork17.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextBlock;

import java.util.List;

public class RegistrationPage extends BasePage {

    @FindBy(xpath = "//input[@id=\"userEmail\"]")
    private List<HtmlElement> emailField;

    @FindBy(xpath = "//section[@class = 'login-page has-animation']//button[@id='se_userLogin']")
    private Button enterButton;

    @FindBy(xpath = "//div[@class = 'errorboxContainer']")
    private TextBlock errorMessage;

    public RegistrationPage(WebDriver driver) {
        super(driver);
    }

    public void enter() {
        enterButton.click();
    }

    public boolean isErrorMessage() {
        return errorMessage.isDisplayed();
    }

    public WebElement getLoadableElement() {
        return emailField.get(0);
    }
}
