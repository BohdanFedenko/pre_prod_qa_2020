package com.epam.test.HomeWork23;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.epam.test.HomeWork23.utils.TestConfigurator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class SelenoidTest {

    static final String BASE_URL = "https://www.google.com/";

    static final String GOOGLE_SEARCH_FIELD_NAME = "q";

    static final String SEARCH_RESULT_XPATH = "//a[@href='https://www.selenium.dev/']/h3/span";

    static TestConfigurator testConfigurator = new TestConfigurator();

    @BeforeAll
    public static void setUp() {
        testConfigurator.setSelenoidConfiguration(BASE_URL);
    }

    @ParameterizedTest
    @CsvSource("Selenium, Selenium")
    public void searchSelenium(String searchValue, String resultValue) {
        //Given
        Selenide.open("/");

        //When
        $(By.name(GOOGLE_SEARCH_FIELD_NAME)).setValue(searchValue).pressEnter();
        String result = $x(SEARCH_RESULT_XPATH).shouldBe(Condition.visible).getText();

        //Then
        Assertions.assertTrue(result.contains(resultValue));
    }
}
