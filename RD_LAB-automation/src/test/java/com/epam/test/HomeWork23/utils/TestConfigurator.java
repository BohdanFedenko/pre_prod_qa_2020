package com.epam.test.HomeWork23.utils;

import com.codeborne.selenide.Configuration;

public class TestConfigurator {

    public void setSelenoidConfiguration(String baseUrl) {
        Configuration.driverManagerEnabled = false;
        Configuration.baseUrl = baseUrl;
        Configuration.startMaximized = true;
        Configuration.browser = CustomProvider.class.getName();
    }
}
