package com.epam.test.HomeWork20.restassured;

import com.epam.test.HomeWork20.restassured.models.*;
import com.epam.test.HomeWork20.restassured.services.UserService;
import com.epam.test.HomeWork20.restassured.utils.ObjectFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserTest {

    static final UserService userService = new UserService();

    static final ObjectFactory objectFactory = new ObjectFactory();

    @BeforeAll
    static void setUp() {
        CreateUserRequest user = objectFactory.createUser(1001, "TestUser", "Test", "Test");
        CreateUserRequest user1 = objectFactory.createUser(1002, "User2", "Test", "Test");
        CreateUserRequest user2 = objectFactory.createUser(1003, "User3", "Test", "Test");
        userService.createUserRequest(user);
        userService.createUserRequest(user1);
        userService.createUserRequest(user2);
    }

    @Test
    public void checkCreateUserTest() {
        //GIVEN
        CreateUserRequest user = objectFactory.createUser(1004, "User4", "Test", "Test");

        //WHEN
        CreateUserResponse createdUser = userService.createUserRequest(user);

        //THEN
        assertEquals(createdUser.getMessage(), "" + user.getId() + "");
    }

    @Test
    public void checkCreateUsersWithArrayTest() {
        //GIVEN
        CreateUserRequest user = objectFactory.createUser(1005, "User5", "Test", "Test");
        CreateUserRequest user1 = objectFactory.createUser(1006, "User6", "Test", "Test");
        CreateUserRequest[] createUsersRequests = new CreateUserRequest[]{user, user1};

        //WHEN
        CreateUsersWithArrayResponse createdUsersArray = userService.createUsersWithArrayRequest(createUsersRequests);

        //THEN
        assertEquals(createdUsersArray.getMessage(), "ok");
        assertEquals(createdUsersArray.getType(), "unknown");
    }

    @Test
    public void checkCreateUsersWithListTest() {
        //GIVEN
        CreateUserRequest user = objectFactory.createUser(1007, "User7", "Test", "Test");
        CreateUserRequest user1 = objectFactory.createUser(1008, "User8", "Test", "Test");
        List<CreateUserRequest> createUsersRequests = new ArrayList<>();
        createUsersRequests.add(user);
        createUsersRequests.add(user1);

        //WHEN
        CreateUsersWithListResponse createdUsersList = userService.createUsersWithListRequest(createUsersRequests);

        //THEN
        assertEquals(createdUsersList.getMessage(), "ok");
        assertEquals(createdUsersList.getType(), "unknown");
    }

    @Test
    public void checkGetUserByUserNameTest() {
        //GIVEN
        String userName = "User3";

        //WHEN
        GetUserByUserNameResponse user = userService.getUserByUserNameRequest(userName);

        //THEN
        assertEquals(user.getUsername(), userName);
    }

    @Test
    public void CheckUpdateUserByUserNameTest() {
        //GIVEN
        String userName = "User2";
        UpdateUserRequest user = objectFactory.getUpdateUser(1002, "Updated", "Test", "Test");

        //WHEN
        UpdateUserResponse updatedUser = userService.updateUserByUserNameRequest(userName, user);

        //THEN
        assertEquals(updatedUser.getType(), "unknown");
    }

    @Test
    public void checkDeleteUserByUserNameTest() {
        //GIVEN
        String userName = "TestUser";

        //WHEN
        DeleteUserResponse deleteMessage = userService.deleteUserByNameRequest(userName);

        //THEN
        assertEquals(deleteMessage.getType(), "unknown");
    }

    @Test
    public void checkLoginUserTest() {
        //GIVEN
        String userName = "Test User";
        String password = "qwerty";

        //WHEN
        LogInUserResponse loginMessage = userService.loginUserRequest(userName, password);

        //THEN
        assertEquals(loginMessage.getType(), "unknown");
    }

    @Test
    public void checkLogoutUserTest() {
        //GIVEN

        //WHEN
        LogOutUserResponse logoutMessage = userService.logoutUserRequest();

        //THEN
        assertEquals(logoutMessage.getMessage(), "ok");
    }

    @AfterAll
    static void tearDown() {
        userService.deleteUserByNameRequest("User3");
        userService.deleteUserByNameRequest("User4");
        userService.deleteUserByNameRequest("User5");
        userService.deleteUserByNameRequest("User6");
        userService.deleteUserByNameRequest("User7");
        userService.deleteUserByNameRequest("User8");
        userService.deleteUserByNameRequest("Updated");
    }
}
