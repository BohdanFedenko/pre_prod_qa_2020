package com.epam.test.HomeWork20.okhttp.utils;

import com.epam.test.HomeWork12.models.requests.addnewpet.AddNewPetRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PetHelper {

    ObjectMapper objectMapper = new ObjectMapper();

    public AddNewPetRequest createPet(int id, String name, String status) {
        AddNewPetRequest pet = new AddNewPetRequest();
        pet.setId(id);
        pet.setName(name);
        pet.setStatus(status);
        return pet;
    }
}
