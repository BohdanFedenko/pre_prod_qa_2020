package com.epam.test.HomeWork20.okhttp.services;

import com.epam.test.HomeWork12.models.requests.addnewpet.AddNewPetRequest;
import com.epam.test.HomeWork20.okhttp.models.deletepetresponse.DeletePetResponse;
import com.epam.test.HomeWork20.okhttp.models.getpetbyidresponse.GetPetResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;

import java.io.IOException;

public class PetService {

    static final String BASE_URI = "https://petstore.swagger.io";

    static final String PET = "/v2/pet/";

    OkHttpClient client = new OkHttpClient();

    ObjectMapper objectMapper = new ObjectMapper();

    public GetPetResponse addNewPet(AddNewPetRequest newPet) throws IOException {
        MediaType json = MediaType.parse("application/json");
        String jsonPet = objectMapper.writeValueAsString(newPet);
        RequestBody body = RequestBody.create(json, jsonPet);
        Request request = new Request.Builder()
                .url(BASE_URI + PET)
                .post(body)
                .build();
        ResponseBody responseBody = client.newCall(request)
                .execute().body();
        return objectMapper.readValue(responseBody.string(), GetPetResponse.class);
    }

    public GetPetResponse getPetById(int id) throws IOException {
        Request request = new Request.Builder()
                .url(BASE_URI + PET + id)
                .build();
        ResponseBody responseBody = client.newCall(request)
                .execute().body();
        return objectMapper.readValue(responseBody.string(), GetPetResponse.class);
    }

    public GetPetResponse updatePet(AddNewPetRequest newPet) throws IOException {
        MediaType json = MediaType.parse("application/json");
        String jsonPet = objectMapper.writeValueAsString(newPet);
        RequestBody body = RequestBody.create(json, jsonPet);
        Request request = new Request.Builder()
                .url(BASE_URI + PET)
                .put(body)
                .build();
        ResponseBody responseBody = client.newCall(request)
                .execute().body();
        return objectMapper.readValue(responseBody.string(), GetPetResponse.class);
    }

    public DeletePetResponse deletePet(int id) throws IOException {
        Request request = new Request.Builder()
                .url(BASE_URI + PET + id)
                .delete()
                .build();
        ResponseBody responseBody = client.newCall(request)
                .execute().body();
        return objectMapper.readValue(responseBody.string(), DeletePetResponse.class);
    }
}
