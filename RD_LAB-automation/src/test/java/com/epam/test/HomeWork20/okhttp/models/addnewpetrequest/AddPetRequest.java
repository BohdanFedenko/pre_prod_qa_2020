package com.epam.test.HomeWork20.okhttp.models.addnewpetrequest;

import com.epam.test.HomeWork12.models.responses.addnewpet.Category;
import com.epam.test.HomeWork12.models.responses.addnewpet.TagsItem;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class AddPetRequest {

    @JsonProperty("photoUrls")
    private List<String> photoUrls;

    @JsonProperty("name")
    private String name;

    @JsonProperty("id")
    private int id;

    @JsonProperty("category")
    private Category category;

    @JsonProperty("tags")
    private List<TagsItem> tags;

    @JsonProperty("status")
    private String status;
}
