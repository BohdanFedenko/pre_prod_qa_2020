package com.epam.test.HomeWork20.okhttp;

import com.epam.test.HomeWork12.models.requests.addnewpet.AddNewPetRequest;
import com.epam.test.HomeWork20.okhttp.models.deletepetresponse.DeletePetResponse;
import com.epam.test.HomeWork20.okhttp.models.getpetbyidresponse.GetPetResponse;
import com.epam.test.HomeWork20.okhttp.services.PetService;
import com.epam.test.HomeWork20.okhttp.utils.PetHelper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PetTest {

    PetService petService = new PetService();

    PetHelper petHelper = new PetHelper();

    @BeforeAll
    void setUp() throws IOException {
        AddNewPetRequest pet = petHelper.createPet(1001, "Rex", "available");
        AddNewPetRequest pet2 = petHelper.createPet(1002, "Rex10", "available");
        AddNewPetRequest pet3 = petHelper.createPet(1003, "Rex110", "available");
        AddNewPetRequest pet4 = petHelper.createPet(1004, "Rex1110", "available");
        petService.addNewPet(pet);
        petService.addNewPet(pet2);
        petService.addNewPet(pet3);
        petService.addNewPet(pet4);
    }

    @Test
    void checkAddNewPetTest() throws IOException {
        //GIVEN
        AddNewPetRequest pet = petHelper.createPet(1005, "Rex Created", "available");

        //WHEN
        GetPetResponse addedPet = petService.addNewPet(pet);

        //THEN;
        assertEquals(pet.getId(), addedPet.getId());
        assertEquals(pet.getName(), addedPet.getName());
        assertEquals(pet.getStatus(), addedPet.getStatus());
    }

    @Test
    void checkUpdateExistPetTest() throws IOException {
        //GIVEN
        AddNewPetRequest pet = petHelper.createPet(1001, "Rex Update", "available");

        //WHEN
        GetPetResponse addedPet = petService.updatePet(pet);

        //THEN;
        assertEquals(pet.getId(), addedPet.getId());
        assertEquals(pet.getName(), addedPet.getName());
        assertEquals(pet.getStatus(), addedPet.getStatus());
    }

    @Test
    void checkUpdateExistPetWithFormDataTest() throws IOException {
        //GIVEN
        AddNewPetRequest pet = petHelper.createPet(1002, "Rex10 Update", "available");

        //WHEN
        GetPetResponse addedPet = petService.updatePet(pet);

        //THEN;
        assertEquals(pet.getId(), addedPet.getId());
        assertEquals(pet.getName(), addedPet.getName());
        assertEquals(pet.getStatus(), addedPet.getStatus());
    }

    @Test
    void checkGetExistPetByIdTest() throws IOException {
        //GIVEN
        int existPetId = 1003;

        //WHEN
        GetPetResponse existPet = petService.getPetById(existPetId);

        //THEN
        assertEquals(existPetId, existPet.getId());
        assertEquals("Rex110", existPet.getName());
    }

    @Test
    void checkDeleteExistPetTest() throws IOException {
        //GIVEN
        int existPetId = 1004;

        //WHEN
        DeletePetResponse message = petService.deletePet(existPetId);

        //THEN
        assertEquals(Integer.toString(existPetId), message.getMessage());
    }

    @AfterAll
    void tearDown() throws IOException {
        petService.deletePet(1001);
        petService.deletePet(1002);
        petService.deletePet(1003);
        petService.deletePet(1005);
    }
}
