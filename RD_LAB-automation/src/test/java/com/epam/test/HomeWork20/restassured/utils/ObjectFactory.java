package com.epam.test.HomeWork20.restassured.utils;

import com.epam.test.HomeWork20.restassured.models.CreateUserRequest;
import com.epam.test.HomeWork20.restassured.models.UpdateUserRequest;

public class ObjectFactory {

    public CreateUserRequest createUser(int id, String userName, String firstName, String lastName) {
        CreateUserRequest user = new CreateUserRequest();
        user.setId(id);
        user.setUsername(userName);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    public UpdateUserRequest getUpdateUser(int id, String userName, String firstName, String lastName) {
        UpdateUserRequest user = new UpdateUserRequest();
        user.setId(id);
        user.setUsername(userName);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }
}
