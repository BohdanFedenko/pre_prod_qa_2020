package com.epam.test.HomeWork9;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class TextAnalyzer {

    private final Logger logger = Logger.getLogger(InputHelper.class.getName());

    private String filePath;

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void getOperation(String operationType) {
        logger.info("Words from the file:\n" + getWords());
        switch (operationType) {
            case "frequency":
                getFrequency();
                break;
            case "length":
                getLength();
                break;
            case "duplicates":
                getDuplicates();
                break;
            default:
                logger.warning("Selected operation is incorrect!");
                break;
        }
    }

    private List<String> getWords() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        List<String> words = new ArrayList<>();
        while (scanner.hasNext()) {
            String word = scanner.useDelimiter("[\\,]\\s+|[\\.]?\\s+|[\\,.]+|[\\.]+|[-?\\d]+").next();
            words.add(word);
        }
        return words;
    }

    private void getFrequency() {
        logger.info("The two most common words in this file.");
        Set<Map.Entry<String, Long>> frequency = getWords()
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet();
        frequency.stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(2)
                .sorted(Map.Entry.comparingByKey(Comparator.reverseOrder()))
                .forEach(System.out::println);
    }

    private void getLength() {
        logger.info("The three words of the longest words in this file.");
        Set<Map.Entry<Integer, List<String>>> length = getWords()
                .stream()
                .distinct()
                .collect(Collectors.groupingBy(String::length))
                .entrySet();
        length
                .stream()
                .sorted(Map.Entry.comparingByKey(Comparator.reverseOrder()))
                .limit(3)
                .forEach(word -> System.out.println(word.getValue().get(0) + " = " + word.getKey()));
    }

    private void getDuplicates() {
        logger.info("The three words with duplicate in this file.");
        getWords()
                .stream()
                .filter(i -> Collections.frequency(getWords(), i) > 1)
                .limit(3)
                .sorted(Comparator.comparing(String::length))
                .forEach(word -> System.out.println(new StringBuilder(word.toUpperCase()).reverse()));
    }
}
