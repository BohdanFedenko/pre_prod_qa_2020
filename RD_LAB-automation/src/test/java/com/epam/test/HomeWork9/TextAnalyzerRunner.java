package com.epam.test.HomeWork9;

public class TextAnalyzerRunner {
    public static void main(String[] args) {

        InputHelper helper = new InputHelper();
        TextAnalyzer textAnalyzer = new TextAnalyzer();

        while (helper.input()) {
            textAnalyzer.setFilePath(helper.getFilePath());
            textAnalyzer.getOperation(helper.getOperationType());
        }
    }
}
