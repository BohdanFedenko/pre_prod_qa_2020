package com.epam.test.HomeWork9;

import java.util.Scanner;

public class InputHelper {

    private String filePath;

    private String operationType;

    public String getFilePath() {
        return filePath;
    }

    public String getOperationType() {
        return operationType;
    }

    // filePath = "./src/test/resources/testFile.txt";
    public boolean input() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter a path to file for reading from it or 'quit' for exit:");
        String inputValue = scanner.nextLine();
        if (inputValue.equals("quit")) {
            return false;
        }
        filePath = inputValue;
        System.out.println("Please, select one of the operations like:\n- frequency;\n- length;\n- duplicates." +
                "\n- quit - for exit.");
        inputValue = scanner.nextLine();
        if (inputValue.equals("quit")) {
            return false;
        }
        operationType = inputValue;
        return true;
    }
}
