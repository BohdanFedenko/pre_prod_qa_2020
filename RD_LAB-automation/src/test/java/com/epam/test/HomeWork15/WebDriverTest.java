package com.epam.test.HomeWork15;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

public class WebDriverTest {

    WebDriver driver;

    StopWatch watch;

    private static final Logger LOG = LogManager.getLogger(WebDriverTest.class);

    private static final String URL_GOOGLE = "https://www.google.com/";

    private static final String URL_OLX = "https://www.olx.ua/";

    private static final String URL_OLX_PRODUCT_LIST = "https://www.olx.ua/list/";

    private static final String GOOGLE_SEARCH_FIELD_NAME = "q";

    private static final String OLX_FOUND_LINK_XPATH = "//a[@href='https://olx.ua/']/h3";

    private static final String OLX_HEADER_XPATH = "//a[@id = 'headerLogo']";

    private static final String SEARCH_FIELD_XPATH = "//input[@id = 'headerSearch']";

    private static final String SEARCH_BUTTON_XPATH = "//input[@id = 'submit-searchmain']";

    private static final String RESULT_SEARCH_BLOCK_XPATH = "//div[@class = 'offer-wrapper']";

    private static final String PUT_OFFER_XPATH = "//a[@id = 'postNewAdLink']";

    private static final String ENTER_BUTTON_XPATH = "//section[@class = 'login-page has-animation']//button[@id='se_userLogin']";

    private static final String LANG_SWITCHER_XPATH = "//a[@class='x-normal']";

    private static final String CATEGORY_HEADER_XPATH = "//div[@class = 'maincategories']//h3";

    private static final String WISH_LIST_XPATH = "//a[contains(@href, 'favorites/search')]";

    private static final String MESSAGE_HEADER_XPATH = "//h2[contains(@class, 'x-large fbold lheight20')]";

    private static final String PROFILE_BUTTON_XPATH = "//a[@id = 'topLoginLink']";

    private static final String ERROR_EMAIL_MESSAGE_XPATH = "//div[@class = 'errorboxContainer']/label[@for = 'userEmail']";

    private static final String ADD_TO_WISHLIST_XPATH = "//span[@data-icon = 'star']";

    private static final String MESSAGE_XPATH = "//*[@id='fancybox-title-inside']/p";

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        watch = new StopWatch();
    }

    @Test
    public void findOlxWithImplicitWaitTest() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        LOG.info("Test with Implicit wait started");
        LOG.info("Count started");
        watch.start();
        driver.get(URL_GOOGLE);
        LOG.info(URL_GOOGLE + " is opening");
        driver.findElement(By.name(GOOGLE_SEARCH_FIELD_NAME)).sendKeys("olx", Keys.ENTER);
        LOG.info("olx is found");
        driver.findElement(By.xpath(OLX_FOUND_LINK_XPATH)).click();
        boolean isLogo = driver.findElement(By.xpath(OLX_HEADER_XPATH)).isDisplayed();
        watch.stop();
        LOG.info("Actual result: " + isLogo);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertTrue(isLogo);
    }

    @Test
    public void findOlxWithExplicitWaitTest() {
        LOG.info("Test with Explicit wait started");
        driver.get(URL_GOOGLE);
        LOG.info(URL_GOOGLE + " is opening");
        LOG.info("Count started");
        watch.start();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.name(GOOGLE_SEARCH_FIELD_NAME)))
                .sendKeys("olx", Keys.ENTER);
        LOG.info("olx is found");
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(OLX_FOUND_LINK_XPATH)))
                .click();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OLX_HEADER_XPATH)));
        boolean isLogo = driver.findElement(By.xpath(OLX_HEADER_XPATH)).isDisplayed();
        watch.stop();
        LOG.info("Actual result: " + isLogo);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertTrue(isLogo);
    }

    @Test
    public void searchOnOlxWithImplicitWaitTest() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        LOG.info("Test with Implicit wait started");
        driver.get(URL_OLX);
        LOG.info(URL_OLX + " is opening");
        LOG.info("Count started");
        watch.start();
        driver.findElement(By.xpath(SEARCH_FIELD_XPATH)).sendKeys("наушники");
        driver.findElement(By.xpath(SEARCH_BUTTON_XPATH)).click();
        boolean isResultBlock = driver.findElement(By.xpath(RESULT_SEARCH_BLOCK_XPATH)).isDisplayed();
        watch.stop();
        LOG.info("Actual result: " + isResultBlock);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertTrue(isResultBlock);
    }

    @Test
    public void searchOnOlxWithExplicitWaitTest() {
        LOG.info("Test with Explicit wait started");
        driver.get(URL_OLX);
        LOG.info(URL_OLX + " is opening");
        LOG.info("Count started");
        watch.start();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(SEARCH_FIELD_XPATH)))
                .sendKeys("наушники");
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(SEARCH_BUTTON_XPATH)))
                .click();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OLX_HEADER_XPATH)));
        boolean isLogo = driver.findElement(By.xpath(OLX_HEADER_XPATH)).isDisplayed();
        watch.stop();
        LOG.info("Actual result: " + isLogo);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertTrue(isLogo);
    }

    @Test
    public void openRegistrationFormOlxWithImplicitWaitTest() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        LOG.info("Test with Implicit wait started");
        driver.get(URL_OLX);
        LOG.info(URL_OLX + " is opening");
        LOG.info("Count started");
        watch.start();
        driver.findElement(By.xpath(PUT_OFFER_XPATH)).click();
        boolean isEnterButton = driver.findElement(By.xpath(ENTER_BUTTON_XPATH)).isDisplayed();
        watch.stop();
        LOG.info("Actual result: " + isEnterButton);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertTrue(isEnterButton);
    }

    @Test
    public void openRegistrationFormOlxWithExplicitWaitTest() {
        LOG.info("Test with Explicit wait started");
        driver.get(URL_OLX);
        LOG.info(URL_OLX + " is opening");
        LOG.info("Count started");
        watch.start();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(PUT_OFFER_XPATH)))
                .click();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ENTER_BUTTON_XPATH)));
        boolean isEnterButton = driver.findElement(By.xpath(ENTER_BUTTON_XPATH)).isDisplayed();
        watch.stop();
        LOG.info("Actual result: " + isEnterButton);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertTrue(isEnterButton);
    }

    @Test
    public void switchLangOlxWithImplicitWaitTest() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        LOG.info("Test with Implicit wait started");
        driver.get(URL_OLX);
        LOG.info(URL_OLX + " is opening");
        LOG.info("Count started");
        watch.start();
        String expectedHeader = driver.findElement(By.xpath(CATEGORY_HEADER_XPATH)).getText();
        driver.findElement(By.xpath(LANG_SWITCHER_XPATH)).click();
        String actualHeader = driver.findElement(By.xpath(CATEGORY_HEADER_XPATH)).getText();
        watch.stop();
        LOG.info("Actual result: " + actualHeader);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertNotEquals(expectedHeader, actualHeader);
    }

    @Test
    public void switchLangOlxWithExplicitWaitTest() {
        LOG.info("Test with Explicit wait started");
        driver.get(URL_OLX);
        LOG.info(URL_OLX + " is opening");
        LOG.info("Count started");
        watch.start();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(CATEGORY_HEADER_XPATH)));
        String expectedHeader = driver.findElement(By.xpath(CATEGORY_HEADER_XPATH)).getText();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(LANG_SWITCHER_XPATH)))
                .click();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CATEGORY_HEADER_XPATH)));
        String actualHeader = driver.findElement(By.xpath(CATEGORY_HEADER_XPATH)).getText();
        watch.stop();
        LOG.info("Actual result: " + actualHeader);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertNotEquals(expectedHeader, actualHeader);
    }

    @Test
    public void openWishListOlxWithImplicitWaitTest() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        LOG.info("Test with Implicit wait started");
        driver.get(URL_OLX);
        LOG.info(URL_OLX + " is opening");
        LOG.info("Count started");
        watch.start();
        driver.findElement(By.xpath(WISH_LIST_XPATH)).click();
        String message = driver.findElement(By.xpath(MESSAGE_HEADER_XPATH)).getText();
        watch.stop();
        LOG.info("Actual result: " + message);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertTrue(message.contains("Избранные"));
    }

    @Test
    public void openWishListOlxWithExplicitWaitTest() {
        LOG.info("Test with Explicit wait started");
        driver.get(URL_OLX);
        LOG.info(URL_OLX + " is opening");
        LOG.info("Count started");
        watch.start();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(WISH_LIST_XPATH)))
                .click();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MESSAGE_HEADER_XPATH)));
        String message = driver.findElement(By.xpath(MESSAGE_HEADER_XPATH)).getText();
        watch.stop();
        LOG.info("Actual result: " + message);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertTrue(message.contains("Избранные"));
    }


    @Test
    public void logInOlxWithEmptyEmailWithImplicitWaitTest() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        LOG.info("Test with Implicit wait started");
        driver.get(URL_OLX);
        LOG.info(URL_OLX + " is opening");
        LOG.info("Count started");
        watch.start();
        driver.findElement(By.xpath(PROFILE_BUTTON_XPATH)).click();
        driver.findElement(By.xpath(ENTER_BUTTON_XPATH)).click();
        boolean message = driver.findElement(By.xpath(ERROR_EMAIL_MESSAGE_XPATH)).isDisplayed();
        watch.stop();
        LOG.info("Actual result: " + message);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertTrue(message);
    }

    @Test
    public void logInOlxWithEmptyEmailWithExplicitWaitTest() {
        LOG.info("Test with Explicit wait started");
        driver.get(URL_OLX);
        LOG.info(URL_OLX + " is opening");
        LOG.info("Count started");
        watch.start();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(PROFILE_BUTTON_XPATH)))
                .click();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(ENTER_BUTTON_XPATH)))
                .click();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(ERROR_EMAIL_MESSAGE_XPATH)));
        boolean message = driver.findElement(By.xpath(ERROR_EMAIL_MESSAGE_XPATH)).isDisplayed();
        watch.stop();
        LOG.info("Actual result: " + message);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertTrue(message);
    }

    @Test
    public void addToWishlistOlxWithImplicitWaitTest() {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        LOG.info("Test with Implicit wait started");
        driver.get(URL_OLX);
        LOG.info(URL_OLX + " is opening");
        LOG.info("Count started");
        watch.start();
        driver.findElement(By.xpath(SEARCH_FIELD_XPATH)).sendKeys("nokia", Keys.ENTER);
        driver.findElements(By.xpath(ADD_TO_WISHLIST_XPATH)).get(0).click();
        boolean message = driver.findElement(By.xpath(MESSAGE_XPATH)).isDisplayed();
        watch.stop();
        LOG.info("Actual result: " + message);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertTrue(message);
    }

    @Test
    public void addToWishlistOlxWithExplicitWaitTest() {
        LOG.info("Test with Explicit wait started");
        driver.get(URL_OLX);
        LOG.info(URL_OLX + " is opening");
        LOG.info("Count started");
        watch.start();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(SEARCH_FIELD_XPATH)))
                .sendKeys("nokia", Keys.ENTER);
        new WebDriverWait(driver, 30)
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(ADD_TO_WISHLIST_XPATH)));
        driver.findElements(By.xpath(ADD_TO_WISHLIST_XPATH)).get(0).click();
        new WebDriverWait(driver, 30)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(MESSAGE_XPATH)));
        boolean message = driver.findElement(By.xpath(MESSAGE_XPATH)).isDisplayed();
        watch.stop();
        LOG.info("Actual result: " + message);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertTrue(message);
    }

    @Test
    public void openProductsListPageOlxWithImplicitWaitTest() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        LOG.info("Test with Implicit wait started");
        driver.get(URL_OLX_PRODUCT_LIST);
        LOG.info(URL_OLX_PRODUCT_LIST + " is opening");
        LOG.info("Count started");
        watch.start();
        String url = driver.getCurrentUrl();
        watch.stop();
        LOG.info("Actual result: " + url);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertEquals(URL_OLX_PRODUCT_LIST, url);
    }

    @Test
    public void openProductsListPageOlxWithExplicitWaitTest() {
        LOG.info("Test with Explicit wait started");
        driver.get(URL_OLX_PRODUCT_LIST);
        LOG.info(URL_OLX_PRODUCT_LIST + " is opening");
        LOG.info("Count started");
        watch.start();
        String url = driver.getCurrentUrl();
        watch.stop();
        LOG.info("Actual result: " + url);
        LOG.info("Wasted time = " + watch.getTime() + " milliseconds");
        assertEquals(URL_OLX_PRODUCT_LIST, url);
    }

    @AfterEach
    public void tearDown() {
        driver.close();
    }
}
