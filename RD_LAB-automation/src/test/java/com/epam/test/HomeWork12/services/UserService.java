package com.epam.test.HomeWork12.services;

import com.epam.test.HomeWork12.models.requests.createuser.CreateUserRequest;
import com.epam.test.HomeWork12.models.requests.updateuser.UpdateUserRequest;
import com.epam.test.HomeWork12.models.responses.createuser.CreateUserResponse;
import com.epam.test.HomeWork12.models.responses.createuserswitharray.CreateUsersWithArrayResponse;
import com.epam.test.HomeWork12.models.responses.createuserswithlist.CreateUsersWithListResponse;
import com.epam.test.HomeWork12.models.responses.deleteuser.DeleteUserResponse;
import com.epam.test.HomeWork12.models.responses.getuserbyusername.GetUserByUserNameResponse;
import com.epam.test.HomeWork12.models.responses.loginuser.LoginUserResponse;
import com.epam.test.HomeWork12.models.responses.logoutuser.LogoutUserResponse;
import com.epam.test.HomeWork12.models.responses.updateuser.UpdateUserResponse;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import java.util.List;

public class UserService {

    static final String API_KEY = "special-key";

    static final String BASE_URI = "https://petstore.swagger.io";

    static final String USER = "/v2/user/";

    static final String USERS_WITH_ARRAY = "/v2/user/createWithArray";

    static final String USERS_WITH_LIST = "/v2/user/createWithList";


    public GetUserByUserNameResponse getUserByUserNameRequest(String userName) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .when().get(BASE_URI + USER + userName)
                .then().log().all()
                .statusCode(200).extract()
                .as(GetUserByUserNameResponse.class);
    }

    public UpdateUserResponse updateUserByUserNameRequest(String userName, UpdateUserRequest newUser) {
        return RestAssured
                .given()
                .log().body().contentType("application/json")
                .body(newUser)
                .when().put(BASE_URI + USER + userName)
                .then().log().all()
                .statusCode(200).extract()
                .as(UpdateUserResponse.class);
    }

    public DeleteUserResponse deleteUserByNameRequest(String userName) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .queryParam("appid", API_KEY)
                .when().delete(BASE_URI + USER + userName)
                .then().log().all()
                .statusCode(200).extract()
                .as(DeleteUserResponse.class);
    }

    public CreateUserResponse createUserRequest(CreateUserRequest newUser) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .body(newUser)
                .when().post(BASE_URI + USER)
                .then().log().all()
                .statusCode(200).extract()
                .as(CreateUserResponse.class);
    }

    public LoginUserResponse loginUserRequest(String userName, String password) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .when().get(BASE_URI + USER + "login?username=" + userName + "&password=" + password)
                .then().log().all()
                .statusCode(200).extract()
                .as(LoginUserResponse.class);
    }

    public LogoutUserResponse logoutUserRequest() {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .when().get(BASE_URI + USER + "logout")
                .then().log().all()
                .statusCode(200).extract()
                .as(LogoutUserResponse.class);
    }

    public CreateUsersWithArrayResponse createUsersWithArrayRequest(CreateUserRequest[] createUsersRequestsArray) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .body(createUsersRequestsArray)
                .when().post(BASE_URI + USERS_WITH_ARRAY)
                .then().log().all()
                .statusCode(200).extract()
                .as(CreateUsersWithArrayResponse.class);
    }

    public CreateUsersWithListResponse createUsersWithListRequest(List<CreateUserRequest> createUsersRequestList) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .body(createUsersRequestList)
                .when().post(BASE_URI + USERS_WITH_LIST)
                .then().log().all()
                .statusCode(200).extract()
                .as(CreateUsersWithListResponse.class);
    }
}
