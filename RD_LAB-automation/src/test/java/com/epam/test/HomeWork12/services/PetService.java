package com.epam.test.HomeWork12.services;

import com.epam.test.HomeWork12.models.requests.addnewpet.AddNewPetRequest;
import com.epam.test.HomeWork12.models.responses.addnewpet.AddNewPetResponse;
import com.epam.test.HomeWork12.models.responses.deletexistpet.DeletePetResponse;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class PetService {

    static final String API_KEY = "special-key";

    static final String BASE_URI = "https://petstore.swagger.io";

    static final String PET = "/v2/pet/";

    public AddNewPetResponse addNewPetRequest(AddNewPetRequest newPet) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .body(newPet)
                .when().post(BASE_URI + PET)
                .then().log().all()
                .statusCode(200).extract()
                .as(AddNewPetResponse.class);
    }

    public AddNewPetResponse updateExistPetRequest(AddNewPetRequest newPet) {
        return RestAssured
                .given()
                .log().body().contentType("application/json")
                .body(newPet)
                .when().put(BASE_URI + PET)
                .then().log().all()
                .statusCode(200).extract()
                .as(AddNewPetResponse.class);
    }

    public AddNewPetResponse updateExistPetWithFormDataRequest(AddNewPetRequest newPet) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .body(newPet)
                .when().post(BASE_URI + PET)
                .then().log().all()
                .statusCode(200).extract()
                .as(AddNewPetResponse.class);
    }

    public AddNewPetResponse getExistPetByIdRequest(int existPet) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .when().get(BASE_URI + PET + existPet)
                .then().log().all()
                .statusCode(200).extract()
                .as(AddNewPetResponse.class);
    }

    public DeletePetResponse deleteExistPetByIdRequest(int existPet) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .queryParam("appid", API_KEY)
                .when().delete(BASE_URI + PET + existPet)
                .then().log().all()
                .statusCode(200).extract()
                .as(DeletePetResponse.class);
    }
}
