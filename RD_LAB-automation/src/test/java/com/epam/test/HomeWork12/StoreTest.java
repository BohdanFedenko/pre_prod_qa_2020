package com.epam.test.HomeWork12;

import com.epam.test.HomeWork12.models.requests.addnewpet.AddNewPetRequest;
import com.epam.test.HomeWork12.models.requests.addnewpet.Category;
import com.epam.test.HomeWork12.models.requests.addnewpet.TagsItem;
import com.epam.test.HomeWork12.models.requests.placeorderforpet.PlaceOrderForPetRequest;
import com.epam.test.HomeWork12.models.responses.deleteorder.DeleteOrderResponse;
import com.epam.test.HomeWork12.models.responses.findorder.FindOrderForPetResponse;
import com.epam.test.HomeWork12.models.responses.petinventoriesbystatus.PetInventoryResponse;
import com.epam.test.HomeWork12.models.responses.placeorderforpet.PlaceOrderForPetResponse;
import com.epam.test.HomeWork12.services.PetService;
import com.epam.test.HomeWork12.services.StoreService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StoreTest {

    static StoreService storeService = new StoreService();

    static PetService petService = new PetService();

    @BeforeAll
    public static void createPetAndStore() {
        AddNewPetRequest newPet = new AddNewPetRequest();
        newPet.setId(1100);
        newPet.setName("Rex");
        newPet.setCategory(new Category());
        newPet.setStatus("available");
        newPet.setTags(new ArrayList<TagsItem>() {
        });
        newPet.setPhotoUrls(new ArrayList<>());
        petService.addNewPetRequest(newPet);

        PlaceOrderForPetRequest orderForPet = new PlaceOrderForPetRequest();
        orderForPet.setId(1);
        orderForPet.setPetId(1100);
        orderForPet.setStatus("placed");
        orderForPet.setQuantity(0);
        orderForPet.setComplete(true);
        storeService.placeOrderForPetRequest(orderForPet);
    }

    @Test
    public void checkPlaceOrderForPetTest() {
        //GIVEN
        PlaceOrderForPetRequest orderForPet = new PlaceOrderForPetRequest();
        orderForPet.setId(3);
        orderForPet.setPetId(1100);
        orderForPet.setStatus("placed");
        orderForPet.setQuantity(0);
        orderForPet.setComplete(true);

        //WHEN
        PlaceOrderForPetResponse orderedPet = storeService.placeOrderForPetRequest(orderForPet);

        //THEN
        assertEquals(orderedPet.getId(), orderForPet.getId());
        assertEquals(orderedPet.getPetId(), orderForPet.getPetId());
        assertEquals(orderedPet.getStatus(), orderForPet.getStatus());
        assertEquals(orderedPet.getQuantity(), orderForPet.getQuantity());
        assertEquals(orderedPet.isComplete(), orderForPet.isComplete());
    }

    @Test
    public void checkGetOrderByPetIdTest() {
        //GIVEN
        int orderId = 2;

        //WHEN
        FindOrderForPetResponse orderForPet = storeService.getOrderByIdRequest(orderId);

        //THEN
        assertEquals(orderForPet.getId(), orderId);
    }

    @Test
    public void checkDeleteExistPetTest() {
        //GIVEN
        int orderId = 1;

        //WHEN
        DeleteOrderResponse deleteMessage = storeService.deleteOrderByOrderIdRequest(orderId);

        //THEN
        assertEquals(deleteMessage.getMessage(), "1");
    }

    @Test
    public void checkPetInventoryRequest() {
        //GIVEN

        //WHEN
        PetInventoryResponse petInventories = storeService.getPetInventoryRequest();

        //THEN
        assertEquals(petInventories.getAlive(), 0);
    }
}
