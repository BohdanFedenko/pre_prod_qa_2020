package com.epam.test.HomeWork12.models.responses.petinventoriesbystatus;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PetInventoryResponse {

    @JsonProperty("SOLD")
    private int sold;

    @JsonProperty("string")
    private int string;

    @JsonProperty("ALIVE")
    private int alive;

    @JsonProperty("PENDING")
    private int pending;

    @JsonProperty("pending")
    private int pendingTwo;

    @JsonProperty("Pending")
    private int pendingThree;

    @JsonProperty("available")
    private int available;

    @JsonProperty("AVAILABLE")
    private int availableTwo;

    @JsonProperty("Nonavailable")
    private int nonavailable;
}