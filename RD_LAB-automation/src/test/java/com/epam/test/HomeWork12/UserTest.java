package com.epam.test.HomeWork12;

import com.epam.test.HomeWork12.models.requests.createuser.CreateUserRequest;
import com.epam.test.HomeWork12.models.requests.updateuser.UpdateUserRequest;
import com.epam.test.HomeWork12.models.responses.createuser.CreateUserResponse;
import com.epam.test.HomeWork12.models.responses.createuserswitharray.CreateUsersWithArrayResponse;
import com.epam.test.HomeWork12.models.responses.createuserswithlist.CreateUsersWithListResponse;
import com.epam.test.HomeWork12.models.responses.deleteuser.DeleteUserResponse;
import com.epam.test.HomeWork12.models.responses.getuserbyusername.GetUserByUserNameResponse;
import com.epam.test.HomeWork12.models.responses.loginuser.LoginUserResponse;
import com.epam.test.HomeWork12.models.responses.logoutuser.LogoutUserResponse;
import com.epam.test.HomeWork12.models.responses.updateuser.UpdateUserResponse;
import com.epam.test.HomeWork12.services.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserTest {

    static final UserService userService = new UserService();

    @BeforeAll
    static void checkCreateUser() {
        CreateUserRequest newUser = new CreateUserRequest();
        newUser.setId(10);
        newUser.setUsername("TestUser");
        newUser.setFirstName("Test");
        newUser.setLastName("Test");
        userService.createUserRequest(newUser);

        CreateUserRequest newUser2 = new CreateUserRequest();
        newUser2.setId(12);
        newUser2.setUsername("TestUser4");
        newUser2.setFirstName("Test");
        newUser2.setLastName("Test");
        userService.createUserRequest(newUser2);

        CreateUserRequest newUser3 = new CreateUserRequest();
        newUser3.setId(800);
        newUser3.setUsername("TestUser11");
        newUser3.setFirstName("Test");
        newUser3.setLastName("Test");
        userService.createUserRequest(newUser3);
    }

    @Test
    public void checkCreateUserTest() {
        //GIVEN
        CreateUserRequest newUser = new CreateUserRequest();
        newUser.setId(11);
        newUser.setUsername("TestUser1");
        newUser.setFirstName("Test");
        newUser.setLastName("Test");

        //WHEN
        CreateUserResponse createdUser = userService.createUserRequest(newUser);

        //THEN
        assertEquals(createdUser.getMessage(), "" + newUser.getId() + "");
    }

    @Test
    public void checkCreateUsersWithArrayTest() {
        //GIVEN
        CreateUserRequest newUser1 = new CreateUserRequest();
        newUser1.setId(1000);
        newUser1.setUsername("TestUser5");
        newUser1.setFirstName("Test1");
        newUser1.setLastName("Test1");

        CreateUserRequest newUser2 = new CreateUserRequest();
        newUser2.setId(1001);
        newUser2.setUsername("TestUser6");
        newUser2.setFirstName("Test2");
        newUser2.setLastName("Test2");

        CreateUserRequest[] createUsersRequests = new CreateUserRequest[]{newUser1, newUser2};

        //WHEN
        CreateUsersWithArrayResponse createdUsersArray = userService.createUsersWithArrayRequest(createUsersRequests);

        //THEN
        assertEquals(createdUsersArray.getMessage(), "ok");
        assertEquals(createdUsersArray.getType(), "unknown");
    }

    @Test
    public void checkCreateUsersWithListTest() {
        //GIVEN
        CreateUserRequest newUser1 = new CreateUserRequest();
        newUser1.setId(1000);
        newUser1.setUsername("TestUser7");
        newUser1.setFirstName("Test1");
        newUser1.setLastName("Test1");

        CreateUserRequest newUser2 = new CreateUserRequest();
        newUser2.setId(1001);
        newUser2.setUsername("TestUser8");
        newUser2.setFirstName("Test2");
        newUser2.setLastName("Test2");

        List<CreateUserRequest> createUsersRequests = new ArrayList<>();
        createUsersRequests.add(newUser1);
        createUsersRequests.add(newUser2);

        //WHEN
        CreateUsersWithListResponse createdUsersList = userService.createUsersWithListRequest(createUsersRequests);

        //THEN
        assertEquals(createdUsersList.getMessage(), "ok");
        assertEquals(createdUsersList.getType(), "unknown");
    }

    @Test
    public void checkGetUserByUserNameTest() {
        //GIVEN
        String userName = "TestUser11";

        //WHEN
        GetUserByUserNameResponse user = userService.getUserByUserNameRequest(userName);

        //THEN
        assertEquals(user.getUsername(), userName);
    }

    @Test
    public void CheckUpdateUserByUserNameTest() {
        //GIVEN
        String userName = "TestUser5";
        UpdateUserRequest newUser = new UpdateUserRequest();
        newUser.setId(11);
        newUser.setUsername("TestUser Update");
        newUser.setFirstName("Test");
        newUser.setLastName("Test");

        //WHEN
        UpdateUserResponse updatedUser = userService.updateUserByUserNameRequest(userName, newUser);

        //THEN
        assertEquals(updatedUser.getType(), "unknown");
    }

    @Test
    public void checkDeleteUserByUserNameTest() {
        //GIVEN
        String userName = "TestUser";

        //WHEN
        DeleteUserResponse deleteMessage = userService.deleteUserByNameRequest(userName);

        //THEN
        assertEquals(deleteMessage.getType(), "unknown");
    }

    @Test
    public void checkLoginUserTest() {
        //GIVEN
        String userName = "Test User";
        String password = "qwerty";

        //WHEN
        LoginUserResponse loginMessage = userService.loginUserRequest(userName, password);

        //THEN
        assertEquals(loginMessage.getType(), "unknown");
    }

    @Test
    public void checkLogoutUserTest() {
        //GIVEN

        //WHEN
        LogoutUserResponse logoutMessage = userService.logoutUserRequest();

        //THEN
        assertEquals(logoutMessage.getMessage(), "ok");
    }
}
