package com.epam.test.HomeWork12.services;

import com.epam.test.HomeWork12.models.requests.placeorderforpet.PlaceOrderForPetRequest;
import com.epam.test.HomeWork12.models.responses.deleteorder.DeleteOrderResponse;
import com.epam.test.HomeWork12.models.responses.findorder.FindOrderForPetResponse;
import com.epam.test.HomeWork12.models.responses.petinventoriesbystatus.PetInventoryResponse;
import com.epam.test.HomeWork12.models.responses.placeorderforpet.PlaceOrderForPetResponse;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class StoreService {

    static final String BASE_URI = "https://petstore.swagger.io";

    static final String ORDER = "/v2/store/order/";

    public PlaceOrderForPetResponse placeOrderForPetRequest(PlaceOrderForPetRequest placeOrderForPetRequest) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .body(placeOrderForPetRequest)
                .when().post(BASE_URI + ORDER)
                .then().log().all()
                .statusCode(200).extract()
                .as(PlaceOrderForPetResponse.class);
    }

    public FindOrderForPetResponse getOrderByIdRequest(int orderId) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .when().get(BASE_URI + ORDER + orderId)
                .then().log().all()
                .statusCode(200).extract()
                .as(FindOrderForPetResponse.class);
    }

    public DeleteOrderResponse deleteOrderByOrderIdRequest(int orderId) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .when().delete(BASE_URI + ORDER + orderId)
                .then().log().all()
                .statusCode(200).extract()
                .as(DeleteOrderResponse.class);
    }

    public PetInventoryResponse getPetInventoryRequest() {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .when().get(BASE_URI + "/v2/store/inventory")
                .then().log().all()
                .statusCode(200).extract()
                .as(PetInventoryResponse.class);
    }
}
