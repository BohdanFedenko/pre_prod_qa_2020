package com.epam.test.HomeWork12.models.responses.addnewpet;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Category {

    @JsonProperty("name")
    private String name;

    @JsonProperty("id")
    private int id;
}