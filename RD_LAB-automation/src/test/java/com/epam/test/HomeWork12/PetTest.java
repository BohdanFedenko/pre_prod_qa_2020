package com.epam.test.HomeWork12;

import com.epam.test.HomeWork12.models.requests.addnewpet.AddNewPetRequest;
import com.epam.test.HomeWork12.models.requests.addnewpet.Category;
import com.epam.test.HomeWork12.models.requests.addnewpet.TagsItem;
import com.epam.test.HomeWork12.models.responses.addnewpet.AddNewPetResponse;
import com.epam.test.HomeWork12.models.responses.deletexistpet.DeletePetResponse;
import com.epam.test.HomeWork12.services.PetService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PetTest {

    static PetService petService = new PetService();

    @BeforeAll
    public static void createPet() {
        AddNewPetRequest newPet = new AddNewPetRequest();
        newPet.setId(1000);
        newPet.setName("Rex");
        newPet.setCategory(new Category());
        newPet.setStatus("available");
        newPet.setTags(new ArrayList<TagsItem>() {
        });
        newPet.setPhotoUrls(new ArrayList<>());
        petService.addNewPetRequest(newPet);
    }

    @Test
    public void checkAddNewPetTest() {
        //GIVEN
        AddNewPetRequest newPet = new AddNewPetRequest();
        newPet.setId(1001);
        newPet.setName("Rex Create");
        newPet.setCategory(new Category());
        newPet.setStatus("available");
        newPet.setTags(new ArrayList<TagsItem>() {
        });
        newPet.setPhotoUrls(new ArrayList<>());

        //WHEN
        AddNewPetResponse addedPet = petService.addNewPetRequest(newPet);

        //THEN
        assertEquals(addedPet.getName(), newPet.getName());
        assertEquals(addedPet.getStatus(), newPet.getStatus());
        assertEquals(addedPet.getPhotoUrls(), newPet.getPhotoUrls());
    }

    @Test
    public void checkUpdateExistPetTest() {
        //GIVEN
        AddNewPetRequest newPet = new AddNewPetRequest();
        newPet.setId(1000);
        newPet.setName("Rex Update");
        newPet.setCategory(new Category());
        newPet.setStatus("available");
        newPet.setTags(new ArrayList<TagsItem>() {
        });
        newPet.setPhotoUrls(new ArrayList<>());

        //WHEN
        AddNewPetResponse updatedPet = petService.updateExistPetRequest(newPet);

        //THEN
        assertEquals(updatedPet.getName(), newPet.getName());
        assertEquals(updatedPet.getStatus(), newPet.getStatus());
        assertEquals(updatedPet.getPhotoUrls(), newPet.getPhotoUrls());
    }

    @Test
    public void checkUpdateExistPetWithFormDataTest() {
        //GIVEN
        AddNewPetRequest newPet = new AddNewPetRequest();
        newPet.setId(1001);
        newPet.setName("Rex Created Update");
        newPet.setStatus("available");

        //WHEN
        AddNewPetResponse updatedPet = petService.updateExistPetWithFormDataRequest(newPet);

        //THEN
        assertEquals(updatedPet.getName(), newPet.getName());
        assertEquals(updatedPet.getStatus(), newPet.getStatus());
    }

    @Test
    public void checkGetExistPetTest() {
        //GIVEN
        int existPetId = 1000;

        //WHEN
        AddNewPetResponse existPet = petService.getExistPetByIdRequest(existPetId);

        //THEN
        assertEquals(existPet.getName(), "Rex Update");
    }

    @Test
    public void checkDeleteExistPetTest() {
        //GIVEN
        int existPetId = 1000;

        //WHEN
        DeletePetResponse deleteMessage = petService.deleteExistPetByIdRequest(existPetId);

        //THEN
        assertEquals(deleteMessage.getMessage(), "1000");
    }
}
