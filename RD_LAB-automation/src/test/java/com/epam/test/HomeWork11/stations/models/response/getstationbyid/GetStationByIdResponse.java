package com.epam.test.HomeWork11.stations.models.response.getstationbyid;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetStationByIdResponse {

    @JsonProperty("altitude")
    private int altitude;

    @JsonProperty("updated_at")
    private String updatedAt;

    @JsonProperty("latitude")
    private double latitude;

    @JsonProperty("name")
    private String name;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("rank")
    private int rank;

    @JsonProperty("external_id")
    private String externalId;

    @JsonProperty("id")
    private String id;

    @JsonProperty("longitude")
    private double longitude;
}