package com.epam.test.HomeWork11.calculator.services;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.tempuri.*;

public class CalculatorService {

    static public String BASE_URI = "http://www.dneonline.com/calculator.asmx?op=";

    public AddResponse addRequest(Add add) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.XML).log().all()
                .body(add)
                .when().post(BASE_URI, "Add")
                .then().log().all().statusCode(200).extract()
                .as(AddResponse.class);
    }

    public SubtractResponse subtractRequest(Subtract subtract) {
        return RestAssured
                .given()
                .contentType(ContentType.XML).log().all()
                .body(subtract)
                .when().post(BASE_URI + "Subtract")
                .then().log().all().statusCode(200).extract()
                .as(SubtractResponse.class);
    }

    public DivideResponse divideRequest(Divide divide) {
        return RestAssured
                .given()
                .contentType(ContentType.XML).log().all()
                .body(divide)
                .when().post(BASE_URI + "Divide")
                .then().log().all().statusCode(200).extract()
                .as(DivideResponse.class);
    }

    public MultiplyResponse multiplyRequest(Multiply multiply) {
        return RestAssured
                .given()
                .contentType(ContentType.XML).log().all()
                .body(multiply)
                .when().post(BASE_URI + "Multiply")
                .then().log().all().statusCode(200).extract()
                .as(MultiplyResponse.class);
    }
}

