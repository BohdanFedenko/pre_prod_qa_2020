package com.epam.test.HomeWork11.stations;

import com.epam.test.HomeWork11.stations.models.requests.registeredstation.RegisterStationRequest;
import com.epam.test.HomeWork11.stations.models.response.getstationbyid.GetStationByIdResponse;
import com.epam.test.HomeWork11.stations.models.response.registeredstation.RegisterStationResponse;
import com.epam.test.HomeWork11.stations.models.response.updatestation.UpdateStationResponse;
import com.epam.test.HomeWork11.stations.services.OpenWeatherStationsService;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StationTest {

    static OpenWeatherStationsService openWeatherStationsService = new OpenWeatherStationsService();

    @Test
    public void registerStationTest() {
        //GIVEN
        RegisterStationRequest registerStationRequest = openWeatherStationsService.createStation("Test6",
                "Test_Key6", 35.0, 37.0, 150);

        //WHEN
        RegisterStationResponse response = openWeatherStationsService.registerStation(registerStationRequest);

        //THEN
        assertEquals(response.getName(), registerStationRequest.getName());
        assertEquals(response.getExternalId(), registerStationRequest.getExternalId());
        assertEquals(response.getLatitude(), registerStationRequest.getLatitude());
        assertEquals(response.getLongitude(), registerStationRequest.getLongitude());
        assertEquals(response.getAltitude(), registerStationRequest.getAltitude());
    }

    @Test
    public void getStationTest() {
        //GIVEN
        String existStationId = "5f528980cca8ce0001ef5902";

        //WHEN
        GetStationByIdResponse actualStation = openWeatherStationsService.getStationByStationId(existStationId);

        //THEN
        assertEquals(actualStation.getName(), "Test7");
    }

    @Test
    public void updateStationTest() {
        //GIVEN
        String existedStation = "5f528980cca8ce0001ef5902";
        RegisterStationRequest updateStation = openWeatherStationsService.createStation("Test7",
                "Test_Key7", 35.0, 37.0, 150);
        //WHEN
        UpdateStationResponse updatedStation = openWeatherStationsService.updateStation(existedStation, updateStation);

        //THEN
        assertEquals(updatedStation.getName(), updateStation.getName());
        assertEquals(updatedStation.getExternalId(), updateStation.getExternalId());
        assertEquals(updatedStation.getLatitude(), updateStation.getLatitude());
        assertEquals(updatedStation.getLongitude(), updateStation.getLongitude());
        assertEquals(updatedStation.getAltitude(), updateStation.getAltitude());
    }

    @Test
    public void deleteStationTest() {
        //GIVEN
        RegisterStationRequest registerStationRequest = openWeatherStationsService.createStation("TestForDelete",
                "Test_Key6", 35.0, 37.0, 150);
        RegisterStationResponse existStation = openWeatherStationsService.registerStation(registerStationRequest);

        //WHEN
        openWeatherStationsService.deleteStation(existStation.getId());
    }
}
