package com.epam.test.HomeWork11.stations.services;

import com.epam.test.HomeWork11.stations.models.requests.registeredstation.RegisterStationRequest;
import com.epam.test.HomeWork11.stations.models.response.getstationbyid.GetStationByIdResponse;
import com.epam.test.HomeWork11.stations.models.response.registeredstation.RegisterStationResponse;
import com.epam.test.HomeWork11.stations.models.response.updatestation.UpdateStationResponse;
import com.openweathermap.POSTMeasurementsRequestSchema;
import com.openweathermap.POSTStationsResponseSchema;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class OpenWeatherStationsService {

    public static final String BASE_STATIONS_URI = "http://api.openweathermap.org/data/3.0/stations";

    public static final String BASE_MEASUREMENTS_URI = "http://api.openweathermap.org/data/3.0/measurements";

    public static final String API_KEY = "864c5c739f4fa7b8f7604ff697cc5013";

    public RegisterStationRequest createStation(String name, String externalId, double latitude,
                                                double longitude, int altitude) {
        RegisterStationRequest registerStationRequest = new RegisterStationRequest();
        registerStationRequest.setName(name);
        registerStationRequest.setExternalId(externalId);
        registerStationRequest.setLatitude(latitude);
        registerStationRequest.setLongitude(longitude);
        registerStationRequest.setAltitude(altitude);
        return registerStationRequest;
    }

    public GetStationByIdResponse getStationByStationId(String stationId) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .queryParam("appid", API_KEY)
                .when().get(BASE_STATIONS_URI + "/" + stationId)
                .then().log().all().statusCode(200).extract()
                .as(GetStationByIdResponse.class);
    }

    public RegisterStationResponse registerStation(RegisterStationRequest registerStationRequest) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .queryParam("appid", API_KEY)
                .body(registerStationRequest)
                .when().post(BASE_STATIONS_URI)
                .then().log().all().statusCode(201).extract()
                .as(RegisterStationResponse.class);
    }

    public UpdateStationResponse updateStation(String id, RegisterStationRequest updateStation) {
        return RestAssured
                .given()
                .log().body().contentType("application/json")
                .queryParam("appid", API_KEY)
                .body(updateStation)
                .when().put(BASE_STATIONS_URI + "/" + id)
                .then().log().all().statusCode(200).extract()
                .as(UpdateStationResponse.class);
    }

    public void deleteStation(String id) {
        RestAssured
                .given()
                .log().body().contentType("application/json")
                .queryParam("appid", API_KEY)
                .when().delete(BASE_STATIONS_URI + "/" + id)
                .then().log().all().statusCode(204);
    }


    public POSTStationsResponseSchema createMeasurement(POSTMeasurementsRequestSchema createMeasurementRequest) {
        return RestAssured
                .given()
                .header("Content-Type", ContentType.JSON).log().all()
                .queryParam("appid", API_KEY)
                .body(createMeasurementRequest)
                .when().post(BASE_MEASUREMENTS_URI)
                .then().log().all().statusCode(204).extract()
                .as(POSTStationsResponseSchema.class);
    }
}
