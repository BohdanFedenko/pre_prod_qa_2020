package com.epam.test.HomeWork11.currentWeather.models;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@Setter
@Getter
@XmlAccessorType(XmlAccessType.FIELD)
public class GeographicCoordinates {

    @XmlAttribute
    private String lat;

    @XmlAttribute
    private String lon;
}
