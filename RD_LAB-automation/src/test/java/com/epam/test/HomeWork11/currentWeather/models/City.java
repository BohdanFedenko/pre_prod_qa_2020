package com.epam.test.HomeWork11.currentWeather.models;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@Setter
@Getter
@XmlAccessorType(XmlAccessType.FIELD)
public class City {

    @XmlAttribute
    private String id;

    @XmlAttribute
    private String name;

    @XmlElement(name = "coord")
    private GeographicCoordinates geographicCoordinates;
}
