package com.epam.test.HomeWork11.stations.models.requests.registeredstation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterStationRequest {

    @JsonProperty("altitude")
    private int altitude;

    @JsonProperty("latitude")
    private double latitude;

    @JsonProperty("name")
    private String name;

    @JsonProperty("external_id")
    private String externalId;

    @JsonProperty("longitude")
    private double longitude;
}