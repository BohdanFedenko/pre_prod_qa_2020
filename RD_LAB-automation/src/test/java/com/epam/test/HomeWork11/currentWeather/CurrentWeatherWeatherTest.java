package com.epam.test.HomeWork11.currentWeather;

import com.epam.test.HomeWork11.currentWeather.models.CurrentWeather;
import com.epam.test.HomeWork11.currentWeather.services.OpenWeatherCurrentWeatherService;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CurrentWeatherWeatherTest {

    OpenWeatherCurrentWeatherService openWeatherCurrentWeatherService = new OpenWeatherCurrentWeatherService();

    @Test
    public void checkResponseByCityNameTest() {
        //GIVEN
        String cityName = "Paris";

        //WHEN
        CurrentWeather existCity = openWeatherCurrentWeatherService.getResponseByCityName(cityName);

        //THEN
        assertEquals(existCity.getCity().getName(), cityName);
    }

    @Test
    public void checkResponseByGeographicCoordinatesTest() {
        //GIVEN
        int lon = 139;
        int lat = 35;

        //WHEN
        CurrentWeather existCity = openWeatherCurrentWeatherService.getResponseByGeographicCoordinatesTest(lon, lat);

        //THEN
        assertEquals(existCity.getCity().getName(), "Shuzenji");
    }

    @Test
    public void checkResponseByCityIdTest() {
        //GIVEN
        String cityId = "2988507";

        //WHEN
        CurrentWeather existCity = openWeatherCurrentWeatherService.getResponseByCityId(cityId);

        //THEN
        assertEquals(existCity.getCity().getName(), "Paris");
    }

    @Test
    public void checkResponseByZipCodeTest() {
        //GIVEN
        String zipCode = "94040";

        //WHEN
        CurrentWeather existCity = openWeatherCurrentWeatherService.getResponseByZipCodeTest(zipCode);

        //THEN
        assertEquals(existCity.getCity().getName(), "Mountain View");
    }
}
