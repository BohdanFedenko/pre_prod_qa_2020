package com.epam.test.HomeWork11.calculator;

import com.epam.test.HomeWork11.calculator.services.CalculatorService;
import org.junit.Test;
import org.tempuri.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {

    CalculatorService calculatorService = new CalculatorService();

    @Test
    public void checkAddTest() {
        //GIVEN
        Add add = new ObjectFactory().createAdd();
        add.setIntA(10);
        add.setIntB(10);

        //WHEN
        AddResponse addResult = calculatorService.addRequest(add);

        //THEN
        assertEquals(addResult.getAddResult(), add.getIntA() + add.getIntB());
    }

    @Test
    public void checkSubtractTest() {
        //GIVEN
        Subtract subtract = new ObjectFactory().createSubtract();
        subtract.setIntA(10);
        subtract.setIntB(10);

        //WHEN
        SubtractResponse subtractResult = calculatorService.subtractRequest(subtract);

        //THEN
        assertEquals(subtractResult.getSubtractResult(), subtract.getIntA() - subtract.getIntB());
    }

    @Test
    public void checkDivideTest() {
        //GIVEN
        Divide divide = new ObjectFactory().createDivide();
        divide.setIntA(10);
        divide.setIntB(10);

        //WHEN
        DivideResponse divideResult = calculatorService.divideRequest(divide);

        //THEN
        assertEquals(divideResult.getDivideResult(), divide.getIntA() / divide.getIntB());
    }

    @Test
    public void checkMultiplyTest() {
        //GIVEN
        Multiply multiply = new ObjectFactory().createMultiply();
        multiply.setIntA(10);
        multiply.setIntB(10);

        //WHEN
        MultiplyResponse multiplyResult = calculatorService.multiplyRequest(multiply);

        //THEN
        assertEquals(multiplyResult.getMultiplyResult(), multiply.getIntA() * multiply.getIntB());
    }
}
