package com.epam.test.HomeWork11.currentWeather.services;

import com.epam.test.HomeWork11.currentWeather.models.CurrentWeather;
import io.restassured.RestAssured;

public class OpenWeatherCurrentWeatherService {

    public static final String BASE_URI = "http://api.openweathermap.org/data/2.5/weather";

    public static final String API_KEY = "864c5c739f4fa7b8f7604ff697cc5013";

    public CurrentWeather getResponseByCityName(String cityName) {
        return RestAssured
                .given()
                .baseUri(BASE_URI)
                .queryParam("q", cityName)
                .queryParam("appid", API_KEY)
                .queryParam("mode", "xml").log().all()
                .when().get()
                .then().log().all().statusCode(200).extract()
                .as(CurrentWeather.class);
    }

    public CurrentWeather getResponseByCityId(String cityId) {
        return RestAssured
                .given()
                .baseUri(BASE_URI)
                .queryParam("id", cityId)
                .queryParam("appid", API_KEY)
                .queryParam("mode", "xml").log().all()
                .when().get()
                .then().log().all().statusCode(200).extract()
                .as(CurrentWeather.class);
    }

    public CurrentWeather getResponseByGeographicCoordinatesTest(int lon, int lat) {
        return RestAssured
                .given()
                .baseUri(BASE_URI)
                .queryParam("lon", lon)
                .queryParam("lat", lat)
                .queryParam("appid", API_KEY)
                .queryParam("mode", "xml").log().all()
                .when().get()
                .then().log().all().statusCode(200).extract()
                .as(CurrentWeather.class);
    }

    public CurrentWeather getResponseByZipCodeTest(String zipCode) {
        return RestAssured
                .given()
                .baseUri(BASE_URI)
                .queryParam("zip", zipCode)
                .queryParam("appid", API_KEY)
                .queryParam("mode", "xml").log().all()
                .when().get()
                .then().log().all().statusCode(200).extract()
                .as(CurrentWeather.class);
    }
}
