package com.epam.test.HomeWork11.stations.models.response.updatestation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UpdateStationResponse {

    @JsonProperty("altitude")
    private int altitude;

    @JsonProperty("updated_at")
    private String updatedAt;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("latitude")
    private double latitude;

    @JsonProperty("name")
    private String name;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("external_id")
    private String externalId;

    @JsonProperty("source_type")
    private int sourceType;

    @JsonProperty("id")
    private String id;

    @JsonProperty("longitude")
    private double longitude;

    @JsonProperty("rank")
    private int rank;
}
