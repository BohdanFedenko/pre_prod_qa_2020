package com.epam.test.HomeWork14.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SeleniumWebSiteHomePage extends BasePage {

    private static final String HEADER = "//h3[@class='webdriver-header']";

    public SeleniumWebSiteHomePage(WebDriver driver) {
        super(driver);
    }

    public String getHeader() {
        return driver.findElement(By.xpath(HEADER)).getText();
    }
}
