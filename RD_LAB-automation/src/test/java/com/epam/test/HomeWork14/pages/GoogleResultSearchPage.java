package com.epam.test.HomeWork14.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GoogleResultSearchPage extends BasePage {

    private static final String LINK_TO_SELENIUM = "//div[@class='TbwUpd NJjxre']//cite[@class='iUh30 gBIQub " +
            "tjvcx'][contains(text(),'selenium')]";

    public GoogleResultSearchPage(WebDriver driver) {
        super(driver);
    }

    public SeleniumWebSiteHomePage getFirstResult() {
        driver.findElement(By.xpath(LINK_TO_SELENIUM)).click();
        return new SeleniumWebSiteHomePage(driver);
    }
}
