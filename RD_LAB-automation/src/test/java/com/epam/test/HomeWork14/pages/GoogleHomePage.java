package com.epam.test.HomeWork14.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class GoogleHomePage extends BasePage {

    private static final String SEARCH_FIELD_NAME = "q";

    public GoogleHomePage(WebDriver driver) {
        super(driver);
    }

    public GoogleResultSearchPage search(String searchWord) {
        driver.findElement(By.name(SEARCH_FIELD_NAME)).sendKeys(searchWord, Keys.ENTER);
        return new GoogleResultSearchPage(driver);
    }
}
