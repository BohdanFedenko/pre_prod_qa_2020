package com.epam.test.HomeWork14;

import com.epam.test.HomeWork14.pages.GoogleHomePage;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SeleniumSearchTest {

    static WebDriver driver;

    static final String URL = "https://www.google.com/";

    @BeforeAll
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(URL);
    }

    @Test
    public void findSeleniumWebSiteTest() {
        //GIVEN
        GoogleHomePage googleHomePage = new GoogleHomePage(driver);
        String expectedTitle = "Selenium";

        //WHEN
        String actualTitle = googleHomePage.search("selenium").getFirstResult().getHeader();

        //THEN
        assertTrue(actualTitle.contains(expectedTitle));
    }

    @AfterAll
    public static void tearDown() {
        driver.close();
    }
}
