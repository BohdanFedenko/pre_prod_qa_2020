package com.epam.test.HomeWork21.stepdefs;

import com.epam.test.HomeWork21.pages.ProductListPage;
import cucumber.api.java.en.Then;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.page;

public class OlxProductListPageStepDefs {

    ProductListPage productListPage = page(ProductListPage.class);

    @Then("product list page is opened")
    public void productListPageIsOpened() {
        Assertions.assertTrue(productListPage.isOfferType());
    }
}
