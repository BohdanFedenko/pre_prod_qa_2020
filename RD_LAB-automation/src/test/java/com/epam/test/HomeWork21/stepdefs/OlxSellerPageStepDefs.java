package com.epam.test.HomeWork21.stepdefs;

import com.epam.test.HomeWork21.pages.SellerPage;
import cucumber.api.java.en.Then;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.page;

public class OlxSellerPageStepDefs {

    SellerPage sellerPage = page(SellerPage.class);

    @Then("seller page is opened")
    public void sellerPageIsOpened() {
        Assertions.assertTrue(sellerPage.isPageLoaded());
    }
}
