package com.epam.test.HomeWork21.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class HomePage {

    @FindBy(xpath = "//div[@class = 'li fleft']")
    private SelenideElement mainCategorySection;

    @FindBy(xpath = "//*[@id='cityFieldError']/p")
    private SelenideElement cityErrorMessage;

    @FindBy(xpath = "//a[@class='x-normal']")
    private SelenideElement anotherLang;

    @FindBy(xpath = "//a[@id='postNewAdLink']")
    private SelenideElement putOffer;

    @FindBy(id = "headerSearch")
    private SelenideElement searchField;

    @FindBy(id = "cityField")
    private SelenideElement cityField;

    @FindBy(xpath = "//div[@class='rel fleft input-container']//li[2]//a[1]")
    private SelenideElement firstCityOfCitiesList;

    @FindBy(id = "submit-searchmain")
    private SelenideElement searchButton;

    @FindBy(css = "div[class='li fleft'] a:first-child")
    private ElementsCollection categoriesItems;

    @FindBy(xpath = "//div[@class = 'subcategories-title']")
    private ElementsCollection categoryItems;

    @FindBy(xpath = "//div[@class = 'maincategories']//h3")
    private SelenideElement categoriesBlockTitle;

    public String getTitle() {
        return categoriesBlockTitle.getText();
    }

    public RegistrationPage putOffer() {
        putOffer.click();
        return page(RegistrationPage.class);
    }

    public HomePage switchLang() {
        anotherLang.click();
        return page(this);
    }

    public String getCategoriesTitle() {
        return categoriesBlockTitle.getText();
    }

    public SearchResultPage search(String text) {
        searchField.clear();
        searchField.val(text);
        searchButton.click();
        return page(SearchResultPage.class);
    }

    public SearchResultPage search(String text, String cityName) {
        searchField.clear();
        searchField.val(text);
        cityField.val(cityName);
        firstCityOfCitiesList.click();
        searchButton.click();
        return page(SearchResultPage.class);
    }

    public SearchResultPage searchFromHomePage(String text, String cityName) {
        searchField.clear();
        searchField.val(text);
        cityField.val(cityName);
        searchButton.click();
        return page(SearchResultPage.class);
    }

    public boolean isCityErrorMessage() {
        return cityErrorMessage.isDisplayed();
    }

    public boolean isPageLoaded() {
        return mainCategorySection.isDisplayed();
    }

    public HomePage openCategory(String categoryName) {
        for (SelenideElement element : categoriesItems) {
            if (element.getText().equals(categoryName)) {
                element.click();
                openCategoryItem(categoryName);
                break;
            }
        }
        return page(this);
    }

    private void openCategoryItem(String categoryItemName) {
        for (SelenideElement element : categoryItems) {
            if (element.getText().contains(categoryItemName)) {
                element.click();
                break;
            }
        }
    }
}
