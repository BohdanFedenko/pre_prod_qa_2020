package com.epam.test.HomeWork21.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

public class SellerPage {

    @FindBy(xpath = "//h2[@class='search-user-profile__user-name']")
    private SelenideElement pageTitle;

    public boolean isPageLoaded() {
        return pageTitle.isDisplayed();
    }
}
