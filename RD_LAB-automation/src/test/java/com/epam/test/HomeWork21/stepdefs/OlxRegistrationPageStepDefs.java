package com.epam.test.HomeWork21.stepdefs;

import com.epam.test.HomeWork21.pages.RegistrationPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.WebDriverRunner.url;

public class OlxRegistrationPageStepDefs {

    RegistrationPage registrationPage = page(RegistrationPage.class);

    @When("click submit button")
    public void clickSubmitButton() {
        registrationPage.enter();
    }

    @Then("error message is displayed below both fields")
    public void errorMessageIsDisplayedBelowBothFields() {
        Assertions.assertTrue(registrationPage.isErrorMessage());
    }

    @Then("registration page is opened")
    public void registrationPageIsOpened() {
        Assertions.assertTrue(url().contains("account"));
    }
}
