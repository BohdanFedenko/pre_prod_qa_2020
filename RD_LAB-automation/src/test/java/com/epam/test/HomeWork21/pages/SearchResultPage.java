package com.epam.test.HomeWork21.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.WebDriverRunner.url;

public class SearchResultPage {

    private static final String URL = "https://www.olx.ua/list/";

    @FindBy(xpath = "//table[@id='offers_table']")
    private SelenideElement resultList;

    @FindBy(id = "search-text")
    private SelenideElement searchField;

    @FindBy(id = "cityField")
    private SelenideElement cityField;

    @FindBy(xpath = "//div[@class='rel fleft input-container']//li[2]//a[1]")
    private SelenideElement firstCityOfCitiesList;

    @FindBy(id = "search-submit")
    private SelenideElement searchButton;

    @FindBy(xpath = "//a[contains(@class, 'detailsLink')]/strong")
    private ElementsCollection productCards;

    public void openPage() {
        open(URL);
    }

    public SearchResultPage search(String text) {
        searchField.click();
        searchField.val(text).pressEnter();
        searchButton.click();
        return page(this);
    }

    public SearchResultPage search(String text, String cityName) {
        searchField.clear();
        searchField.val(text);
        cityField.val(cityName);
        firstCityOfCitiesList.waitUntil(Condition.appear, 30).click();
        searchButton.click();
        return page(SearchResultPage.class);
    }

    public ProductPage openProduct() {
        productCards.get(10).click();
        return page(ProductPage.class);
    }

    public boolean isCurrentPage() {
        return url().contains(URL);
    }

    public boolean isPageLoaded() {
        return resultList.isDisplayed();
    }
}
