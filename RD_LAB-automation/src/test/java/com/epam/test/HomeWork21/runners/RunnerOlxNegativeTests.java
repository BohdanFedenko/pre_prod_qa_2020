package com.epam.test.HomeWork21.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"json:target/olxtests/olxnegativetestsreport/cucumber-report.json"},
        features = {"src/test/resources/features"},
        glue = {"com/epam/test/HomeWork21/stepdefs", "com/epam/test/HomeWork21/hooks"},
        tags = {"@negative"}
)
public class RunnerOlxNegativeTests {
}
