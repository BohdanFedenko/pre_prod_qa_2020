package com.epam.test.HomeWork21.hooks;

import com.epam.test.HomeWork21.utils.TestConfigurator;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class TestRunnerHelper {

    TestConfigurator testConfigurator;

    @Before
    public void setUp() {
        testConfigurator = new TestConfigurator();
        testConfigurator.openDriver();
    }

    @After
    public void tearDown() {
        testConfigurator.closeDriver();
    }
}
