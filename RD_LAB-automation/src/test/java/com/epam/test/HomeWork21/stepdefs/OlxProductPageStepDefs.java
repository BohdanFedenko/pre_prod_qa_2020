package com.epam.test.HomeWork21.stepdefs;

import com.epam.test.HomeWork21.pages.ProductPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.page;

public class OlxProductPageStepDefs {

    ProductPage productPage = page(ProductPage.class);

    @When("send message to seller with empty email field")
    public void sendMessageToSellerWithEmptyEmailField() {
        productPage.sendMessageToSeller();
    }

    @When("make empty complain")
    public void makeEmptyComplain() {
        productPage.putComplain();
        productPage.submitComplainForm();
    }

    @When("open products by offer type")
    public void openProductsByType() {
        productPage.openOfferType();
    }

    @And("open seller page")
    public void openSellerPage() {
        productPage.openSellerPage();
    }

    @Then("error message below email field is displayed")
    public void errorMessageBelowEmailFieldIsDisplayed() {
        Assertions.assertTrue(productPage.isSentWithEmptyEmailErrorMessage());
    }

    @Then("error message below text field is displayed")
    public void errorMessageBelowTextFieldIsDisplayed() {
        Assertions.assertTrue(productPage.isSentWithEmptyTextBoxErrorMessage());
    }

    @Then("product page is opened")
    public void productPageIsOpened() {
        Assertions.assertTrue(productPage.isPageLoaded());
    }

    @Then("shown error message")
    public void shownErrorMessage() {
        Assertions.assertTrue(productPage.isErrorMessage());
    }
}
