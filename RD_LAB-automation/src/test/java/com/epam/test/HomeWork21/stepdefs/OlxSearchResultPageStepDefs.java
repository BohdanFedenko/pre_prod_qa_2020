package com.epam.test.HomeWork21.stepdefs;

import com.epam.test.HomeWork21.pages.SearchResultPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.page;

public class OlxSearchResultPageStepDefs {

    SearchResultPage searchResultPage = page(SearchResultPage.class);

    @Given("I navigate to olx product list page")
    public void iNavigateToOlxProductListPage() {
        searchResultPage.openPage();
    }

    @When("open one of the all pages")
    public void openOneOfTheAllPages() {
        searchResultPage.openProduct();
    }

    @When("I entering {string} to search field of page and submit")
    public void iEnteringToSearchFieldOfPageAndSubmit(String productName) {
        searchResultPage.search(productName);
    }

    @Then("results are displayed on the screen")
    public void resultsAreDisplayedOnTheScreen() {
        Assertions.assertTrue(searchResultPage.isPageLoaded());
    }

    @Then("result search page is opened")
    public void resultSearchPageIsOpened() {
        searchResultPage.isCurrentPage();
    }
}
