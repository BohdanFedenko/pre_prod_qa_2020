package com.epam.test.HomeWork21.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

public class ProductListPage {

    @FindBy(xpath = "//li[@class='fleft']//span[@class = 'fbold']")
    private SelenideElement offerType;

    @FindBy(xpath = "//h1[@class = 'small fnormal inline lheight18']")
    private SelenideElement categoryName;

    public boolean isOfferType() {
        return offerType.isDisplayed();
    }

    public String getCategoryName() {
        return categoryName.getText();
    }

    public boolean isPageLoaded() {
        return categoryName.isDisplayed();
    }

}
