package com.epam.test.HomeWork21.utils;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import lombok.Getter;

@Getter
public class TestConfigurator {

    private static final String URL = "https://www.olx.ua/";

    public static final String CHROME = "chrome";

    public static final String FIREFOX = "firefox";

    public static final String INTERNET_EXPLORER = "ie";

    public static final String OPERA = "opera";

    public static final String EDGE = "edge";

    public void openDriver() {
        Configuration.startMaximized = true;
        Selenide.open(URL);
    }

    public void openDriver(String browser) {
        Configuration.startMaximized = true;
        Configuration.browser = browser;
        Selenide.open(URL);
    }

    public void closeDriver() {
        Selenide.closeWebDriver();
    }
}
