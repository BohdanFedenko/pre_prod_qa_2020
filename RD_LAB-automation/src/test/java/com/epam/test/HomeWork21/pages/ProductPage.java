package com.epam.test.HomeWork21.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class ProductPage {

    @FindBy(xpath = "//div[@class='descriptioncontent__headline']")
    private SelenideElement productDescription;

    @FindBy(xpath = "//div[@class='offer-user__actions']//h4//a")
    private SelenideElement seller;

    @FindBy(xpath = "//a[@id = 'reportMe']")
    private SelenideElement complain;

    @FindBy(xpath = "//form[@class = 'searchmain']//input[@type = 'submit']")
    private SelenideElement submitComplain;

    @FindBy(xpath = "//*[@id=\"message_system\"]/div/div/p/span")
    private SelenideElement errorMessage;

    @FindBy(xpath = "//div[@class = 'fblock fblock--submit']//input")
    private SelenideElement sendMessageToSellerButton;

    @FindBy(xpath = "//p[@class = 'desc errorboxContainer']/small[@id = 'se_emailError']")
    private SelenideElement sentWithEmptyEmailErrorMessage;

    @FindBy(xpath = "//p[@class = 'desc errorboxContainer']/small[@id = 'se_messageError']")
    private SelenideElement sentWithEmptyTextBoxErrorMessage;

    @FindBy(xpath = "//span[.='Объявление от']")
    private SelenideElement offerFrom;

    @FindBy(xpath = "//span[.='Объявление от']/following-sibling::strong")
    private SelenideElement offerType;

    public ProductPage sendMessageToSeller() {
        sendMessageToSellerButton.click();
        return page(this);
    }

    public ProductPage putComplain() {
        complain.click();
        return page(this);
    }

    public ProductPage submitComplainForm() {
        submitComplain.click();
        return page(this);
    }

    public boolean isErrorMessage() {
        return errorMessage.isDisplayed();
    }

    public boolean isSentWithEmptyEmailErrorMessage() {
        return sentWithEmptyEmailErrorMessage.isDisplayed();
    }

    public boolean isSentWithEmptyTextBoxErrorMessage() {
        return sentWithEmptyTextBoxErrorMessage.isDisplayed();
    }

    public SellerPage openSellerPage() {
        seller.click();
        return page(SellerPage.class);
    }

    public ProductListPage openOfferType() {
        offerFrom.click();
        return page(ProductListPage.class);
    }

    public String getOfferType() {
        return offerType.getText();
    }

    public boolean isPageLoaded() {
        return productDescription.isDisplayed();
    }
}
