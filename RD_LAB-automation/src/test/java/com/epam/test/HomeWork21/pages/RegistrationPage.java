package com.epam.test.HomeWork21.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class RegistrationPage {

    @FindBy(xpath = "//input[@id=\"userEmail\"]")
    private ElementsCollection emailField;

    @FindBy(xpath = "//section[@class = 'login-page has-animation']//button[@id='se_userLogin']")
    private SelenideElement enterButton;

    @FindBy(xpath = "//div[@class = 'errorboxContainer']")
    private ElementsCollection errorMessage;

    public RegistrationPage enter() {
        enterButton.click();
        return page(this);
    }

    public boolean isErrorMessage() {
        return errorMessage.get(0).isDisplayed();
    }

    public boolean isPageLoaded() {
        return emailField.get(0).isDisplayed();
    }
}
