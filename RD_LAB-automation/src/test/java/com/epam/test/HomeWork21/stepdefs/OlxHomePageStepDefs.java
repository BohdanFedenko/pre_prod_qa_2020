package com.epam.test.HomeWork21.stepdefs;

import com.epam.test.HomeWork21.pages.HomePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.page;

public class OlxHomePageStepDefs {

    String lang;

    HomePage homePage = page(HomePage.class);

    @Given("^I navigate to olx$")
    public void openOlxPage() {
    }

    @When("^changing lang of web site$")
    public void changeOlxLang() {
        lang = homePage.getCategoriesTitle();
        homePage.switchLang();
    }

    @When("entering {string} to search field and submit")
    public void enteringProductToSearchFieldAndSubmit(String productName) {
        homePage.search(productName);
    }

    @When("entering {string} and {string} to search fields and submit")
    public void enteringProductAndCityToSearchFieldsAndSubmit(String productName, String cityName) {
        homePage.search(productName, cityName);
    }

    @When("I open products list page use {string} category")
    public void iOpenProductsListPageUseCategory(String category) {
        homePage.openCategory(category);
    }

    @When("I open put offer page")
    public void iOpenPutOfferPage() {
        homePage.putOffer();
    }

    @When("enter {string} and {string} to search fields and submit")
    public void enterAndToSearchFieldsAndSubmit(String productName, String cityName) {
        homePage.searchFromHomePage(productName, cityName);
    }

    @And("enter {string} to search field and submit")
    public void enterProductNameToSearchFieldAndSubmit(String productName) {
        homePage.search(productName);
    }

    @And("open Мой профиль")
    public void openMyProfile() {
        homePage.putOffer();
    }

    @And("switch lang to default")
    public void switchLangToDefault() {
        homePage.switchLang();
    }

    @Then("^opened page is olx$")
    public void isOlxHomePage() {
        Assertions.assertTrue(homePage.isPageLoaded());
    }

    @Then("^web site lang is changed$")
    public void isOlxLangChange() {
        Assertions.assertNotEquals(lang, homePage.getCategoriesTitle());
    }

    @Then("error message under city field is displayed")
    public void errorMessageUnderCityFieldIsDisplayed() {
        Assertions.assertTrue(homePage.isCityErrorMessage());
    }
}

