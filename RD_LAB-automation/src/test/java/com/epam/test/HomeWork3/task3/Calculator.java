package com.epam.test.HomeWork3.task3;

public class Calculator {
    public void calculate(double valueOne, double valueTwo){
        printResult(valueOne,valueTwo);
    }

    private void printResult(double valueOne, double valueTwo){
        System.out.println("Sum:" + add(valueOne,valueTwo) +
                "\nSub: " + subtract(valueOne,valueTwo) +
                "\nMul: " + multiply(valueOne,valueTwo) +
                "\nDiv: "+ divide(valueOne,valueTwo));
    }

    private double add(double valueOne, double valueTwo){
        return valueOne+valueTwo;
    }

    private double subtract(double valueOne, double valueTwo){
        return valueOne-valueTwo;
    }

    private double multiply(double valueOne, double valueTwo){
        return valueOne*valueTwo;
    }

    private double divide(double valueOne, double valueTwo){
        if(valueTwo==0){
            System.out.println("Divided by zero");
        }
        return valueOne/valueTwo;
    }
}
