package com.epam.test.HomeWork3.task1;

public class ArrayHandler {
    private int [] array = new int[20];

    public void start(){
        arrayGenerator();
        print();
        System.out.println("Task1");
        sumEvenPositionElements();
        replaceNegativeElementsByZero();
        triplePositiveElementBeforeNegative();
        isDiffBetweenAverageAndMinElement();
    }

    /**The method generates the original array*/
    private void arrayGenerator(){
        for (int i = 0; i <array.length ; i++) {
            array[i]=(int)(Math.random()*20-10);
        }
    }

    /**The method prints the original array*/
    private void print(){
        for (int arr:array) {
            System.out.print(" " + arr);
        }
        System.out.println();
    }


    //Task 2
    /**The method calculates a sum of elements are standing in even position */
    public void sumEvenPositionElements(){
        int sum=0;
        for (int i = 0; i <array.length ; i++) {
            if(i%2==0){
                sum+=array[i];
            }
        }
        System.out.println("SUM: " + sum);
    }

    //Task 3
    /**The method replacing a negative element of array by zero*/
    public void replaceNegativeElementsByZero(){
        for (int i = 0; i <array.length ; i++) {
            if(array[i]<0){
                array[i]=0;
            }
            System.out.println(" " + array[i]);
        }
    }

    //Task 4
    /**The method multiply by three each element has a position before negative element*/
    public void triplePositiveElementBeforeNegative(){
        for (int i = 0; i <array.length; i++) {
            if(array[i]>0 && array[i+1]<0){
                array[i]*=3;
            }
            System.out.print(" " + array[i]);
        }
    }

    //Task 5
    /**The method calculates difference between average value of all array elements
     * and MIN element of array
     * */
    public void isDiffBetweenAverageAndMinElement(){
        double average=0;
        int sum=0;
        double difference=0;
        int minElement=array[0];
        for (int i = 0; i <array.length ; i++) {
            sum+=array[i];
            if(array[i]<minElement){
                minElement=array[i];
            }
        }
        average=sum/array.length;
        difference=average-minElement;
        System.out.println("Difference between arithmetic average and element minimum  value: " + difference);

    }

}
