package com.epam.test.HomeWork3.task2;

import java.util.Scanner;

public class StringHandler {
    private Scanner scanner = new Scanner(System.in);

    private String [] stringsList;
    private String [] wordsList;

    public void inputString(){

        System.out.println("Enter string amount");

        int amountOfString= scanner.nextInt();
        stringsList= new String[amountOfString+1];

        System.out.println("Enter string");

        for (int i = 0; i <stringsList.length ; i++) {
            stringsList[i]=scanner.nextLine();
        }
    }

    public void inputWord(){

        System.out.println("Enter words amount");

        int amountOfWords= scanner.nextInt();
        wordsList= new String[amountOfWords];

        System.out.println("Enter words");

        for (int i = 0; i <wordsList.length ; i++) {
            wordsList[i]=scanner.next();
        }
    }

    //Task1
    public void findMinLengthMaxLengthStrings(){
        if(stringsList==null){
            System.out.println("Invoke inputString Method first");
            return;
        }
        String longestString=stringsList[1];
        String shortestString=stringsList[1];

        int maxLenghtString=longestString.length();
        int mixLengthString=shortestString.length();

        for (int i = 1; i < stringsList.length; i++) {
            if(stringsList[i].length()>maxLenghtString){
                longestString=stringsList[i];
                maxLenghtString=longestString.length();
            }
            if(stringsList[i].length()<mixLengthString){
                shortestString=stringsList[i];
                mixLengthString=shortestString.length();
            }
        }

        System.out.println("Shortest String: " + shortestString + " Length: " + mixLengthString
                + "\nLongest String: " + longestString + " Length: " + maxLenghtString);
    }

    //Task2
    private int getStringsAverangeLength(){

        int sumStringLength=0;
        int averangeStringsLength=0;

        for (int i = 0; i < stringsList.length; i++) {
            sumStringLength += stringsList[i].length();
        }

        averangeStringsLength=sumStringLength/stringsList.length;

        return  averangeStringsLength;
    }

    public void getStringLongerAverange(){
        if(stringsList==null){
            System.out.println("Invoke inputString Method first");
            return;
        }
        for (int i = 0; i <stringsList.length ; i++) {
            if(stringsList[i].length()>getStringsAverangeLength()){
                System.out.println("String: " + stringsList[i] + " Length: " + stringsList[i].length());
            }
        }
    }

    //Task3
    public void getStringShorterAverange(){
        if(stringsList==null){
            System.out.println("Invoke inputString Method first");
            return;
        }
        for (int i = 0; i <stringsList.length ; i++) {
            if(stringsList[i].length()<getStringsAverangeLength()){
                System.out.println("String: " + stringsList[i] + " Length: " + stringsList[i].length());
            }
        }
    }

    //Task4
    private int countDiffCharacters(String word){
        int counter=0;

        for (int i = 0; i < word.length()-1; i++) {
            for (int j = i+1; j < word.length(); j++) {
                if(word.charAt(i)==(word.charAt(j))){
                    counter--;
                }
            }
        }
        return counter;
    }

    public void getWordWithMinDiffChar(){
        if(wordsList==null){
            System.out.println("Invoke inputWord Method first");
            return;
        }
        String wordWithMinDiffChar=wordsList[0];
        int amountDiffChar=countDiffCharacters(wordWithMinDiffChar);
        for (int i = 0; i <wordsList.length; i++) {
            if(countDiffCharacters(wordsList[i])<amountDiffChar){
                wordWithMinDiffChar=wordsList[i];
                amountDiffChar=countDiffCharacters(wordWithMinDiffChar);
            }
        }
        System.out.println("Word: " + wordWithMinDiffChar);
    }

    //Task5
    private boolean isDiffCharacters(String word){

        boolean sameChar=true;

        for (int i = 0; i < word.length()-1; i++) {
            for (int j = i+1; j < word.length(); j++) {
                if(word.charAt(i)==(word.charAt(j))){
                    sameChar=false;
                }
            }
        }
        return sameChar;
    }

    public void getWordWithDiffChar(){
        if(wordsList==null){
            System.out.println("Invoke inputWord Method first");
            return;
        }
        for (int i = 0; i <wordsList.length; i++) {
            if(isDiffCharacters(wordsList[i])){
                System.out.println("The word with only different characters: " + wordsList[i]);
                break;
            }
        }
    }

    //Task6
    private boolean isOnlyDigitWord(String word) {
        boolean digitWord = true;
        for (int i = 0; i < word.length(); i++) {
            if (!Character.isDigit(word.charAt(i))) {
                digitWord = false;
            }
        }
        return digitWord;
    }

    public void getOnlyDigitWord(){

        int onlyDigitWordCounter=0;

        if(wordsList==null){
            System.out.println("Invoke inputWord Method first");
            return;
        }
        for (int i = 0; i <wordsList.length; i++) {
            if(isOnlyDigitWord(wordsList[i])){
                if(onlyDigitWordCounter==1){
                    System.out.println("The word with only different characters: " + wordsList[i]);
                }
                onlyDigitWordCounter++;
            }
        }
    }
}
