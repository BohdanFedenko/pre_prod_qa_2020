package com.epam.test.HomeWork3.task2;

public class Main {
    public static void main(String[] args) {

        StringHandler stringHandler = new StringHandler();

        //Task1 - Task3
        stringHandler.inputString();

        stringHandler.findMinLengthMaxLengthStrings();
        stringHandler.getStringLongerAverange();
        stringHandler.getStringShorterAverange();

        //Task4-Task6
        stringHandler.inputWord();

        stringHandler.getWordWithMinDiffChar();
        stringHandler.getWordWithDiffChar();
        stringHandler.getOnlyDigitWord();
    }
}
