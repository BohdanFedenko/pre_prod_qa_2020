package com.epam.test.HomeWork19.testng;

import com.epam.test.HomeWork19.model.Calculator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static org.testng.AssertJUnit.assertEquals;

public class TestNGCalcSortedMethodRunTest {

    private static final Logger LOGGER = Logger.getLogger(TestNGCalcSortedMethodRunTest.class.getName());

    private Calculator calculator;

    @BeforeClass
    public void setUp() {
        calculator = new Calculator();
        LOGGER.info("Test class is started");
    }

    @Test(priority = 1)
    public void subtractMethodTest() {
        LOGGER.info("Test with priority = 1 is started");
        double expectedResult = 5.5;
        double actualResult = calculator.subtract(10.5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test(priority = 3)
    public void multiplyMethodTest() {
        LOGGER.info("Test with priority = 3 is started");
        double expectedResult = 51;
        double actualResult = calculator.multiply(10.2, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test(priority = 2)
    public void divideMethodTest() {
        LOGGER.info("Test with priority = 2 is started");
        double expectedResult = 1;
        double actualResult = calculator.divide(5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @AfterClass
    public void tearDown() {
        LOGGER.info("Test class is stopped");
    }
}
