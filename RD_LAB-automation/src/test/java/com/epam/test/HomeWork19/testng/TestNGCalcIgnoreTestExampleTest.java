package com.epam.test.HomeWork19.testng;

import com.epam.test.HomeWork19.model.Calculator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static org.testng.AssertJUnit.assertEquals;

public class TestNGCalcIgnoreTestExampleTest {

    private static final Logger LOGGER = Logger.getLogger(TestNGCalcIgnoreTestExampleTest.class.getName());

    private Calculator calculator;

    @BeforeClass
    public void setUp() {
        calculator = new Calculator();
        LOGGER.info("Test class is started");
    }

    @Test(enabled = false)
    public void multiplyMethodTest() {
        double expectedResult = 51;
        double actualResult = calculator.multiply(10.2, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void divideMethodTest() {
        double expectedResult = 1;
        double actualResult = calculator.divide(5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @AfterClass
    public void tearDown() {
        LOGGER.info("Test class is stopped");
    }
}

