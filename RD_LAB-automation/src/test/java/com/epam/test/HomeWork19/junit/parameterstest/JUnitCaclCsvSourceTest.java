package com.epam.test.HomeWork19.junit.parameterstest;

import com.epam.test.HomeWork19.model.Calculator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class JUnitCaclCsvSourceTest {

    private static final Logger LOGGER = Logger.getLogger(JUnitCaclCsvSourceTest.class.getName());

    private Calculator calculator;

    @BeforeAll
    public void setUp() {
        calculator = new Calculator();
        LOGGER.info("Test class is started");
    }

    @ParameterizedTest
    @CsvSource({"10, 1, 9", "25, -10, 35", "10.5, 8.2, 2.3"})
    public void subtractMethodTest(double operandOne, double operandTwo, double result) {
        double actualResult = calculator.subtract(operandOne, operandTwo);
        assertEquals(result, actualResult);
    }

    @AfterAll
    public void tearDown() {
        LOGGER.info("Test class is stopped");
    }
}
