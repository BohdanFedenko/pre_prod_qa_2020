package com.epam.test.HomeWork19.testng.listener3;

import org.testng.IHookCallBack;
import org.testng.IHookable;
import org.testng.ITestResult;

public class CustomHook implements IHookable {

    @Override
    public void run(IHookCallBack iHookCallBack, ITestResult iTestResult) {
        iHookCallBack.runTestMethod(iTestResult);
        setIHookMessage();
    }

    public void setIHookMessage() {
        System.out.println("From " + CustomHook.class);
    }
}
