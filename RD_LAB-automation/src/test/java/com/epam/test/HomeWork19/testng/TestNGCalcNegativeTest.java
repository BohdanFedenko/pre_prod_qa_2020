package com.epam.test.HomeWork19.testng;

import com.epam.test.HomeWork19.model.Calculator;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class TestNGCalcNegativeTest {

    private Calculator calculator;

    @BeforeClass
    public void setUp() {
        calculator = new Calculator();
    }

    @Test(expectedExceptions = ArithmeticException.class)
    public void divideByZeroExceptionTest() {
        double expectedResult = 0;
        double actualResult = calculator.divide(5, 0);
        assertEquals(expectedResult, actualResult);
    }
}
