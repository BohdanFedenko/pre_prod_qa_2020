package com.epam.test.HomeWork19.testng.listener;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.util.logging.Logger;

public class CustomListener implements ITestListener {

    private static final Logger LOGGER = Logger.getLogger(CustomListener.class.getName());

    public void onTestStart(ITestResult result) {
        LOGGER.info("Test started " + result.getName());
    }

    public void onTestSuccess(ITestResult result) {
        LOGGER.info("Test passed " + result.getName());
    }

    public void onTestFailure(ITestResult result) {
        LOGGER.info("Test failed " + result.getName());
    }

    public void onTestSkipped(ITestResult result) {
        LOGGER.info("Test skipped " + result.getName());
    }

    public void onStart(ITestContext context) {
        LOGGER.info("Test directory " + context.getOutputDirectory());
    }

    public void onFinish(ITestContext context) {
        LOGGER.info("Test directory " + context.getOutputDirectory());
    }
}
