package com.epam.test.HomeWork19.junit;

import com.epam.test.HomeWork19.model.Calculator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertThrows;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class JUnitCalcExceptionExampleTest {

    private static final Logger LOGGER = Logger.getLogger(JUnitCalcExceptionExampleTest.class.getName());

    private Calculator calculator;

    @BeforeAll
    public void setUp() {
        calculator = new Calculator();
        LOGGER.info("Test class is started");
    }

    @Test
    public void divideByZeroExceptionTest() {
        assertThrows(ArithmeticException.class,
                () -> calculator.divide(5, 0));
    }

    @AfterAll
    public void tearDown() {
        LOGGER.info("Test class is stoped");
    }
}
