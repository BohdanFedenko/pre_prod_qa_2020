package com.epam.test.HomeWork19.junit;

import com.epam.test.HomeWork19.model.Calculator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertThrows;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class JUnitCalcNegativeTest {

    private Calculator calculator;

    @BeforeAll
    public void setUp() {
        calculator = new Calculator();
    }

    @Test
    public void divideByZeroExceptionTest() {
        assertThrows(ArithmeticException.class, () -> calculator.divide(5, 0));
    }
}
