package com.epam.test.HomeWork19.testng;

import com.epam.test.HomeWork19.model.Calculator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static org.testng.AssertJUnit.assertEquals;

public class TestNGCalcExceptionExampleTest {

    private static final Logger LOGGER = Logger.getLogger(TestNGCalcExceptionExampleTest.class.getName());

    private Calculator calculator;

    @BeforeClass
    public void setUp() {
        calculator = new Calculator();
    }

    @Test(expectedExceptions = ArithmeticException.class)
    public void divideByZeroExceptionTest() {
        double expectedResult = 0;
        double actualResult = calculator.divide(5, 0);
        assertEquals(expectedResult, actualResult);
    }

    @AfterClass
    public void tearDown() {
        LOGGER.info("Test class is stopped");
    }
}
