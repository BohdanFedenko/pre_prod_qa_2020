package com.epam.test.HomeWork19.testng.listener;

import com.epam.test.HomeWork19.model.Calculator;
import org.testng.SkipException;
import org.testng.annotations.*;

import java.util.logging.Logger;

import static org.testng.AssertJUnit.assertEquals;

@Listeners(CustomListener.class)
public class CalculatorTestWithCustomListenerTest {

    private static final Logger LOGGER = Logger.getLogger(CalculatorTestWithCustomListenerTest.class.getName());

    private Calculator calculator;

    @BeforeClass
    public void setUp() {
        calculator = new Calculator();
        LOGGER.info("Test class is started");
    }

    @BeforeMethod
    public void setUpMethod() {
        LOGGER.info("Test method is started");
    }

    @Test
    public void addMethodTest() {
        double expectedResult = 15.5;
        double actualResult = calculator.add(10.5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void subtractMethodTest() {
        double expectedResult = 7.0;
        double actualResult = calculator.subtract(10.5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void multiplyMethodTest() throws SkipException {
        double expectedResult = 51;
        double actualResult = calculator.multiply(10.2, 5);
        assertEquals(expectedResult, actualResult);
    }

    @AfterMethod
    public void tearDownMethod() {
        LOGGER.info("Test method is stopped");
    }

    @AfterClass
    public void tearDown() {
        LOGGER.info("Test class is stopped");
    }
}
