package com.epam.test.HomeWork19.junit;

import com.epam.test.HomeWork19.model.Calculator;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runners.MethodSorters;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JUnitCalcSortedMethodRunTest {

    private static final Logger LOGGER = Logger.getLogger(JUnitCalcSortedMethodRunTest.class.getName());

    private Calculator calculator;

    @BeforeAll
    public void setUp() {
        calculator = new Calculator();
        LOGGER.info("Test class is started");
    }

    @Test
    public void addMethodTest() {
        double expectedResult = 15.5;
        double actualResult = calculator.add(10.5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void subtractMethodTest() {
        double expectedResult = 5.5;
        double actualResult = calculator.subtract(10.5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void multiplyMethodTest() {
        double expectedResult = 51;
        double actualResult = calculator.multiply(10.2, 5);
        assertEquals(expectedResult, actualResult);
    }

    @AfterAll
    public void tearDown() {
        LOGGER.info("Test class is stopped");
    }
}
