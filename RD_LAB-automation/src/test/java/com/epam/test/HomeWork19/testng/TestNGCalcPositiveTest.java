package com.epam.test.HomeWork19.testng;

import com.epam.test.HomeWork19.model.Calculator;
import org.testng.annotations.*;

import java.util.logging.Logger;

import static org.testng.AssertJUnit.assertEquals;

public class TestNGCalcPositiveTest {

    private static final Logger LOGGER = Logger.getLogger(TestNGCalcPositiveTest.class.getName());

    private Calculator calculator;

    @BeforeSuite
    public void setUp() {
        calculator = new Calculator();
        LOGGER.info("Test suite is started");
    }

    @BeforeMethod
    public void setUpMethod() {
        LOGGER.info("Test method is started");
    }

    @Test(groups = "smoke")
    public void addMethodTest() {
        double expectedResult = 15.5;
        double actualResult = calculator.add(10.5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void subtractMethodTest() {
        double expectedResult = 5.5;
        double actualResult = calculator.subtract(10.5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test(groups = "smoke")
    public void multiplyMethodTest() {
        double expectedResult = 51;
        double actualResult = calculator.multiply(10.2, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void divideMethodTest() {
        double expectedResult = 1;
        double actualResult = calculator.divide(5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @AfterMethod
    public void tearDownMethod() {
        LOGGER.info("Test method is stopped");
    }

    @AfterSuite
    public void tearDown() {
        LOGGER.info("Test suite is stopped");
    }
}
