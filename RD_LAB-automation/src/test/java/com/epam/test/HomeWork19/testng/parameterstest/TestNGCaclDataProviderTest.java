package com.epam.test.HomeWork19.testng.parameterstest;

import com.epam.test.HomeWork19.model.Calculator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static org.testng.AssertJUnit.assertEquals;

public class TestNGCaclDataProviderTest {

    private static final Logger LOGGER = Logger.getLogger(TestNGCaclDataProviderTest.class.getName());

    private Calculator calculator;

    @BeforeClass
    public void setUp() {
        calculator = new Calculator();
        LOGGER.info("Test class is started");
    }

    @DataProvider(name = "parameters")
    public static Object[][] getParameters() {
        return new Object[][]{{10, 2, 8}, {5, -4, 9}, {10.9, 10.8, 0.1}};
    }

    @Test(dataProvider = "parameters")
    public void subtractMethodTest(double operandOne, double operandTwo, double result) {
        double actualResult = calculator.subtract(operandOne, operandTwo);
        assertEquals(result, actualResult);
    }

    @AfterClass
    public void tearDown() {
        LOGGER.info("Test class is stopped");
    }
}
