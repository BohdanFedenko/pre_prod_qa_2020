package com.epam.test.HomeWork19.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({JUnitCalcPositiveTest.class, JUnitCalcNegativeTest.class})
public class JUnitCalcGroupTestExampleTest {
}
