package com.epam.test.HomeWork19.testng.parameterstest;

import com.epam.test.HomeWork19.model.Calculator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static org.testng.AssertJUnit.assertEquals;

public class TestNGCaclParametersTest {

    private static final Logger LOGGER = Logger.getLogger(TestNGCaclParametersTest.class.getName());

    private Calculator calculator;

    @BeforeClass
    public void setUp() {
        calculator = new Calculator();
        LOGGER.info("Test class is started");
    }

    @Test
    @Parameters({"operandOne", "operandTwo", "result"})
    public void subtractMethodTest(double operandOne, double operandTwo, double result) {
        double actualResult = calculator.subtract(operandOne, operandTwo);
        assertEquals(result, actualResult);
    }

    @AfterClass
    public void tearDown() {
        LOGGER.info("Test class is stopped");
    }
}
