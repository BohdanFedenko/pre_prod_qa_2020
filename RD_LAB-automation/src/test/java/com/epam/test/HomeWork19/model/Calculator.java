package com.epam.test.HomeWork19.model;

import java.util.logging.Logger;

public class Calculator {

    private static final Logger LOGGER = Logger.getLogger(Calculator.class.getName());

    public double add(double operandOne, double operandTwo) {
        LOGGER.info("add() method is executed");
        return operandOne + operandTwo;
    }

    public double subtract(double operandOne, double operandTwo) {
        LOGGER.info("subtract() method is executed");
        return operandOne - operandTwo;
    }

    public double divide(double operandOne, double operandTwo) throws ArithmeticException {
        double result = 0;
        if (operandTwo == 0) {
            throw new ArithmeticException("DivideByZero");
        }
        result = operandOne / operandTwo;
        LOGGER.info("divide() method is executed");
        return result;
    }

    public double multiply(double operandOne, double operandTwo) {
        LOGGER.info("multiply() method is executed");
        return operandOne * operandTwo;
    }
}
