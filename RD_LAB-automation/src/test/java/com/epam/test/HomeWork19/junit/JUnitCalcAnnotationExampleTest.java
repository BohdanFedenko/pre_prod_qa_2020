package com.epam.test.HomeWork19.junit;

import com.epam.test.HomeWork19.model.Calculator;
import org.junit.jupiter.api.*;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class JUnitCalcAnnotationExampleTest {

    private static final Logger LOGGER = Logger.getLogger(JUnitCalcAnnotationExampleTest.class.getName());

    private Calculator calculator;

    @BeforeAll
    public void setUp() {
        calculator = new Calculator();
        LOGGER.info("Test class is started");
    }

    @BeforeEach
    public void setUpEach() {
        LOGGER.info("Test method is started");
    }

    @Test
    public void addMethodTest() {
        double expectedResult = 15.5;
        double actualResult = calculator.add(10.5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @AfterEach
    public void tearDownEach() {
        LOGGER.info("Test method is stopped");
    }

    @AfterAll
    public void tearDown() {
        LOGGER.info("Test class is stopped");
    }
}
