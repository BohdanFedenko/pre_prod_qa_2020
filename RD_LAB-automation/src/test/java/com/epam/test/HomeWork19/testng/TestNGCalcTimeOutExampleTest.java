package com.epam.test.HomeWork19.testng;

import com.epam.test.HomeWork19.model.Calculator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static org.testng.AssertJUnit.assertEquals;

public class TestNGCalcTimeOutExampleTest {

    private static final Logger LOGGER = Logger.getLogger(TestNGCalcTimeOutExampleTest.class.getName());

    private Calculator calculator;

    @BeforeClass
    public void setUp() {
        calculator = new Calculator();
        LOGGER.info("Test class is started");
    }

    @Test(timeOut = 1000)
    public void subtractMethodTest() {
        double expectedResult = 10;
        double actualResult = calculator.subtract(20, 10);
        assertEquals(expectedResult, actualResult);
    }

    @AfterClass
    public void tearDown() {
        LOGGER.info("Test class is stopped");
    }
}
