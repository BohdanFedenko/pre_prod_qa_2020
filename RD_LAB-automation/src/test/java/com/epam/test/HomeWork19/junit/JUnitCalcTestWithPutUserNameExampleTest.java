package com.epam.test.HomeWork19.junit;

import com.epam.test.HomeWork19.model.Calculator;
import org.junit.jupiter.api.*;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class JUnitCalcTestWithPutUserNameExampleTest {

    private static final Logger LOGGER = Logger.getLogger(JUnitCalcTestWithPutUserNameExampleTest.class.getName());

    private Calculator calculator;

    @BeforeAll
    public void setUp() {
        calculator = new Calculator();
        LOGGER.info("Test class is started");
    }

    @Test
    @DisplayName("10.5 + 5.0 = 15.5")
    public void addMethodTest() {
        double expectedResult = 15.5;
        double actualResult = calculator.add(10.5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("10.5 - 5.0 = 5.5")
    public void subtractMethodTest() {
        double expectedResult = 5.5;
        double actualResult = calculator.subtract(10.5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("10.2 * 5.0 = 51.0")
    public void multiplyMethodTest() {
        double expectedResult = 51;
        double actualResult = calculator.multiply(10.2, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("5.0 / 5.0 = 1.0")
    public void divideMethodTest() {
        double expectedResult = 1;
        double actualResult = calculator.divide(5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @AfterAll
    public void tearDown() {
        LOGGER.info("Test class is stoped");
    }
}
