package com.epam.test.HomeWork19.junit;

import com.epam.test.HomeWork19.model.Calculator;
import org.junit.jupiter.api.*;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class JUnitCalcIgnoreTestExampleTest {

    private static final Logger LOGGER = Logger.getLogger(JUnitCalcIgnoreTestExampleTest.class.getName());

    private Calculator calculator;

    @BeforeAll
    public void setUp() {
        calculator = new Calculator();
        LOGGER.info("Test class is started");
    }

    @Disabled
    @Test
    public void multiplyMethodTest() {
        double expectedResult = 51;
        double actualResult = calculator.multiply(10.2, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void divideMethodTest() {
        double expectedResult = 1;
        double actualResult = calculator.divide(5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @AfterAll
    public void tearDown() {
        LOGGER.info("Test class is stopped");
    }
}
