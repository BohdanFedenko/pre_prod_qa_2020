package com.epam.test.HomeWork19.testng.listener2;

import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class AnnotationTransformers implements IAnnotationTransformer {

    public boolean isTestRunning(ITestAnnotation annotation) {
        if (annotation.getAlwaysRun()) {
            return false;
        }
        return true;
    }

    @Override
    public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
        if (isTestRunning(annotation)) {
            annotation.setEnabled(false);
        }
    }
}
