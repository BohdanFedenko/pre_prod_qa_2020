package com.epam.test.HomeWork19.testng.listener3;

import com.epam.test.HomeWork19.model.Calculator;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

@Listeners(CustomHook.class)
public class CalculatorCustomHookTest {

    private Calculator calculator;

    @BeforeClass
    public void setUp() {
        calculator = new Calculator();
    }

    @Test
    public void addMethodTest() {
        double expectedResult = 15.5;
        double actualResult = calculator.add(10.5, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void subtractMethodTest() {
        double expectedResult = 5.5;
        double actualResult = calculator.subtract(10.5, 5);
        assertEquals(expectedResult, actualResult);
    }
}
