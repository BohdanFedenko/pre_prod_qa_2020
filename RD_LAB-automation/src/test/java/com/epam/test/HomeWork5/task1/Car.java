package com.epam.test.HomeWork5.task1;

import java.io.Serializable;

public abstract class Car implements Serializable {

    private String model;

    private double fuelRate;

    private double price;

    public Car(String model,double fuelRate, double price){
        this.model = model;
        this.fuelRate = fuelRate;
        this.price = price;
    }

    public String getModel(){
        return model;
    }

    public double getFuelRate(){
        return fuelRate;
    }

    public double getPrice(){
        return price;
    }

    @Override
    public String toString() {
        return "Model: " + getModel() + ", Price: " + getPrice() + ", Fuel for 100: " + getFuelRate();
    }
}
