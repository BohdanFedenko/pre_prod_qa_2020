package com.epam.test.HomeWork5.task1;

public abstract class TaxiCab extends Car implements ITaxi {

    protected int carNumber;

    protected String priceClass;

    protected int passenger;

    public TaxiCab(String model, double fuelRate, double price, int carNumber, String priceClass, int passenger) {
        super(model, fuelRate, price);
        this.carNumber = carNumber;
        this.passenger = passenger;
        this.priceClass = priceClass;
    }

    @Override
    public String toString() {
        return super.toString() + ", Car number: " + getCarNumber() + ", Price Class: "
                + getPriceClass() + ", Capacity: " + getPassengerCapacity();
    }
}
