package com.epam.test.HomeWork5.task2;

import java.io.Serializable;

public abstract class Calculator implements Serializable {

    protected String name;

    protected Double result;

    public Calculator(String name){
        this.name=name;
    }

    public abstract void input(double valueOne, double valueTwo);

    public abstract IOperation calculate();

    public void printResult(){
        System.out.println("Calculator name - " + name + "\nResult: " + result);
    }
}
