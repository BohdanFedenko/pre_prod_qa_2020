package com.epam.test.HomeWork5.task1;

public class TaxiStationManager {
    public static void main(String[] args) {
        TaxiCab minibus = new Minibus("Ford", 10, 25000, 10,"Economy",18);
        TaxiCab minivan = new Minivan("Hyundai",8,20000,136,"Standart",8);
        TaxiCab sedan = new Sedan("BMW",6,60000,1,"Business", 3);
        TaxiCab limousine = new Limousine("Lincoln", 7,150500,36,"Luxury",12);

        TaxiStation taxiStation = new TaxiStation("Victory", new TaxiCab[] {minibus, minivan, sedan, limousine});

        System.out.println("All cars of Taxi Station cost: " + taxiStation.getAllCarsCost());
        taxiStation.sortByFuelRate();
        taxiStation.showAllCars();
        System.out.println(taxiStation.findCar("Lincoln", "Luxury", 12, 36));
        System.out.println(taxiStation.findCar("BMW", "Business", 3, 8));
    }
}
