package com.epam.test.HomeWork5.task1;

import java.io.Serializable;

public interface ITaxi extends Serializable {

    int getCarNumber();

    String getPriceClass();

    int getPassengerCapacity();
}
