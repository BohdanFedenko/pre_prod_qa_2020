package com.epam.test.HomeWork5.task2;

public class CalculatorManager {
    public static void main(String[] args) {
        SimpleCalculator simpleCalculator = new SimpleCalculator("SimpleCalculator");
        simpleCalculator.input(5,2);
        simpleCalculator.calculate().multiply();
        simpleCalculator.printResult();
    }
}
