package com.epam.test.HomeWork5.task2;

public interface ISimpleOperations extends IOperation {

    double add();

    double subtract();

    double multiply();

    double division() throws ArithmeticException;
}
