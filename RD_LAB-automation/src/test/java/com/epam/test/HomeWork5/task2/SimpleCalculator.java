package com.epam.test.HomeWork5.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SimpleCalculator extends Calculator implements ISimpleOperations {

    private static final Logger LOG = LogManager.getLogger(SimpleCalculator.class);

    private double operantOne;

    private double operantTwo;

    public SimpleCalculator(String name) {
        super(name);
    }

    @Override
    public void input(double valueOne, double valueTwo) {
        if(valueTwo==0){
            LOG.warn("Divide by zero possible");
        }
        this.operantOne=valueOne;
        this.operantTwo=valueTwo;
        LOG.info("Operant 1 = " + operantOne + "; Operant 2 = " + operantTwo);
    }

    @Override
    public ISimpleOperations calculate() {
        LOG.debug("Calculate method executing");
        LOG.info("Return object: " + SimpleCalculator.class.getName());
        return this;
    }

    @Override
    public double add() {
        LOG.debug("Add method executing");
        result = operantOne+operantTwo;
        LOG.info("Operation result: " + result);
        return result;
    }

    @Override
    public double subtract() {
        LOG.debug("Subtract method executing");
        result = operantOne-operantTwo;
        LOG.info("Operation result: " + result);
        return result;
    }

    @Override
    public double multiply() {
        LOG.debug("Multiply method executing");
        result = operantOne*operantTwo;
        LOG.info("Operation result: " + result);
        return result;
    }

    @Override
    public double division() throws ArithmeticException {
        LOG.debug("Division method executing");
        if(operantTwo==0){
            LOG.error("Division by zero");
            throw new ArithmeticException("Division by zero");
        }
        result = operantOne/operantTwo;
        LOG.info("Operation result: " + result);
        return result;
    }
}
