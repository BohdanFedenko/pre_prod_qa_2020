package com.epam.test.HomeWork5.task1;

public class Limousine extends TaxiCab {

    public Limousine(String model, double fuelRate, double price, int carNumber, String priceClass, int passenger) {
        super(model, fuelRate, price, carNumber, priceClass, passenger);
    }

    @Override
    public int getCarNumber() {
        return carNumber;
    }

    @Override
    public String getPriceClass() {
        return priceClass;
    }

    @Override
    public int getPassengerCapacity() {
        return passenger;
    }
}
