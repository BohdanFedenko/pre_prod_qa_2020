package com.epam.test.HomeWork5.task1;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;

public class TaxiStation implements Serializable {

    private String stantionName;

    private TaxiCab[] cars;

    public TaxiStation(String stantionName, TaxiCab[] cars) {
        this.stantionName = stantionName;
        this.cars = cars;
    }

    public TaxiCab[] getCars() {
        return cars;
    }

    public double getAllCarsCost() {
        double allCarsCost = 0;
        for (Car car : cars) {
            allCarsCost += car.getPrice();
        }
        return allCarsCost;
    }

    public void sortByFuelRate() {
        Arrays.sort(cars, new Comparator<TaxiCab>() {
            @Override
            public int compare(TaxiCab taxiCab1, TaxiCab taxiCab2) {
                return Double.compare(taxiCab1.getFuelRate(), taxiCab2.getFuelRate());
            }
        });
    }

    /**
     * Method finds a car what has all of the entered parameters.
     * If such car is not exist returns first of all car of array
     */
    public TaxiCab findCar(String model, String priceClass, int passenger, int carNumber) {
        TaxiCab findCar = cars[0];
        for (TaxiCab car : cars) {
            boolean isModel = car.getModel().equals(model);
            boolean isPriceClass = car.getPriceClass().equals(priceClass);
            boolean isPassenger = car.getPassengerCapacity() == passenger;
            boolean isCarNumber = car.getCarNumber() == carNumber;
            if (isModel && isPriceClass && isPassenger && isCarNumber) {
                findCar = car;
            }
        }
        return findCar;
    }

    public void showAllCars(){
        for (TaxiCab car:cars) {
            System.out.println(car);
        }
    }
}
