package com.epam.test.HomeWork13.model;

import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Account {

    private int id;

    private int balance;

    private int clientId;
}
