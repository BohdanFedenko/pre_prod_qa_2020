package com.epam.test.HomeWork13.dao;

import java.sql.SQLException;
import java.util.List;

public interface BankDao<T> {

    void save(T t) throws SQLException;

    void save(List<T> t) throws SQLException;

    List<T> getAll() throws SQLException;

    T getById(int id) throws SQLException;

    void delete(int id) throws SQLException;

    void update(String columnName, int newValue, int id) throws SQLException;

    void update(String columnName, String newValue, int id) throws SQLException;
}
