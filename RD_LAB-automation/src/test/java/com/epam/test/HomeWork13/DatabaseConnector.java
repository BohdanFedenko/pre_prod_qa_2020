package com.epam.test.HomeWork13;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnector {

    private Connection connection;

    private String userName;

    private String password;

    private String url;

    public Connection open() {
        setDatabaseConnectionData(getProperties());
        try {
            connection = DriverManager.getConnection(url, userName, password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }

    private Properties getProperties() {
        Properties properties = new Properties();
        try {
            FileInputStream file = new FileInputStream("src/test/resources/db.properties");
            properties.load(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    private void setDatabaseConnectionData(Properties properties) {
        this.url = properties.getProperty("db.url");
        this.userName = properties.getProperty("db.user");
        this.password = properties.getProperty("db.password");
    }
}
