package com.epam.test.HomeWork13.services;

import com.epam.test.HomeWork13.dao.ClientDao;
import com.epam.test.HomeWork13.model.Client;

import java.sql.SQLException;
import java.util.List;

public class ClientsService {

    private ClientDao clientDao;

    public ClientsService() {
        clientDao = new ClientDao();
    }

    public Client getClientById(int clientId) throws SQLException {
        return clientDao.getById(clientId);
    }

    public List<Client> getAllClients() throws SQLException {
        return clientDao.getAll();
    }

    public void addClient(Client client) throws SQLException {
        clientDao.save(client);
    }

    public void addClientsList(List<Client> clientsList) throws SQLException {
        clientDao.save(clientsList);
    }

    public void removeClientById(int clientId) throws SQLException {
        clientDao.delete(clientId);
    }

    public void updateClientById(String columnName, String newValue, int id) throws SQLException {
        clientDao.update(columnName, newValue, id);
    }

    public void closeConnection() throws SQLException {
        clientDao.closeConnection();
    }
}
