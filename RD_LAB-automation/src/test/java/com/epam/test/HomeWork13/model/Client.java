package com.epam.test.HomeWork13.model;

import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Client {

    private int id;

    private String firstName;

    private String lastName;
}
