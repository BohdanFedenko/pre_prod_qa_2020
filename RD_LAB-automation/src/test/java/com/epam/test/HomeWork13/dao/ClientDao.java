package com.epam.test.HomeWork13.dao;

import com.epam.test.HomeWork13.DatabaseConnector;
import com.epam.test.HomeWork13.model.Client;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ClientDao implements BankDao<Client> {

    private static final String READ_ALL = "SELECT * FROM clients";

    private static final String READ_BY_COLUMN_NAME = "SELECT * FROM clients WHERE id =";

    private static final String INSERT = "INSERT INTO clients VALUES ";

    private static final String DELETE = "DELETE FROM clients WHERE id=";

    private static final String UPDATE = "UPDATE clients SET ";

    private Connection connection;

    private Statement statement;

    private DatabaseConnector databaseConnector;

    public ClientDao() {
        databaseConnector = new DatabaseConnector();
        this.connection = databaseConnector.open();
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }

    public void save(Client client) throws SQLException {
        statement = connection.createStatement();
        String newClient = INSERT + "(" + client.getId() + ",'"
                + client.getFirstName() + "','"
                + client.getLastName() + "')";
        statement.executeUpdate(newClient);
    }

    public void save(List<Client> clientsList) throws SQLException {
        statement = connection.createStatement();
        for (Client client : clientsList) {
            String newClient = INSERT + "(" + client.getId() + ",'"
                    + client.getFirstName()
                    + "','" + client.getLastName() + "')";
            statement.executeUpdate(newClient);
        }
    }

    public List<Client> getAll() throws SQLException {
        statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(READ_ALL);
        return getClientsList(resultSet);
    }

    public Client getById(int id) throws SQLException {
        statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(READ_BY_COLUMN_NAME + id);
        return getClient(resultSet);
    }

    public void delete(int id) throws SQLException {
        statement = connection.createStatement();
        String client = DELETE + id;
        statement.executeUpdate(client);
    }

    public void update(String columnName, String newValue, int id) throws SQLException {
        statement = connection.createStatement();
        String newClient = UPDATE + "" + columnName + "='" + newValue + "' WHERE id=" + id;
        statement.executeUpdate(newClient);
    }

    private Client getClient(ResultSet resultSet) throws SQLException {
        Client client = new Client();
        while (resultSet.next()) {
            client.setId(resultSet.getInt("Id"));
            client.setFirstName(resultSet.getString("First_Name"));
            client.setLastName(resultSet.getString("Last_Name"));
        }
        return client;
    }

    private List<Client> getClientsList(ResultSet resultSet) throws SQLException {
        List<Client> clients = new ArrayList<>();
        while (resultSet.next()) {
            clients.add(new Client(resultSet.getInt("Id"),
                    resultSet.getString("First_Name"), resultSet.getString("Last_Name")));
        }
        return clients;
    }

    public void update(String columnName, int newValue, int id) throws SQLException {

    }
}
