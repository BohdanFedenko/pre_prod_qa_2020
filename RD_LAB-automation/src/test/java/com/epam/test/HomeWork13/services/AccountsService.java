package com.epam.test.HomeWork13.services;

import com.epam.test.HomeWork13.dao.AccountDao;
import com.epam.test.HomeWork13.model.Account;

import java.sql.SQLException;
import java.util.List;

public class AccountsService {

    private static AccountDao accountDao;

    public AccountsService() {
        accountDao = new AccountDao();
    }

    public Account getAccountById(int accountId) throws SQLException {
        return accountDao.getById(accountId);
    }

    public List<Account> getAllAccounts() throws SQLException {
        return accountDao.getAll();
    }

    public void addAccount(Account account) throws SQLException {
        accountDao.save(account);
    }

    public void addAccountsList(List<Account> accountList) throws SQLException {
        accountDao.save(accountList);
    }

    public void removeAccountById(int accountId) throws SQLException {
        accountDao.delete(accountId);
    }

    public void updateAccountById(String columnName, int newValue, int id) throws SQLException {
        accountDao.update(columnName, newValue, id);
    }

    public void closeConnection() throws SQLException {
        accountDao.closeConnection();
    }
}
