package com.epam.test.HomeWork13.dao;

import com.epam.test.HomeWork13.DatabaseConnector;
import com.epam.test.HomeWork13.model.Account;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AccountDao implements BankDao<Account> {

    private static final String READ_ALL = "SELECT * FROM accounts";

    private static final String READ_BY_COLUMN_NAME = "SELECT * FROM accounts WHERE id =";

    private static final String INSERT = "INSERT INTO accounts VALUES ";

    private static final String DELETE = "DELETE FROM accounts WHERE id=";

    private static final String UPDATE = "UPDATE accounts SET ";

    private Connection connection;

    private Statement statement;

    private DatabaseConnector databaseConnector;

    public AccountDao() {
        databaseConnector = new DatabaseConnector();
        this.connection = databaseConnector.open();
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }

    public void save(Account account) throws SQLException {
        statement = connection.createStatement();
        String newAccount = INSERT + "(" + account.getId() + ","
                + account.getBalance() + ","
                + account.getClientId() + ")";
        statement.executeUpdate(newAccount);
    }

    public void save(List<Account> accountsList) throws SQLException {
        statement = connection.createStatement();
        for (Account account : accountsList) {
            String newAccount = INSERT + "(" + account.getId() + ","
                    + account.getBalance()
                    + "," + account.getClientId() + ")";
            statement.executeUpdate(newAccount);
        }
    }

    public Account getById(int id) throws SQLException {
        statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(READ_BY_COLUMN_NAME + id);
        return getAccount(resultSet);
    }

    public List<Account> getAll() throws SQLException {
        statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(READ_ALL);
        return getAccountsList(resultSet);
    }

    public void delete(int id) throws SQLException {
        statement = connection.createStatement();
        String account = DELETE + id;
        statement.executeUpdate(account);
    }

    public void update(String columnName, int newValue, int id) throws SQLException {
        statement = connection.createStatement();
        String newAccount = UPDATE + "" + columnName + "=" + newValue + " WHERE id=" + id;
        statement.executeUpdate(newAccount);
    }

    private Account getAccount(ResultSet resultSet) throws SQLException {
        Account account = new Account();
        while (resultSet.next()) {
            account.setId(resultSet.getInt("Id"));
            account.setBalance(resultSet.getInt("Balance"));
            account.setClientId(resultSet.getInt("ClientId"));
        }
        return account;
    }

    private List<Account> getAccountsList(ResultSet resultSet) throws SQLException {
        List<Account> accounts = new ArrayList<>();
        while (resultSet.next()) {
            accounts.add(new Account(resultSet.getInt("Id"),
                    resultSet.getInt("Balance"), resultSet.getInt("ClientId")));
        }
        return accounts;
    }

    public void update(String columnName, String newValue, int id) throws SQLException {
    }
}
