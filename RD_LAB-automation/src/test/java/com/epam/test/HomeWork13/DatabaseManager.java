package com.epam.test.HomeWork13;

import com.epam.test.HomeWork13.model.Account;
import com.epam.test.HomeWork13.model.Client;
import com.epam.test.HomeWork13.services.AccountsService;
import com.epam.test.HomeWork13.services.ClientsService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class DatabaseManager {

    private static final Logger LOGGER = Logger.getLogger(DatabaseConnector.class.getName());

    public static void main(String[] args) throws SQLException {

        LOGGER.info("Connection to Accounts DB is opened!");
        AccountsService accountsService = new AccountsService();

        accountsService.addAccount(new Account(1, 100, 1));

        Account accountTwo = new Account(2, 250, 2);
        Account accountThree = new Account(3, 400, 3);
        List<Account> accounts = new ArrayList<>();
        accounts.add(accountTwo);
        accounts.add(accountThree);
        accountsService.addAccountsList(accounts);

        LOGGER.info("Get account by id = 1:");
        System.out.println(accountsService.getAccountById(1));

        LOGGER.info("Get all accounts:");
        for (Account account : accountsService.getAllAccounts()) {
            System.out.println(account);
        }

        LOGGER.info("Update account by id = 2:");
        System.out.println("Before updating: " + accountsService.getAccountById(2));
        accountsService.updateAccountById("Balance", 1000, 2);
        System.out.println("After updating: " + accountsService.getAccountById(2));

        LOGGER.info("Delete account by id = 3");
        accountsService.removeAccountById(3);
        LOGGER.info("All Accounts of Accounts DB:");
        for (Account account : accountsService.getAllAccounts()) {
            System.out.println(account);
        }

        accountsService.closeConnection();
        LOGGER.info("Connection to Accounts DB is closed!");

        LOGGER.info("Connection to Clients DB is opened!");
        ClientsService clientsService = new ClientsService();

        clientsService.addClient(new Client(1, "Petr", "Petrov"));

        Client clientTwo = new Client(2, "Igor", "Igorev");
        Client clientThree = new Client(3, "Oleg", "Olegov");
        List<Client> clients = new ArrayList<>();
        clients.add(clientTwo);
        clients.add(clientThree);
        clientsService.addClientsList(clients);

        LOGGER.info("Get client by id = 1:");
        System.out.println(clientsService.getClientById(1));

        LOGGER.info("Get all clients:");
        for (Client client : clientsService.getAllClients()) {
            System.out.println(client);
        }

        LOGGER.info("Update client by id = 2:");
        System.out.println("Before updating: " + clientsService.getClientById(2));
        clientsService.updateClientById("Last_Name", "Knut", 2);
        System.out.println("After updating: " + clientsService.getClientById(2));

        LOGGER.info("Delete client by id = 3");
        clientsService.removeClientById(3);
        LOGGER.info("All Clients of Clients DB:");
        for (Client client : clientsService.getAllClients()) {
            System.out.println(client);
        }

        clientsService.closeConnection();
        LOGGER.info("Connection to Clients DB is closed!");
    }
}
