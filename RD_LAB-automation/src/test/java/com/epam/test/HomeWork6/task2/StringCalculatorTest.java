package com.epam.test.HomeWork6.task2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StringCalculatorTest {

    @Test
    void addEmtyStringInMethodParameterTest() throws Exception{
        StringCalculator stringCalculator = new StringCalculator();
        assertEquals(0,stringCalculator.add(""));
    }

    @Test
    void addStringWithOneNumberInMethodParameterTest() throws Exception{
        StringCalculator stringCalculator = new StringCalculator();
        assertEquals(1,stringCalculator.add("1"));
    }

    @Test
    void addStringWithTwoNumbersInMethodParameterTest() throws Exception{
        StringCalculator stringCalculator = new StringCalculator();
        assertEquals(3,stringCalculator.add("1,2"));
    }

    @Test
    void addStringWithAnyQuantityNumbersInMethodParameterTest() throws Exception{
        StringCalculator stringCalculator = new StringCalculator();
        assertEquals(21,stringCalculator.add("1,2,3,4,5,6"));
    }

    @Test
    void addStringWithNewLineBetweenNumbersInMethodParameterTest() throws Exception{
        StringCalculator stringCalculator = new StringCalculator();
        assertEquals(6,stringCalculator.add("1\n2,3"));
    }

    @Test
    void addStringWithDifferentDelimiterNumbersInMethodParameterTest() throws Exception{
        StringCalculator stringCalculator = new StringCalculator();
        assertEquals(3,stringCalculator.add("//;\n1;2"));
    }

    @Test
    void addStringWithoutFirstLineWithDelimiterNumbersInMethodParameterTest() throws Exception{
        StringCalculator stringCalculator = new StringCalculator();
        assertEquals(3,stringCalculator.add("n1;2"));
    }

    @Test
    void addStringWithNegativeNumbersInMethodParameterTest() throws Exception{
        StringCalculator stringCalculatoor = new StringCalculator();

        Exception exception = assertThrows(Exception.class, ()-> {
            stringCalculatoor.add("n1;-2,5, -6");
        });
    }
}
