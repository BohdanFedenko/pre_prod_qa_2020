package com.epam.test.HomeWork6.task1;

import com.epam.test.HomeWork5.task2.SimpleCalculator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleCalculatorTest {

    private static SimpleCalculator simpleCalculator;

    @BeforeAll
    static void setBeforeAll(){
        simpleCalculator = new SimpleCalculator("");
    }

    @Test
    void addWithValidDataTest() throws Exception {
        simpleCalculator.input(10,25);
        assertEquals(35,simpleCalculator.calculate().add());
    }

    @Test
    void subtractWithValidDataTest() throws Exception {
        simpleCalculator.input(5,0);
        assertEquals(5,simpleCalculator.calculate().subtract());
    }

    @Test
    void multiplyWithValidDataTest() throws Exception {
        simpleCalculator.input(15,10);
        assertEquals(150,simpleCalculator.calculate().multiply());
    }

    @Test
    void divisionWithValidDataTest() throws Exception {
        simpleCalculator.input(15,10);
        assertEquals(1.5,simpleCalculator.calculate().division());
    }

    @Test
    void divisionByZeroTest() throws Exception {
        simpleCalculator.input(10,0);
        ArithmeticException exception = assertThrows(ArithmeticException.class, ()->{
           simpleCalculator.calculate().division();
        });
    }
}