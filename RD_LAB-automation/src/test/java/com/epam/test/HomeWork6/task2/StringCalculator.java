package com.epam.test.HomeWork6.task2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringCalculator {

       public int add(String numbers) throws Exception{

        Pattern pattern = Pattern.compile("-?[0-9]+");

        Matcher matcher = pattern.matcher(numbers);

        String negativeNumbers="";

        int numbersSum=0;

        while(matcher.find()) {
            if(Integer.parseInt(matcher.group())<0){
                negativeNumbers+=", " + Integer.parseInt(matcher.group());
            } else {
                numbersSum += Integer.parseInt(matcher.group());
            }
        }
        if(negativeNumbers.length()>0){
            throw new Exception("negatives not allowed" + negativeNumbers);
        }
        return numbersSum;
    }
}
