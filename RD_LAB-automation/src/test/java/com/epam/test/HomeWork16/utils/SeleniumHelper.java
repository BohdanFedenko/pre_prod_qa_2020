package com.epam.test.HomeWork16.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumHelper {

    public static WebDriver startChromeDriver(String uri) {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(uri);
        return driver;
    }
}
