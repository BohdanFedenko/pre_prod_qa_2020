package com.epam.test.HomeWork16.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {

    private static final String REACT = "//a[@href = 'examples/react']";

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public ExampleReactPage getReactExample() {
        driver.findElement(By.xpath(REACT)).click();
        return new ExampleReactPage(driver);
    }
}
