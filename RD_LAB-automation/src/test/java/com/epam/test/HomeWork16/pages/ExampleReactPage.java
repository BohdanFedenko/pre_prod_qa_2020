package com.epam.test.HomeWork16.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ExampleReactPage extends BasePage {

    private static final String INPUT_FIELD = "//input[@class='new-todo']";

    private static final String TODOS_LIST = "//ul[@class='todo-list']//li";

    private static final String COMPLETED_TODO_LIST = "//ul[@class='todo-list']//li[@class='completed']";

    private static final String LABEL_OF_ITEM = "label";

    private static final String CHECKBOX_OF_ITEM = "input";

    private static final String CHECKBOXES = "//ul[@class='todo-list']//input[@class='toggle']";

    public ExampleReactPage(WebDriver driver) {
        super(driver);
    }

    public int getTodosListSize() {
        return getTodosList().size();
    }

    public ExampleReactPage addNewItem(String todo) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath(INPUT_FIELD)).sendKeys(todo, Keys.ENTER);
        return this;
    }

    public ExampleReactPage markItemCompleteByLabel(String label) {
        for (WebElement element : getTodosList()) {
            if (element.findElement(By.tagName(LABEL_OF_ITEM)).getText().equals(label)) {
                element.findElement(By.tagName(CHECKBOX_OF_ITEM)).click();
            }
        }
        return this;
    }

    public ExampleReactPage markAllItemsCompleted() {
        for (WebElement element : getAllCheckboxes()) {
            element.click();
        }
        return this;
    }

    public boolean isCompletedAllItems() {
        int completedItemsCount = 0;
        for (WebElement element : getTodosList()) {
            if (element.findElement(By.xpath(COMPLETED_TODO_LIST)).isDisplayed()) {
                completedItemsCount++;
            }
        }
        return completedItemsCount == getTodosListSize();
    }

    public boolean isCompletedItemByLabel(String label) {
        boolean isCompletedItem = false;
        for (WebElement element : getTodosList()) {
            if (element.findElement(By.tagName(LABEL_OF_ITEM)).getText().equals(label)) {
                if (element.findElement(By.xpath(COMPLETED_TODO_LIST)).isDisplayed()) {
                    isCompletedItem = true;
                }
            }
        }
        return isCompletedItem;
    }

    private List<WebElement> getTodosList() {
        return driver.findElements(By.xpath(TODOS_LIST));
    }

    private List<WebElement> getAllCheckboxes() {
        return driver.findElements(By.xpath(CHECKBOXES));
    }
}
