package com.epam.test.HomeWork16;

import com.epam.test.HomeWork16.pages.HomePage;
import com.epam.test.HomeWork16.utils.SeleniumHelper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReactTodosTest {

    public static final String URI = "http://todomvc.com/";

    public static final Logger LOG = Logger.getLogger(ReactTodosTest.class.getName());

    public WebDriver driver;

    public HomePage homePage;

    @BeforeEach
    public void openDriver() {
        System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\chromedriver.exe");
        driver = SeleniumHelper.startChromeDriver(URI);
        LOG.info(URI + " is opened");
        homePage = new HomePage(driver);
    }

    @Test
    public void fillTodosListTest() {
        int actualTodosListSize;
        int expectedTodosListSize = 3;
        actualTodosListSize = homePage
                .getReactExample()
                .addNewItem("Todo1")
                .addNewItem("Todo2")
                .addNewItem("Todo3")
                .getTodosListSize();
        LOG.info("Todos list size = " + actualTodosListSize);
        assertEquals(actualTodosListSize, expectedTodosListSize);
    }

    @Test
    public void markTodosListItemLikeCompletedByLabel() {
        boolean isCompletedItem;
        isCompletedItem = homePage
                .getReactExample()
                .addNewItem("Todo1")
                .addNewItem("Todo2")
                .markItemCompleteByLabel("Todo1")
                .isCompletedItemByLabel("Todo1");
        LOG.info("Completed item - " + isCompletedItem);
        assertTrue(isCompletedItem);
    }

    @Test
    public void markAllTodosListItemsLikeCompletedTest() {
        boolean isCompletedAllItems;
        isCompletedAllItems = homePage
                .getReactExample()
                .addNewItem("Todo1")
                .addNewItem("Todo2")
                .addNewItem("Todo3")
                .markAllItemsCompleted()
                .isCompletedAllItems();
        LOG.info("Completed all items from Todos list - " + isCompletedAllItems);
        assertTrue(isCompletedAllItems);
    }

    @AfterEach
    public void closeDriver() {
        driver.close();
    }
}
