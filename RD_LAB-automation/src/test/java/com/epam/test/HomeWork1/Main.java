package com.epam.test.HomeWork1;

public class Main {
    public static void main(String[] args) {
        System.out.println(WarmupOne.sleepIn(false, false));
        System.out.println(WarmupOne.monkeyTrouble(true, true));
        System.out.println(WarmupOne.sumDouble(1, 2));
        System.out.println(WarmupOne.diff21(19));
        System.out.println(WarmupOne.makes10(9, 10));
        System.out.println(WarmupOne.nearHundred(93));
        System.out.println(WarmupOne.posNeg(1, -1, false));
        System.out.println(WarmupOne.notString("candy"));
        System.out.println(WarmupOne.missingChar("Kitten", 1));
        System.out.println(WarmupOne.frontBack("code"));
        System.out.println(WarmupOne.front3("Java"));
        System.out.println(WarmupOne.backAround("cat"));
        System.out.println(WarmupOne.or35(8));
        System.out.println(WarmupOne.front22("kitten"));
        System.out.println(WarmupOne.startHi("hi there"));
        System.out.println(WarmupOne.icyHot(2, 120));
        System.out.println(WarmupOne.in1020(12, 99));
        System.out.println(WarmupOne.hasTeen(13, 20, 10));
        System.out.println(WarmupOne.loneTeen(13, 99));
        System.out.println(WarmupOne.delDel("adelbc"));
        System.out.println(WarmupOne.mixStart("piz snacks"));
        System.out.println(WarmupOne.startOz("oxx"));
        System.out.println(WarmupOne.intMax(1, 2, 3));
        System.out.println(WarmupOne.close10(8, 13));
        System.out.println(WarmupOne.in3050(30, 31));
        System.out.println(WarmupOne.max1020(11, 19));
        System.out.println(WarmupOne.stringE("Hello"));
        System.out.println(WarmupOne.lastDigit(7, 17));
        System.out.println(WarmupOne.endUp("Hello"));
        System.out.println(WarmupOne.everyNth("Miracle", 2));
    }
}
