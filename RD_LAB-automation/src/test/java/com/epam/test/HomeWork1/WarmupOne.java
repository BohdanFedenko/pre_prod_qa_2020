package com.epam.test.HomeWork1;

public class WarmupOne {

    //   Task1: sleepIn
//   The parameter   weekday is true if it is a weekday, and the
//   parameter vacation is true if we are on vacation. We sleep
//   in if it is not a weekday or we're on vacation. Return true if we sleep in.
    public static boolean sleepIn(boolean weekday, boolean vacation){
        if(!weekday || vacation) return true;
        else return false;
    }
    //    Task2: monkeyTrouble
//    We have two monkeys, a and b, and the parameters aSmile and
//    bSmile indicate if each is smiling. We are in trouble if they
//    are both smiling or if neither of them is smiling. Return true
//    if we are in trouble.
    public static boolean monkeyTrouble(boolean aSmile, boolean bSmile) {
        if ((!aSmile && bSmile) || (!aSmile && !bSmile)) return true;
        else return false;
    }
    //    Task3: sumDouble
//    Given two int values, return their sum. Unless the two values are
//    the same, then return double their sum.
    public static int sumDouble(int a, int b) {
        if(a==b) return 2*(a+b);
        return a+b;
    }
    //    Task4: diff21
//    Given an int n, return the absolute difference between n and 21,
//    except return double the absolute difference if n is over 21.
    public static int diff21(int n) {
        int sub=Math.abs(n-21);
        if(n>21) return 2*sub;
        else return sub;
    }
    //    Task5: parrotTrouble
//    We have a loud talking parrot.
//    The "hour" parameter is the current hour time in the range
//    0..23. We are in trouble if the parrot is talking and the hour
//    is before 7 or after 20. Return true if we are in trouble.
    public static boolean parrotTrouble(boolean talking, int hour) {
        if(talking && (hour<7 || hour >20)) return true;
        else return false;
    }
    //    Task6: makes10
//    Given 2 ints, a and b, return true if one if
//    them is 10 or if their sum is 10.
    public static boolean makes10(int a, int b) {
        if(a==10 || b==10 || (a+b)==10) return true;
        else return false;
    }
    //    Task7: nearHundred
//    Given an int n, return true if it is within 10 of 100
//    or 200. Note: Math.abs(num) computes the absolute value
//    of a number.
    public static boolean nearHundred(int n) {
        int num1 = Math.abs(n-100);
        int num2 = Math.abs(n-200);
        if(num1<=10 || num2<=10) return true;
        else return false;
    }
    //    Task8: posNeg
//    Given 2 int values, return true if one is negative and
//    one is positive. Except if the parameter "negative" is
//    true, then return true only if both are negative.
    public static boolean posNeg(int a, int b, boolean negative) {
        if(negative && a<0 && b<0) return true;
        else if (!negative && ((a>=0 && b<0) || (a<0 && b>=0))) return true;
        else return false;
    }
    //    Task9: notString
//    Given a string, return a new string where "not " has been added to
//    the front. However, if the string already begins with "not", return
//    the string unchanged. Note: use .equals() to compare 2 strings.
    public static String notString(String str) {
        String n = "not";
        if(str.length() < 3){
            return n+" "+str;
        }
        else{
            String s = str.substring(0,3);
            if(s.equals(n)) return str;
            else return n+" "+str;
        }
    }
    //    Task10: missingChar
//    Given a non-empty string and an int n, return a new string
//    where the char at index n has been removed. The value of n
//    will be a valid index of a char in the original string
//    (i.e. n will be in the range 0..str.length()-1 inclusive).
    public static String missingChar(String str, int n) {
        String newStr="";
        for(int i=0; i <= str.length()-1;i++){
            if(i==n) continue;
            newStr+=str.charAt(i);
        }
        return newStr;
    }
    //    Task11: frontBack
//    Given a string, return a new string where the first and
//    last chars have been exchanged.
    public static String frontBack(String str) {
        int n = str.length();
        if(n==0 || n==1){
            return str;
        }
        else{
            char ch1=str.charAt(0);
            char ch2=str.charAt(n-1);
            String newStr="";
            for(int i=0; i<=n-1;i++){
                if(i==0){
                    newStr+=ch2;
                    continue;
                }
                if(i==n-1){
                    newStr+=ch1;
                }
                else
                    newStr+=str.charAt(i);
            }
            return newStr;
        }
    }
    //    Task12: front3
//    Given a string, we'll say that the front is the first 3
//    chars of the string. If the string length is less than 3,
//    the front is whatever is there. Return a new string which is
//    3 copies of the front.
    public static String front3(String str) {
        int length = str.length();
        if(length<3){
            return str+str+str;
        }
        else{
            String newStr=str.substring(0,3);
            return newStr+newStr+newStr;
        }

    }
    //    Task13:  backAround
//    Given a string, take the last char and return a new string
//    with the last char added at the front and back, so "cat"
//    yields "tcatt". The original string will be length 1 or more.
    public static String backAround(String str) {
        int length = str.length();
        if(length==0){
            return str;
        }
        else{
            char ch=str.charAt(length-1);
            return ch+str+ch;
        }
    }
    //   Task14: or35
//   Return true if the given non-negative number is
//   a multiple of 3 or a multiple of 5. Use the % "mod"
//   operator -- see Introduction to Mod
    public static boolean or35(int n) {
        if(n%3==0 || n%5==0) return true;
        else return false;
    }
    //    Task15: front22
//    Given a string, take the first 2 chars and return the
//    string with the 2 chars added at both the front and back,
//    so "kitten" yields"kikittenki". If the string length is less
//    than 2, use whatever chars are there.
    public static String front22(String str) {
        if(str.length()<2){
            return str+str+str;
        }
        else{
            String s=str.substring(0,2);
            return s+str+s;
        }
    }
    //    Task16: startHi
//    Given a string, return true if the string starts with "hi" and
//    false otherwise.
    public static boolean startHi(String str) {
        if(str.length()<2){
            return false;
        }
        else{
            String s=str.substring(0,2);
            if(s.equals("hi")){
                return true;
            }
            else{
                return false;
            }
        }
    }
    //    Task17: icyHot
//    Given two temperatures, return true
//    if one is less than 0 and the other is greater than 100.
    public static boolean icyHot(int temp1, int temp2) {
        if((temp1<0 && temp2>100)||(temp1>100 && temp2<0))
            return true;
        else
            return false;
    }
    //    Task18: in1020
//    Given 2 int values, return true if either
//    of them is in the range 10..20 inclusive.
    public static boolean in1020(int a, int b) {
        if((a>=10 && a<=20)||(b>=10 && b<=20))
            return true;
        else
            return false;
    }
    //    Task19: hasTeen
//    We'll say that a number is "teen" if it is
//    in the range 13..19 inclusive. Given 3 int values,
//    return true if 1 or more of them are teen.
    public static boolean hasTeen(int a, int b, int c) {
        if((a>=13 && a<=19)||(b>=13 && b<=19)||(c>=13 && c<=19))
            return true;
        else
            return false;
    }
    //    Task20: loneTeen
//    We'll say that a number is "teen" if it is in the range
//    13..19 inclusive. Given 2 int values, return true if one
//    or the other is teen, but not both.
    public static boolean loneTeen(int a, int b) {
        boolean ab=a>=13 && a<=19;
        boolean bb=b>=13 && b<=19;
        if(ab!=bb) return true;
        else return false;
    }
    //    Task21: delDel
//   Given a string, if the string "del" appears starting at index 1,
//   return a string where that "del" has been deleted. Otherwise,
//   return the string unchanged.
    public static String delDel(String str) {
        int length=str.length();
        if(length<4){
            return str;
        }
        else{
            String s=str.substring(1,4);
            if(s.equals("del")){
                str=str.replace(s,"");
            }
            return str;
        }
    }
    //    Task22: mixStart
//    Return true if the given string begins with "mix",
//    except the 'm' can be anything, so "pix", "9ix" ..
//    all count.
    public static boolean mixStart(String str) {
        int length=str.length();
        if(length<3){
            return false;
        }
        else{
            String s=str.substring(1,3);
            if(s.equals("ix")){
                return true;
            }
            else{
                return false;
            }

        }
    }
    //    Task23: startOz
//    Given a string, return a string made of the first 2
//    chars (if present), however include first char only if
//    it is 'o' and include the second only if it is 'z', so
//    "ozymandias" yields "oz".
    public static String startOz(String str) {
        int length=str.length();
        if(length<=1){
            if(length==1 && str.charAt(0)=='o')
                return "o";
            return "";
        }
        else{
            char ch1 =str.charAt(0);
            char ch2=str.charAt(1);
            if(ch1=='o' && ch2=='z')
                return "oz";
            if(ch1=='o') return "o";
            if(ch2=='z') return "z";
            else return "";
        }
    }
    //    Task 24: intMax
//    Given three int values, a b c, return the largest.
    public static int intMax(int a, int b, int c) {
        int [] array ={a,b,c};
        int max=array[0];
        for(int i=0; i<array.length;i++){
            if(max<array[i]) max=array[i];
        }
        return max;
    }
    //    Task25: close10
//    Given 2 int values, return whichever value is
//    nearest to the value 10, or return 0 in the event of a tie.
//    Note that Math.abs(n) returns the absolute value of a number.
    public static int close10(int a, int b) {
        int subA = Math.abs(a-10);
        int subB = Math.abs(b-10);
        if(subA<subB) return a;
        if(subA>subB) return b;
        else return 0;
    }
    //    Task26: in3050
//    Given 2 int values, return true if they are both in
//    the range 30..40 inclusive, or they are both
//    in the range 40..50 inclusive.
    public static boolean in3050(int a, int b) {
        if((a>=30 && a<=40) && (b>=30 && b<=40)){
            return true;
        }
        if((a>=40 && a<=50) && (b>=40 && b<=50)){
            return true;
        }
        else
            return false;
    }
    //    Task27: max1020
//   Given 2 positive int values, return the larger
//   value that is in the range 10..20 inclusive, or return 0
//   if neither is in that range.
    public static int max1020(int a, int b) {
        boolean ab = a>=10 && a<=20;
        boolean bb = b>=10 && b<=20;
        if(!ab && !bb) return 0;
        else if(ab && bb){
            if(a>b) return a;
            if(a<b) return b;
            else return 0;
        }
        else if(ab) return a;
        else if(bb) return b;
        else return 0;
    }
    //   Task28: stringE
//   Return true if the given string contains between 1 and 3 'e' chars.
    public static boolean stringE(String str) {
        int counter = 0;
        for(int i=0; i<=str.length()-1;i++){
            if(str.charAt(i)=='e'){
                counter++;
            }
        }
        if(counter>=1 && counter <=3)
            return true;
        else
            return false;
    }
    //    Task29: lastDigit
//    Given two non-negative int values, return true if they
//    have the same last digit, such as with 27 and 57. Note
//    that the % "mod" operator computes remainders, so 17 %
//    10 is 7.
    public static boolean lastDigit(int a, int b) {
        int num1=a%10;
        int num2=b%10;
        if(num1==num2) return true;
        else return false;
    }
    //    Task30: endUp
//    Given a string, return a new string where the last 3
//    chars are now in upper case. If the string has less than
//    3 chars, uppercase whatever is there. Note that
//    str.toUpperCase() returns the uppercase version of a string.
    public static String endUp(String str) {
        if(str.length()<3)
            return str.toUpperCase();
        else{
            String s=str.substring(str.length()-3,str.length());
            String newS=str.replace(s,"");
            s=s.toUpperCase();
            return newS+s;
        }
    }
    //    Task31:  everyNth
//    Given a non-empty string and an int N, return the string
//    made starting with char 0, and then every Nth char of the
//    string. So if N is 3, use char 0, 3, 6, ... and so on.
//    N is 1 or more.
    public static String everyNth(String str, int n) {
        int length=str.length();
        String newStr="";
        if(length==0 || n<1)
            return str;
        else{
            for(int i=0; i<length;i+=n){
                newStr+=str.charAt(i);
            }
            return newStr;
        }
    }
}





