package com.epam.test.HomeWork8.task3;

import com.epam.test.HomeWork5.task1.*;
import com.epam.test.HomeWork5.task2.SimpleCalculator;

import java.io.IOException;

public class ObjectCreator {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        TaxiCab minibus = new Minibus("Ford", 10, 25000, 10,"Economy",18);
        TaxiCab minivan = new Minivan("Hyundai",8,20000,136,"Standart",8);
        TaxiCab sedan = new Sedan("BMW",6,60000,1,"Business", 3);
        TaxiCab limousine = new Limousine("Lincoln", 7,150500,36,"Luxury",12);

        TaxiStation taxiStation = new TaxiStation("Victory", new TaxiCab[] {minibus, minivan, sedan, limousine});

        Serializer.serialize("TaxiStation.out",taxiStation);
        TaxiStation taxiStation1 = (TaxiStation) Serializer.deSerialize("TaxiStation.out");
        System.out.println(taxiStation1.getAllCarsCost());

        SimpleCalculator simpleCalculator = new SimpleCalculator("SimpleCalculator");

        Serializer.serialize("SimpleCalculator.out", simpleCalculator);
        SimpleCalculator simpleCalculator1 = (SimpleCalculator) Serializer.deSerialize("SimpleCalculator.out");
        simpleCalculator1.input(5, 4);
        System.out.println(simpleCalculator1.calculate().add());
    }
}
