package com.epam.test.HomeWork8.task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class TextAnalyzer {

    private String filePath;

    private String operationType;

    private String quitChecker;

    public void run() throws FileNotFoundException {
        do{
            input();
            if (quitChecker.equals("quit")){
                break;
            }
        }while (!getOperation());
    }

    // filePath = "./src/test/resources/testFile.txt";
    private void input(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter a path to file");
        filePath=scanner.nextLine();
        System.out.println("Please, enter an operation type");
        operationType=scanner.nextLine();
        System.out.println("For quit enter 'quit', for continue any other letters");
        quitChecker = scanner.next();
    }

    private boolean getOperation() throws FileNotFoundException {
        System.out.println("Words from the file:\n" + getWords());
        if ("frequency".equals(operationType)) {
            getFrequency();
        } else if ("length".equals(operationType)) {
            getLength();
        } else if ("duplicates".equals(operationType)) {
            getDuplicates();
        } else {
            System.out.println("Choice is incorrect");
        }
        return false;
    }

    private List<String> getWords() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(filePath));
        List<String > words = new ArrayList<String>();
        while (scanner.hasNext()) {
            String word = scanner.useDelimiter("[\\,]\\s+|[\\.]?\\s+|[\\,.]+|[\\.]+|[-?\\d]++").next();
            words.add(word);
        }
        return words;
    }

    private Map<String,Integer> getWordAmound() throws FileNotFoundException {
        Map<String, Integer> statistics = new HashMap<String, Integer>();
        for (String word: getWords()) {
            Integer count = statistics.get(word);
            if(count == null){
                count = 0;
            }
            statistics.put(word, ++count);
        }
        return statistics;
    }

    private void getFrequency() throws FileNotFoundException {
        List words = new ArrayList(getWordAmound().entrySet());
        Collections.sort(words, new Comparator<Map.Entry<String,Integer>>() {
            public int compare(Map.Entry<String, Integer> word1, Map.Entry<String, Integer> word2) {
                return word2.getValue()-word1.getValue();
            }
        });
        List twoWords = new ArrayList();
        twoWords.add(words.get(0));
        twoWords.add(words.get(1));
        Collections.sort(twoWords, new Comparator<Map.Entry<String,Integer>>() {
            public int compare(Map.Entry<String, Integer> word1, Map.Entry<String, Integer> word2) {
                return word2.getKey().compareTo(word1.getKey());
            }
        });
        System.out.println("Two most common words in this file:");
        for (Object word: twoWords) {
            System.out.println(word);
        }
    }

    private void getLength() throws FileNotFoundException {
        List<String> longestWords = new ArrayList<String>();
        int longestWord=getWords().get(0).length();
        for (int i = 0; i <getWords().size() ; i++) {
            if(getWords().get(i).length() >= longestWord){
                longestWords.add(getWords().get(i));
                longestWord=getWords().get(i).length();
                if(longestWords.size()==3){
                    break;
                }
            }
        }
        Collections.sort(longestWords, new Comparator<String>() {
            @Override
            public int compare(String word1, String word2) {
                return word2.length()-word1.length();
            }
        });
        System.out.println("The first three of the longest words in this file:");
        for (String word:longestWords) {
            System.out.println(word + "=" + word.length());
        }
    }

    private void getDuplicates() throws FileNotFoundException {
        List<String> duplicateWords = new ArrayList<String>();
        for (String word: getWords()) {
            if(getWordAmound().containsKey(word) && getWordAmound().get(word) > 1){
                duplicateWords.add(word.toUpperCase());
                if(duplicateWords.size()==3){
                    break;
                }
            }
        }
        Collections.sort(duplicateWords, new Comparator<String>() {
            @Override
            public int compare(String word1, String word2) {
                return word1.length()-word2.length();
            }
        });
        System.out.println("The first three of the words with duplicates in this file:");
        for (String word: duplicateWords) {
            System.out.println(new StringBuilder(word).reverse().toString());
        }
    }
}
