package com.epam.test.HomeWork8.task2;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class ObjectsManager {
    public static void main(String[] args) {
        StringLengthHashCodeGenerator hashCodeTypeOne1 = new StringLengthHashCodeGenerator("apple");
        StringLengthHashCodeGenerator hashCodeTypeOne2 = new StringLengthHashCodeGenerator("banana");

        StringCharsSumHashCodeGenerator hashCodeTypeTwo1 = new StringCharsSumHashCodeGenerator("apple");
        StringCharsSumHashCodeGenerator hashCodeTypeTwo2 = new StringCharsSumHashCodeGenerator("banana");

        Map<String, StringLengthHashCodeGenerator> hashMapWithHashCodeTypeOne = new HashMap<>();
        Map<String, StringLengthHashCodeGenerator> linkedHashMapWithHashCodeTypeOne = new LinkedHashMap<>();

        Map<String, StringCharsSumHashCodeGenerator> hashMapWithHashCodeTypeTwo = new HashMap<>();
        Map<String, StringCharsSumHashCodeGenerator> linkedHashMapWithHashCodeTypeTwo = new LinkedHashMap<>();

        hashMapWithHashCodeTypeOne.put(hashCodeTypeOne1.getKey(),hashCodeTypeOne1);
        hashMapWithHashCodeTypeOne.put(hashCodeTypeOne2.getKey(),hashCodeTypeOne2);
        hashMapWithHashCodeTypeTwo.put(hashCodeTypeTwo1.getKey(),hashCodeTypeTwo1);
        hashMapWithHashCodeTypeTwo.put(hashCodeTypeTwo2.getKey(),hashCodeTypeTwo2);

        linkedHashMapWithHashCodeTypeOne.put(hashCodeTypeOne1.getKey(), hashCodeTypeOne1);
        linkedHashMapWithHashCodeTypeOne.put(hashCodeTypeOne2.getKey(), hashCodeTypeOne2);
        linkedHashMapWithHashCodeTypeTwo.put(hashCodeTypeTwo1.getKey(), hashCodeTypeTwo1);
        linkedHashMapWithHashCodeTypeTwo.put(hashCodeTypeTwo2.getKey(), hashCodeTypeTwo2);

        System.out.println("Using hashcode with type one:\n");
        System.out.println("HashMap with object with HASHCODE equal string length:");
        Iterator<Map.Entry<String, StringLengthHashCodeGenerator>> temp = hashMapWithHashCodeTypeOne.entrySet().iterator();
        while (temp.hasNext()) {
            Map.Entry<String, StringLengthHashCodeGenerator> pair = temp.next();
            System.out.println(pair.getKey() + " hashcode= " + pair.getKey().hashCode()
                               + "; Object: " + pair.getValue() + " hashcode= " + pair.getValue().hashCode());
        }
        System.out.println();
        System.out.println("LinkedHashMap with object with HASHCODE equal string length:");
        Iterator<Map.Entry<String, StringLengthHashCodeGenerator>> temp1 = linkedHashMapWithHashCodeTypeOne.entrySet().iterator();
        while (temp1.hasNext()) {
            Map.Entry<String, StringLengthHashCodeGenerator> pair = temp1.next();
            System.out.println(pair.getKey() + " hashcode= " + pair.getKey().hashCode()
                    + "; Object: " + pair.getValue() + " hashcode= " + pair.getValue().hashCode() );
        }
        System.out.println();
        System.out.println("Using hashcode with type two:\n");
        System.out.println("HashMap with object with HASHCODE equal sum first four chars of string key:");
        Iterator<Map.Entry<String, StringCharsSumHashCodeGenerator>> temp2 = hashMapWithHashCodeTypeTwo.entrySet().iterator();
        while (temp2.hasNext()) {
            Map.Entry<String, StringCharsSumHashCodeGenerator> pair = temp2.next();
            System.out.println(pair.getKey() + " hashcode= " + pair.getKey().hashCode()
                    + "; Object: " + pair.getValue() + " hashcode= " + pair.getValue().hashCode() );
        }
        System.out.println();
        System.out.println("HashMap with object with HASHCODE equal sum first four chars of string key:");
        Iterator<Map.Entry<String, StringCharsSumHashCodeGenerator>> temp3 = linkedHashMapWithHashCodeTypeTwo.entrySet().iterator();
        while (temp3.hasNext()) {
            Map.Entry<String, StringCharsSumHashCodeGenerator> pair = temp3.next();
            System.out.println(pair.getKey() + " hashcode= " + pair.getKey().hashCode()
                    + "; Object: " + pair.getValue() + " hashcode= " + pair.getValue().hashCode());
        }

    }
}
