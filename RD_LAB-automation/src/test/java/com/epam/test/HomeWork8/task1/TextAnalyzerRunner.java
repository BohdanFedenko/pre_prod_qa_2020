package com.epam.test.HomeWork8.task1;

import java.io.FileNotFoundException;

public class TextAnalyzerRunner {
    public static void main(String[] args) throws FileNotFoundException {
        TextAnalyzer textAnalyzer = new TextAnalyzer();
        textAnalyzer.run();
    }
}
