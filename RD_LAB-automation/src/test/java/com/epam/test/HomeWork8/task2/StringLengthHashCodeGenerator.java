package com.epam.test.HomeWork8.task2;

public class StringLengthHashCodeGenerator {

    private  String key;

    public StringLengthHashCodeGenerator(String key){
        this.key=key;
    }

    public String getKey(){
        return key;
    }

    @Override
    public int hashCode() {
        return key.length();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        StringLengthHashCodeGenerator hashCodeTypeOne = (StringLengthHashCodeGenerator) obj;
        if (!key.equals(hashCodeTypeOne.key)) return false;
        return true;
    }
}
