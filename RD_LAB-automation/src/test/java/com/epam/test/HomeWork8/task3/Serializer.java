package com.epam.test.HomeWork8.task3;

import java.io.*;

public class Serializer {

    public static void serialize(String fileName, Object object) throws IOException {
        ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(fileName));
        output.writeObject(object);
        output.flush();
        output.close();
    }

    public static Serializable deSerialize(String fileName) throws IOException, ClassNotFoundException {
        ObjectInputStream input = new ObjectInputStream(new FileInputStream(fileName));
        return (Serializable) input.readObject();
    }

}
