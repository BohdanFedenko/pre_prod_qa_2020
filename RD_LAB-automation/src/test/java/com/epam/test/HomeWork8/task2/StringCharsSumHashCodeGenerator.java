package com.epam.test.HomeWork8.task2;

public class StringCharsSumHashCodeGenerator {

    private  String key;

    public StringCharsSumHashCodeGenerator(String key){
        this.key=key;
    }

    public String getKey(){
        return key;
    }

    @Override
    public int hashCode() {
        int hashCode = 0;
        hashCode = key.charAt(1) + key.charAt(2) + key.charAt(3) + key.charAt(4);
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        StringCharsSumHashCodeGenerator hashCodeTypeTwo = (StringCharsSumHashCodeGenerator) obj;
        if (!key.equals(hashCodeTypeTwo.key)) return false;
        return true;
    }
}
