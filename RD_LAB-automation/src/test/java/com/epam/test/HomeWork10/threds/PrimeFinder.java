package com.epam.test.HomeWork10.threds;

import java.util.concurrent.CopyOnWriteArrayList;

public class PrimeFinder implements Runnable {

    private RangeManager rangeManager;

    private static CopyOnWriteArrayList<String> primeNumbers = new CopyOnWriteArrayList<>();

    public PrimeFinder(RangeManager rangeManager) {
        this.rangeManager = rangeManager;
    }

    public CopyOnWriteArrayList<String> getPrimeNumbers() {
        return primeNumbers;
    }

    private int isPrimeNumber(int number) {
        int counter = 0;
        for (int i = 2; i <= number; i++) {
            if (number % i == 0) {
                counter++;
            }
        }
        return counter;
    }

    private void searchPrimeNumber() {
        for (int number : rangeManager.getPartOfRange()) {
            if (isPrimeNumber(number) == 1) {
                primeNumbers.add(number + " - " + Thread.currentThread().getName());
            }
        }
    }

    @Override
    public void run() {
        synchronized (this) {
            searchPrimeNumber();
        }
    }
}
