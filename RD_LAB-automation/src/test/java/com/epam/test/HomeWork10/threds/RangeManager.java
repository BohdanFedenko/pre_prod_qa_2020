package com.epam.test.HomeWork10.threds;

public class RangeManager {


    private int rightBorder;

    private int leftBorder;

    private int interval;

    private int upperGapLimit;

    private int gapSize;

    private static int iterationCounter = 1;

    public RangeManager(int rightBorder, int leftBorder, int interval) {
        this.rightBorder = rightBorder;
        this.leftBorder = leftBorder;
        this.interval = interval;
        countGapSize();
        countUpperGapLimit();
    }

    private void countGapSize() {
        this.gapSize = (rightBorder - leftBorder) / interval;
    }

    private void countUpperGapLimit() {
        this.upperGapLimit = gapSize + leftBorder;
    }

    public int[] getPartOfRange() {
        int[] gapSizeRange = new int[upperGapLimit - leftBorder];
        int index = 0;
        for (int i = leftBorder; i < upperGapLimit; i++) {
            gapSizeRange[index] = i;
            index++;
        }
        iterationCounter++;
        leftBorder = upperGapLimit;
        upperGapLimit = leftBorder + gapSize;
        if (iterationCounter == interval) {
            upperGapLimit = rightBorder + 1;
        }
        return gapSizeRange;
    }
}
