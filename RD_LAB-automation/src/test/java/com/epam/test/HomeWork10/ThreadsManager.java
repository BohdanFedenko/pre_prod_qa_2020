package com.epam.test.HomeWork10;

import com.epam.test.HomeWork10.threds.PrimeFinder;
import com.epam.test.HomeWork10.threds.RangeManager;
import com.epam.test.HomeWork10.utils.ReadFromConsole;
import com.epam.test.HomeWork10.utils.WriteToConsole;

public class ThreadsManager {

    static PrimeFinder primeFinder;

    public static void main(String[] args) throws InterruptedException {

        ReadFromConsole reader = new ReadFromConsole();
        reader.read();

        WriteToConsole writer = new WriteToConsole();
        writer.print(reader.getLeftBorder(), reader.getRightBorder(), "The source range of numbers");

        RangeManager rangeManager = new RangeManager(reader.getRightBorder(), reader.getLeftBorder(), reader.getNumberOfThreads());

        primeFinder = new PrimeFinder(rangeManager);

        startThread(primeFinder, reader.getNumberOfThreads());

        writer.print(primeFinder.getPrimeNumbers(), "The prime numbers list:");
    }

    private static void startThread(Object object, int amountOfThreads) throws InterruptedException {
        Thread[] arrayThreads = new Thread[amountOfThreads];
        for (int i = 0; i < arrayThreads.length; i++) {
            (arrayThreads[i] = new Thread((Runnable) object)).start();
        }
        for (Thread arrayThread : arrayThreads) {
            arrayThread.join();
        }
    }
}
