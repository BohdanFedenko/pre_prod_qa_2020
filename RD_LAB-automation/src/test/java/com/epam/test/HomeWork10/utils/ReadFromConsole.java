package com.epam.test.HomeWork10.utils;

import java.util.Scanner;
import java.util.logging.Logger;

public class ReadFromConsole {

    private static Logger logger = Logger.getLogger(ReadFromConsole.class.getName());

    private int leftBorder;

    private int rightBorder;

    private int numberOfThreads;

    public int getLeftBorder() {
        return leftBorder;
    }

    public int getRightBorder() {
        return rightBorder;
    }

    public int getNumberOfThreads() {
        return numberOfThreads;
    }

    public void read() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a range to search for prime numbers from 0 to 100 000:");
        System.out.println("Enter the left border:");
        leftBorder = scanner.nextInt();
        System.out.println("Enter the right border:");
        rightBorder = scanner.nextInt();
        System.out.println("Enter the number of threads:");
        numberOfThreads = scanner.nextInt();
        if (numberOfThreads > rightBorder) {
            logger.info("The entered number of threads bigger than right border\n" +
                    "so the number of gaps set equals right border");
            numberOfThreads = rightBorder;
        }
    }
}
