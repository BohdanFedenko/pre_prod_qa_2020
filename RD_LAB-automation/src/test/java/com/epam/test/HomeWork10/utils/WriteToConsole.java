package com.epam.test.HomeWork10.utils;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Logger;

public class WriteToConsole {

    private static Logger logger = Logger.getLogger(WriteToConsole.class.getName());

    public void print(CopyOnWriteArrayList<String> list, String message) {
        logger.info(message);
        for (String word : list) {
            System.out.println(word);
        }
    }

    public void print(int startNumber, int endNumber, String message) {
        logger.info(message);
        for (int i = startNumber; i <= endNumber; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
