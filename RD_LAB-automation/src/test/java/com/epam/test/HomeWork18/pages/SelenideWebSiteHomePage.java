package com.epam.test.HomeWork18.pages;

import com.codeborne.selenide.Condition;

import static com.codeborne.selenide.Selenide.$x;

public class SelenideWebSiteHomePage {

    public String getTitle() {
        return $x("//div[@class = 'news']//a").shouldBe(Condition.visible).getText();
    }
}
