package com.epam.test.HomeWork18.pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class GoogleHomePage {

    public GoogleResultSearchPage search(String query) {
        $(By.name("q")).setValue(query).pressEnter();
        return page(GoogleResultSearchPage.class);
    }
}
