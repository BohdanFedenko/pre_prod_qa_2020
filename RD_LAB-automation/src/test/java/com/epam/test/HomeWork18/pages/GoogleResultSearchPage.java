package com.epam.test.HomeWork18.pages;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class GoogleResultSearchPage {

    public SelenideWebSiteHomePage getFirstResult() {
        $x("//div[@class='TbwUpd NJjxre']//cite[@class='iUh30 gBIQub tjvcx'][contains(text(),'selenide')]").click();
        return page(SelenideWebSiteHomePage.class);
    }
}
