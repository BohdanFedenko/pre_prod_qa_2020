package com.epam.test.HomeWork18.utils;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.junit.ScreenShooter;
import org.junit.Rule;

public class TestConfigurator {

    private static final String URL = "https://www.google.com/";

    public static final String CHROME = "chrome";

    public static final String FIREFOX = "firefox";

    public static final String INTERNET_EXPLORER = "ie";

    public static final String OPERA = "opera";

    public static final String EDGE = "edge";

    public String getUrl() {
        return URL;
    }

    public static void openDriver() {
        Configuration.startMaximized = true;
        Selenide.open(URL);
    }

    public static void openDriver(String browser) {
        Configuration.startMaximized = true;
        Configuration.browser = browser;
        Selenide.open(URL);
    }

    @Rule
    public ScreenShooter makeScreenshotOnFailure = ScreenShooter.failedTests().succeededTests();
}
