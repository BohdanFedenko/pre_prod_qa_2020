package com.epam.test.HomeWork18;

import com.epam.test.HomeWork18.pages.GoogleHomePage;
import com.epam.test.HomeWork18.pages.GoogleResultSearchPage;
import com.epam.test.HomeWork18.pages.SelenideWebSiteHomePage;
import com.epam.test.HomeWork18.utils.TestConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class SeleniumSearchTest extends TestConfigurator {

    private static GoogleHomePage googleHomePage;

    private static GoogleResultSearchPage googleResultSearchPage;

    private static SelenideWebSiteHomePage selenideWebSiteHomePage;

    @BeforeClass
    public static void setUp() {
        openDriver(TestConfigurator.CHROME);
        googleHomePage = new GoogleHomePage();
        googleResultSearchPage = new GoogleResultSearchPage();
        selenideWebSiteHomePage = new SelenideWebSiteHomePage();
    }

    @Test
    public void checkSearchSelenideWebSiteTest() {
        googleHomePage.search("Selenide");
        googleResultSearchPage.getFirstResult();
        Assertions.assertTrue(selenideWebSiteHomePage.getTitle().contains("Selenide"));
    }
}
