package com.epam.test.HomeWork2.task1;

public enum Courses {
    ManualTesting(16),
    RequirementsTesting(4),
    JavaBasic(15),
    JavaProfessional(40);

    private int duration;

    public int getDuration(){
        return duration;
    }

    Courses(int duration){
        this.duration=duration;
    }
}
