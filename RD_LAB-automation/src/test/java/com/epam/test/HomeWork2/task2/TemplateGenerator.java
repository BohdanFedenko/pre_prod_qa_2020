package com.epam.test.HomeWork2.task2;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TemplateGenerator {
    private static final String REGEX_TEMPLATE ="[${]+[^$]+[}]";

    private Scanner sc;

    private String template;

    private StringBuilder builderString=new StringBuilder();

    private int count=0;

    private Pattern pattern;

    private Matcher matcher;

    private ArrayList<String> placeholders=new ArrayList<>();

    private ArrayList<String> variables=new ArrayList<>();

    public TemplateGenerator(){
        sc=new Scanner(System.in);
    }

    public void generate(){
        input();
        print();
    }

    private void input(){
        System.out.println("Enter template");
        template=sc.nextLine();
        if(template.matches(REGEX_TEMPLATE)){
            System.out.println("Error, there is no suck template");
            return;
        }

        pattern = Pattern.compile(REGEX_TEMPLATE);

        matcher = pattern.matcher(template);

        while(matcher.find())
            placeholders.add(matcher.group().substring(2,matcher.group().length()-1));
        for(String pl:placeholders){
            System.out.println("Enter variables for \"" + pl + "\" placeholder: ");
            variables.add(sc.nextLine());
        }

        for(String text:template.split(REGEX_TEMPLATE)){
            builderString.append(text);
            if(count<variables.size()){
                builderString.append(variables.toArray()[count]);
                count++;
            }
        }
        sc.close();
    }

    private void print(){
        System.out.println(builderString);
    }
}
