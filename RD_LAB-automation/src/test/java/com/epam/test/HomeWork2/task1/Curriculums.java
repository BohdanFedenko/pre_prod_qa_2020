package com.epam.test.HomeWork2.task1;

public enum Curriculums {
    QA_ENGINEER( Courses.ManualTesting, Courses.RequirementsTesting){
        @Override
        public int getDuration() {
            return Courses.ManualTesting.getDuration() + Courses.RequirementsTesting.getDuration();
        }
    },
    TAQA_ENGINEER(Courses.ManualTesting, Courses.JavaBasic){
        @Override
        public int getDuration() {
            return Courses.ManualTesting.getDuration() + Courses.JavaBasic.getDuration();
        }
    },
    JAVA_DEVELOPER(Courses.JavaBasic,Courses.JavaProfessional){
        public int getDuration() {
            return Courses.JavaBasic.getDuration() + Courses.JavaProfessional.getDuration();
        }
    };

    private Courses courseOne;

    private Courses CourseTwo;

    Curriculums(Courses courseOne, Courses courseTwo){
        this.courseOne=courseOne;
        this.CourseTwo=courseTwo;
    }

    public abstract int getDuration();
}
