package com.epam.test.HomeWork2.task1;

import java.time.LocalDate;

public enum Students {
    STUDENTOne("Ivan","Ivanov", Curriculums.JAVA_DEVELOPER, LocalDate.of(2020,8,10)),
    STUDENTTwo("Petr","Petrov", Curriculums.QA_ENGINEER, LocalDate.of(2020,2,1)),
    STUDENTThree("Fyodor","Fedorov", Curriculums.TAQA_ENGINEER,LocalDate.of(2020,6,20)),
    STUDENTFour("Evgen","Evgenov", Curriculums.QA_ENGINEER, LocalDate.of(2020,7,10)),
    STUDENTFive("Oleg","Olegov", Curriculums.TAQA_ENGINEER,LocalDate.of(2020, 8, 12));

    private String name;

    private String lastname;

    private Curriculums curriculum;

    private LocalDate date;

    Students(String name, String lastname, Curriculums curriculum, LocalDate date) {
        this.name=name;
        this.lastname=lastname;
        this.curriculum=curriculum;
        this.date=date;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public Curriculums getCurriculum() {
        return curriculum;
    }

    public LocalDate getDate(){
        return date;
    }

    @Override
    public String toString(){
        return String.format("STUDENT: "+name+" "+lastname +
                "\nCURRICULUM: " + curriculum +
                "\nSTART DATE: " + date);
    }
}
