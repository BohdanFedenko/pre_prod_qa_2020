package com.epam.test.HomeWork2.task1;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Scanner;

public class ReportGenerator {
    /**Variables setting the length of the school day, hour and minutes of receiving the assignment*/

    public static final int lengthOfTheDay = 24;

    private int workingHoursOfDay;

    private int startHoursOfWorkingDay;

    private int startMinutesOfWorkingDay;

    public ReportGenerator(int workingHoursOfDay, int startHoursOfWorkingDay, int startMinutesOfWorkingDay){
        this.workingHoursOfDay=workingHoursOfDay;
        this.startHoursOfWorkingDay=startHoursOfWorkingDay;
        this.startMinutesOfWorkingDay=startMinutesOfWorkingDay;
    }

    /** Method for displaying information about students of Students enum*/
    public void printStudentsInfo(){
        for (Students st: Students.values()) {
            System.out.println(st.toString());
            System.out.println();
        }
    }

    /**Method offering the choice of a way to display a report on student progress
     *@return printShortReport() or printFullReport()*/
    public void printReport(){
        System.out.println("Please enter '0' for showing short report or any other letter for full report:\n");
        Scanner sc = new Scanner(System.in);
        String param=sc.next();
        if(param.equals("0")){
            System.out.println("Short report:\n");
            printShortReport();
        } else{
            System.out.println("Full report:\n");
            printFullReport();
        }
    }

    /**Method for displaying a short report on student progress*/
    public void printShortReport(){
        for (Students student: Students.values()) {
            System.out.println(student.getName()+" "+student.getLastname()
                    +" ("+student.getCurriculum()+")"
                    + getEducationStatus(getEndCurriculum(student),student));
        }
    }

    /**Method for displaying a full report on student progress*/
    public void printFullReport(){
        for (Students student: Students.values()) {
            System.out.println("FIO: "+student.getName()+" "+student.getLastname());
            System.out.println("Time spent: "+ checkCourseTimeSpent(student)+"h");
            System.out.println("CURRICULUM: "+student.getCurriculum());
            System.out.println("Course duration: "+student.getCurriculum().getDuration()+"h");
            System.out.println("START_DATE: "+student.getDate());
            System.out.println("END_DATE "+getEndCurriculum(student).toLocalDate());
            if(LocalDateTime.now().isAfter(getEndCurriculum(student))){
                System.out.println("Time after course finishing: "+getPastAfterCourseTime(getEndCurriculum(student)));
            }else{
                System.out.println("Time left: "+getLeftCourseTime(student));
            }
            System.out.println();
        }
    }

    /**Method for return an end date of curriculum (course finished) for any student of Students enum
     * @return LocalDateTime type */
    private  LocalDateTime getEndCurriculum(Students student){
        int days=student.getCurriculum().getDuration()/workingHoursOfDay;
        int lastHours=student.getCurriculum().getDuration()%workingHoursOfDay;
        LocalTime localTime=LocalTime.of(startHoursOfWorkingDay,startMinutesOfWorkingDay);
        LocalDateTime dateTime=LocalDateTime.of(student.getDate(),localTime);
        return dateTime=dateTime.plusDays(days).plusHours(lastHours);
    }

    /**Method for return a state of the educational process (course is in progress or completed)
     * and how many time left or spend for any student of Students enum
     *  @return String type */
    private String getEducationStatus(LocalDateTime endEducationDate,Students student){
        LocalDateTime dateTimeNow = LocalDateTime.now();
        if(dateTimeNow.isAfter(endEducationDate)){
            return " - Educations is complete. After the end has passed "+ getPastAfterCourseTime(endEducationDate);
        } else{
            return " - Education is not complete. Until the end is left "+ getLeftCourseTime(student);
        }
    }

    /**Method for return a spent time after curriculum(course) started to this moment
     *  @return int type */
    private int getCourseTimeSpent(Students student){
        LocalTime startTime=LocalTime.of(startHoursOfWorkingDay,startMinutesOfWorkingDay);
        LocalDateTime startDateTime=LocalDateTime.of(student.getDate(),startTime);
        Duration duration=Duration.between(LocalDateTime.now(),startDateTime);
        int daysPass = (int)Math.abs(duration.toDays());
        int hoursPass = (int)Math.abs(duration.toHours());
        int timeSpent=0;
        if(LocalDateTime.now().getHour()>=startHoursOfWorkingDay && LocalDateTime.now().getHour()<=18){
            timeSpent=daysPass*8+(hoursPass-daysPass*24);
        }else if(LocalDateTime.now().getHour()<startHoursOfWorkingDay){
            timeSpent=daysPass*workingHoursOfDay;
        }else if(LocalDateTime.now().getHour()>(startHoursOfWorkingDay+workingHoursOfDay)){
            timeSpent=daysPass*workingHoursOfDay+workingHoursOfDay;
        }
        return timeSpent;
    }

    /**The method checks if the course has finished before the current moment or not
     *  and accordingly returns the value of the time spent on the course.
     * @return student.getCurriculum().getDuration() - if the course is finished before this moment
     * or
     * @return  getCourseTimeSpent(student) - if the course is in progress now.
     * */
    private int checkCourseTimeSpent(Students student){
        LocalDateTime dateTimeNow = LocalDateTime.now();
        if(dateTimeNow.isAfter(getEndCurriculum(student))){
            return student.getCurriculum().getDuration();
        }else{
            return getCourseTimeSpent(student);
        }
    }

    /**Method for return a time to end of the curriculum(course) from this moment
     *  @return String type */
    private String getLeftCourseTime(Students student){
        int leftCurriculumTime=student.getCurriculum().getDuration();
        leftCurriculumTime=leftCurriculumTime- getCourseTimeSpent(student);
        int leftDays=leftCurriculumTime/workingHoursOfDay;
        int leftHours=leftCurriculumTime%workingHoursOfDay;
        return ""+leftDays+" d "+leftHours+" hrs. ";
    }

    /**Method for return a time spent after curriculum(course) finished to this moment
     *@return String type */
    private String getPastAfterCourseTime(LocalDateTime endEducationDate){
        Duration duration=Duration.between(LocalDateTime.now(),endEducationDate);
        long hours=Math.abs(duration.toHours());
        int days=(int)(hours/lengthOfTheDay);
        int lastHours=(int)hours-lengthOfTheDay*days;
        return " "+days+" d "+lastHours+" hrs.";
    }
}
