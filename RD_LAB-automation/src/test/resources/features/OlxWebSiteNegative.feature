@negative
Feature: Olx web site tests. Negative test

  Scenario: Searching with incorrect city input
    Given I navigate to olx
    When enter "Очки" and "City" to search fields and submit
    Then error message under city field is displayed

  Scenario: Sending message to seller with empty email field
    Given I navigate to olx product list page
    And open one of the all pages
    When send message to seller with empty email field
    Then error message below email field is displayed

  Scenario: Sending message to seller with empty text field
    Given I navigate to olx product list page
    And open one of the all pages
    When send message to seller with empty email field
    Then error message below text field is displayed

  Scenario: Login with empty fields
    Given I navigate to olx
    And open Мой профиль
    When click submit button
    Then error message is displayed below both fields

  Scenario: Make empty complain
    Given I navigate to olx product list page
    And open one of the all pages
    When make empty complain
    Then shown error message



