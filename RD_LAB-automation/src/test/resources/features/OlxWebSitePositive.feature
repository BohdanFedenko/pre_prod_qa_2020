@positive
Feature: Olx web site tests. Positive test

  Scenario: Open olx home page
    Given I navigate to olx
    Then opened page is olx

  Scenario: Switch lang of olx web site
    Given I navigate to olx
    When changing lang of web site
    Then web site lang is changed
    And switch lang to default

  Scenario: Searching without city input
    Given I navigate to olx
    When entering "солнцезащитные очки" to search field and submit
    Then results are displayed on the screen

  Scenario: Searching with city input
    Given I navigate to olx
    When entering "Очки" and "Днепр" to search fields and submit
    Then results are displayed on the screen

  Scenario: Open product page from product list
    Given I navigate to olx
    And enter "Очки" to search field and submit
    When open one of the all pages
    Then product page is opened

  Scenario: Open seller page from product page
    Given I navigate to olx product list page
    When open one of the all pages
    And open seller page
    Then seller page is opened

  Scenario: Searching from search result page
    Given I navigate to olx product list page
    When I entering "наушники" to search field of page and submit
    Then result search page is opened

  Scenario: Opening products list page by offer type
    Given I navigate to olx product list page
    And open one of the all pages
    When open products by offer type
    Then product list page is opened

  Scenario: Open products list page from category
    Given I navigate to olx
    When I open products list page use "Обмен" category
    Then product list page is opened

  Scenario: Put offer without login
    Given I navigate to olx
    When I open put offer page
    Then registration page is opened




