Feature: Olx web site tests

  Scenario: Find olx in google
    Given I navigate to google
    And enter "olx" to search field
    When I open fist item of the result list
    Then olx home page is opened

  Scenario: Searching in olx
    Given I navigate to olx
    When I enter "наушники" to search field and submit
    Then result is displayed

  Scenario: Open registration form while putting offer without login
    Given I navigate to olx
    When I click put offer button
    Then registration page is opened

  Scenario: Switch language of olx web site
    Given I navigate to olx
    When I changing language of web site
    Then web site language is changed
    And switch language to default

  Scenario: Open wish list page
    Given I navigate to olx
    When I clicking on heart icon of the header
    Then wish list page is opened

  Scenario: Log in with empty email
    Given I navigate to olx
    And open Мой профиль page
    When I clicking enter button of the displayed form
    Then error message about incorrect email is displayed

  Scenario: Add to wish list
    Given I navigate to olx
    And search "nokia"
    When I clicking add to wish list icon of the one of the result list
    Then message about successful operation is displayed

  Scenario: Open product list page
    Given I navigate to product list page
    Then product list page is opened
	


	