
package com.openweathermap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Pressure
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "min",
    "max",
    "average",
    "weight"
})
public class Pressure implements Serializable
{

    /**
     * Min
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("min")
    private Integer min = 0;
    /**
     * Max
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("max")
    private Integer max = 0;
    /**
     * Average
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("average")
    private Double average = 0.0D;
    /**
     * Weight
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("weight")
    private Integer weight = 0;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -8923595386571982901L;

    /**
     * Min
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("min")
    public Integer getMin() {
        return min;
    }

    /**
     * Min
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("min")
    public void setMin(Integer min) {
        this.min = min;
    }

    public Pressure withMin(Integer min) {
        this.min = min;
        return this;
    }

    /**
     * Max
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("max")
    public Integer getMax() {
        return max;
    }

    /**
     * Max
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("max")
    public void setMax(Integer max) {
        this.max = max;
    }

    public Pressure withMax(Integer max) {
        this.max = max;
        return this;
    }

    /**
     * Average
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("average")
    public Double getAverage() {
        return average;
    }

    /**
     * Average
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("average")
    public void setAverage(Double average) {
        this.average = average;
    }

    public Pressure withAverage(Double average) {
        this.average = average;
        return this;
    }

    /**
     * Weight
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("weight")
    public Integer getWeight() {
        return weight;
    }

    /**
     * Weight
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("weight")
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Pressure withWeight(Integer weight) {
        this.weight = weight;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Pressure withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Pressure.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("min");
        sb.append('=');
        sb.append(((this.min == null)?"<null>":this.min));
        sb.append(',');
        sb.append("max");
        sb.append('=');
        sb.append(((this.max == null)?"<null>":this.max));
        sb.append(',');
        sb.append("average");
        sb.append('=');
        sb.append(((this.average == null)?"<null>":this.average));
        sb.append(',');
        sb.append("weight");
        sb.append('=');
        sb.append(((this.weight == null)?"<null>":this.weight));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.average == null)? 0 :this.average.hashCode()));
        result = ((result* 31)+((this.weight == null)? 0 :this.weight.hashCode()));
        result = ((result* 31)+((this.min == null)? 0 :this.min.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.max == null)? 0 :this.max.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Pressure) == false) {
            return false;
        }
        Pressure rhs = ((Pressure) other);
        return ((((((this.average == rhs.average)||((this.average!= null)&&this.average.equals(rhs.average)))&&((this.weight == rhs.weight)||((this.weight!= null)&&this.weight.equals(rhs.weight))))&&((this.min == rhs.min)||((this.min!= null)&&this.min.equals(rhs.min))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.max == rhs.max)||((this.max!= null)&&this.max.equals(rhs.max))));
    }

}
