
package com.openweathermap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Items
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "station_id",
    "dt",
    "temperature",
    "wind_speed",
    "wind_gust",
    "pressure",
    "humidity",
    "rain_1h",
    "clouds"
})
public class POSTMeasurementsRequestSchema implements Serializable
{

    /**
     * Station_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("station_id")
    private String stationId = "";
    /**
     * Dt
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("dt")
    private Integer dt = 0;
    /**
     * Temperature
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("temperature")
    private Double temperature = 0.0D;
    /**
     * Wind_speed
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("wind_speed")
    private Double windSpeed = 0.0D;
    /**
     * Wind_gust
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("wind_gust")
    private Double windGust = 0.0D;
    /**
     * Pressure
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("pressure")
    private Integer pressure = 0;
    /**
     * Humidity
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("humidity")
    private Integer humidity = 0;
    /**
     * Rain_1h
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("rain_1h")
    private Integer rain1h = 0;
    /**
     * Clouds
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("clouds")
    private List<Cloud> clouds = new ArrayList<Cloud>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -3083401851929265020L;

    /**
     * Station_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("station_id")
    public String getStationId() {
        return stationId;
    }

    /**
     * Station_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("station_id")
    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public POSTMeasurementsRequestSchema withStationId(String stationId) {
        this.stationId = stationId;
        return this;
    }

    /**
     * Dt
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("dt")
    public Integer getDt() {
        return dt;
    }

    /**
     * Dt
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("dt")
    public void setDt(Integer dt) {
        this.dt = dt;
    }

    public POSTMeasurementsRequestSchema withDt(Integer dt) {
        this.dt = dt;
        return this;
    }

    /**
     * Temperature
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("temperature")
    public Double getTemperature() {
        return temperature;
    }

    /**
     * Temperature
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("temperature")
    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public POSTMeasurementsRequestSchema withTemperature(Double temperature) {
        this.temperature = temperature;
        return this;
    }

    /**
     * Wind_speed
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("wind_speed")
    public Double getWindSpeed() {
        return windSpeed;
    }

    /**
     * Wind_speed
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("wind_speed")
    public void setWindSpeed(Double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public POSTMeasurementsRequestSchema withWindSpeed(Double windSpeed) {
        this.windSpeed = windSpeed;
        return this;
    }

    /**
     * Wind_gust
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("wind_gust")
    public Double getWindGust() {
        return windGust;
    }

    /**
     * Wind_gust
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("wind_gust")
    public void setWindGust(Double windGust) {
        this.windGust = windGust;
    }

    public POSTMeasurementsRequestSchema withWindGust(Double windGust) {
        this.windGust = windGust;
        return this;
    }

    /**
     * Pressure
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("pressure")
    public Integer getPressure() {
        return pressure;
    }

    /**
     * Pressure
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("pressure")
    public void setPressure(Integer pressure) {
        this.pressure = pressure;
    }

    public POSTMeasurementsRequestSchema withPressure(Integer pressure) {
        this.pressure = pressure;
        return this;
    }

    /**
     * Humidity
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("humidity")
    public Integer getHumidity() {
        return humidity;
    }

    /**
     * Humidity
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("humidity")
    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    public POSTMeasurementsRequestSchema withHumidity(Integer humidity) {
        this.humidity = humidity;
        return this;
    }

    /**
     * Rain_1h
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("rain_1h")
    public Integer getRain1h() {
        return rain1h;
    }

    /**
     * Rain_1h
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("rain_1h")
    public void setRain1h(Integer rain1h) {
        this.rain1h = rain1h;
    }

    public POSTMeasurementsRequestSchema withRain1h(Integer rain1h) {
        this.rain1h = rain1h;
        return this;
    }

    /**
     * Clouds
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("clouds")
    public List<Cloud> getClouds() {
        return clouds;
    }

    /**
     * Clouds
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("clouds")
    public void setClouds(List<Cloud> clouds) {
        this.clouds = clouds;
    }

    public POSTMeasurementsRequestSchema withClouds(List<Cloud> clouds) {
        this.clouds = clouds;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public POSTMeasurementsRequestSchema withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(POSTMeasurementsRequestSchema.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("stationId");
        sb.append('=');
        sb.append(((this.stationId == null)?"<null>":this.stationId));
        sb.append(',');
        sb.append("dt");
        sb.append('=');
        sb.append(((this.dt == null)?"<null>":this.dt));
        sb.append(',');
        sb.append("temperature");
        sb.append('=');
        sb.append(((this.temperature == null)?"<null>":this.temperature));
        sb.append(',');
        sb.append("windSpeed");
        sb.append('=');
        sb.append(((this.windSpeed == null)?"<null>":this.windSpeed));
        sb.append(',');
        sb.append("windGust");
        sb.append('=');
        sb.append(((this.windGust == null)?"<null>":this.windGust));
        sb.append(',');
        sb.append("pressure");
        sb.append('=');
        sb.append(((this.pressure == null)?"<null>":this.pressure));
        sb.append(',');
        sb.append("humidity");
        sb.append('=');
        sb.append(((this.humidity == null)?"<null>":this.humidity));
        sb.append(',');
        sb.append("rain1h");
        sb.append('=');
        sb.append(((this.rain1h == null)?"<null>":this.rain1h));
        sb.append(',');
        sb.append("clouds");
        sb.append('=');
        sb.append(((this.clouds == null)?"<null>":this.clouds));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.dt == null)? 0 :this.dt.hashCode()));
        result = ((result* 31)+((this.windGust == null)? 0 :this.windGust.hashCode()));
        result = ((result* 31)+((this.temperature == null)? 0 :this.temperature.hashCode()));
        result = ((result* 31)+((this.humidity == null)? 0 :this.humidity.hashCode()));
        result = ((result* 31)+((this.pressure == null)? 0 :this.pressure.hashCode()));
        result = ((result* 31)+((this.rain1h == null)? 0 :this.rain1h.hashCode()));
        result = ((result* 31)+((this.clouds == null)? 0 :this.clouds.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.windSpeed == null)? 0 :this.windSpeed.hashCode()));
        result = ((result* 31)+((this.stationId == null)? 0 :this.stationId.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof POSTMeasurementsRequestSchema) == false) {
            return false;
        }
        POSTMeasurementsRequestSchema rhs = ((POSTMeasurementsRequestSchema) other);
        return (((((((((((this.dt == rhs.dt)||((this.dt!= null)&&this.dt.equals(rhs.dt)))&&((this.windGust == rhs.windGust)||((this.windGust!= null)&&this.windGust.equals(rhs.windGust))))&&((this.temperature == rhs.temperature)||((this.temperature!= null)&&this.temperature.equals(rhs.temperature))))&&((this.humidity == rhs.humidity)||((this.humidity!= null)&&this.humidity.equals(rhs.humidity))))&&((this.pressure == rhs.pressure)||((this.pressure!= null)&&this.pressure.equals(rhs.pressure))))&&((this.rain1h == rhs.rain1h)||((this.rain1h!= null)&&this.rain1h.equals(rhs.rain1h))))&&((this.clouds == rhs.clouds)||((this.clouds!= null)&&this.clouds.equals(rhs.clouds))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.windSpeed == rhs.windSpeed)||((this.windSpeed!= null)&&this.windSpeed.equals(rhs.windSpeed))))&&((this.stationId == rhs.stationId)||((this.stationId!= null)&&this.stationId.equals(rhs.stationId))));
    }

}
