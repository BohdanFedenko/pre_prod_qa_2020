
package com.openweathermap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Root
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "external_id",
    "name",
    "latitude",
    "longitude",
    "altitude"
})
public class POSTStationsRequestSchema implements Serializable
{

    /**
     * External_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_id")
    private String externalId = "";
    /**
     * Name
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    private String name = "";
    /**
     * Latitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("latitude")
    private Double latitude = 0.0D;
    /**
     * Longitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("longitude")
    private Double longitude = 0.0D;
    /**
     * Altitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("altitude")
    private Integer altitude = 0;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -4370176298490956052L;

    /**
     * External_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_id")
    public String getExternalId() {
        return externalId;
    }

    /**
     * External_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_id")
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public POSTStationsRequestSchema withExternalId(String externalId) {
        this.externalId = externalId;
        return this;
    }

    /**
     * Name
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * Name
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public POSTStationsRequestSchema withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Latitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("latitude")
    public Double getLatitude() {
        return latitude;
    }

    /**
     * Latitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("latitude")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public POSTStationsRequestSchema withLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    /**
     * Longitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("longitude")
    public Double getLongitude() {
        return longitude;
    }

    /**
     * Longitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("longitude")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public POSTStationsRequestSchema withLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    /**
     * Altitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("altitude")
    public Integer getAltitude() {
        return altitude;
    }

    /**
     * Altitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("altitude")
    public void setAltitude(Integer altitude) {
        this.altitude = altitude;
    }

    public POSTStationsRequestSchema withAltitude(Integer altitude) {
        this.altitude = altitude;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public POSTStationsRequestSchema withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(POSTStationsRequestSchema.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("externalId");
        sb.append('=');
        sb.append(((this.externalId == null)?"<null>":this.externalId));
        sb.append(',');
        sb.append("name");
        sb.append('=');
        sb.append(((this.name == null)?"<null>":this.name));
        sb.append(',');
        sb.append("latitude");
        sb.append('=');
        sb.append(((this.latitude == null)?"<null>":this.latitude));
        sb.append(',');
        sb.append("longitude");
        sb.append('=');
        sb.append(((this.longitude == null)?"<null>":this.longitude));
        sb.append(',');
        sb.append("altitude");
        sb.append('=');
        sb.append(((this.altitude == null)?"<null>":this.altitude));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.altitude == null)? 0 :this.altitude.hashCode()));
        result = ((result* 31)+((this.latitude == null)? 0 :this.latitude.hashCode()));
        result = ((result* 31)+((this.name == null)? 0 :this.name.hashCode()));
        result = ((result* 31)+((this.externalId == null)? 0 :this.externalId.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.longitude == null)? 0 :this.longitude.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof POSTStationsRequestSchema) == false) {
            return false;
        }
        POSTStationsRequestSchema rhs = ((POSTStationsRequestSchema) other);
        return (((((((this.altitude == rhs.altitude)||((this.altitude!= null)&&this.altitude.equals(rhs.altitude)))&&((this.latitude == rhs.latitude)||((this.latitude!= null)&&this.latitude.equals(rhs.latitude))))&&((this.name == rhs.name)||((this.name!= null)&&this.name.equals(rhs.name))))&&((this.externalId == rhs.externalId)||((this.externalId!= null)&&this.externalId.equals(rhs.externalId))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.longitude == rhs.longitude)||((this.longitude!= null)&&this.longitude.equals(rhs.longitude))));
    }

}
