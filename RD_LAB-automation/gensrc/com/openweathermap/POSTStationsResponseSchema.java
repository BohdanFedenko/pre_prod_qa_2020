
package com.openweathermap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Root
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ID",
    "updated_at",
    "created_at",
    "user_id",
    "external_id",
    "name",
    "latitude",
    "longitude",
    "altitude",
    "source_type"
})
public class POSTStationsResponseSchema implements Serializable
{

    /**
     * Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("ID")
    private String id = "";
    /**
     * Updated_at
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("updated_at")
    private String updatedAt = "";
    /**
     * Created_at
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("created_at")
    private String createdAt = "";
    /**
     * User_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("user_id")
    private String userId = "";
    /**
     * External_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_id")
    private String externalId = "";
    /**
     * Name
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    private String name = "";
    /**
     * Latitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("latitude")
    private Double latitude = 0.0D;
    /**
     * Longitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("longitude")
    private Double longitude = 0.0D;
    /**
     * Altitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("altitude")
    private Integer altitude = 0;
    /**
     * Source_type
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("source_type")
    private Integer sourceType = 0;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 3212793018140658448L;

    /**
     * Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("ID")
    public String getId() {
        return id;
    }

    /**
     * Id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("ID")
    public void setId(String id) {
        this.id = id;
    }

    public POSTStationsResponseSchema withId(String id) {
        this.id = id;
        return this;
    }

    /**
     * Updated_at
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Updated_at
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public POSTStationsResponseSchema withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
     * Created_at
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * Created_at
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public POSTStationsResponseSchema withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
     * User_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("user_id")
    public String getUserId() {
        return userId;
    }

    /**
     * User_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("user_id")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public POSTStationsResponseSchema withUserId(String userId) {
        this.userId = userId;
        return this;
    }

    /**
     * External_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_id")
    public String getExternalId() {
        return externalId;
    }

    /**
     * External_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_id")
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public POSTStationsResponseSchema withExternalId(String externalId) {
        this.externalId = externalId;
        return this;
    }

    /**
     * Name
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * Name
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public POSTStationsResponseSchema withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Latitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("latitude")
    public Double getLatitude() {
        return latitude;
    }

    /**
     * Latitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("latitude")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public POSTStationsResponseSchema withLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    /**
     * Longitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("longitude")
    public Double getLongitude() {
        return longitude;
    }

    /**
     * Longitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("longitude")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public POSTStationsResponseSchema withLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    /**
     * Altitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("altitude")
    public Integer getAltitude() {
        return altitude;
    }

    /**
     * Altitude
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("altitude")
    public void setAltitude(Integer altitude) {
        this.altitude = altitude;
    }

    public POSTStationsResponseSchema withAltitude(Integer altitude) {
        this.altitude = altitude;
        return this;
    }

    /**
     * Source_type
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("source_type")
    public Integer getSourceType() {
        return sourceType;
    }

    /**
     * Source_type
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("source_type")
    public void setSourceType(Integer sourceType) {
        this.sourceType = sourceType;
    }

    public POSTStationsResponseSchema withSourceType(Integer sourceType) {
        this.sourceType = sourceType;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public POSTStationsResponseSchema withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(POSTStationsResponseSchema.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null)?"<null>":this.id));
        sb.append(',');
        sb.append("updatedAt");
        sb.append('=');
        sb.append(((this.updatedAt == null)?"<null>":this.updatedAt));
        sb.append(',');
        sb.append("createdAt");
        sb.append('=');
        sb.append(((this.createdAt == null)?"<null>":this.createdAt));
        sb.append(',');
        sb.append("userId");
        sb.append('=');
        sb.append(((this.userId == null)?"<null>":this.userId));
        sb.append(',');
        sb.append("externalId");
        sb.append('=');
        sb.append(((this.externalId == null)?"<null>":this.externalId));
        sb.append(',');
        sb.append("name");
        sb.append('=');
        sb.append(((this.name == null)?"<null>":this.name));
        sb.append(',');
        sb.append("latitude");
        sb.append('=');
        sb.append(((this.latitude == null)?"<null>":this.latitude));
        sb.append(',');
        sb.append("longitude");
        sb.append('=');
        sb.append(((this.longitude == null)?"<null>":this.longitude));
        sb.append(',');
        sb.append("altitude");
        sb.append('=');
        sb.append(((this.altitude == null)?"<null>":this.altitude));
        sb.append(',');
        sb.append("sourceType");
        sb.append('=');
        sb.append(((this.sourceType == null)?"<null>":this.sourceType));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.createdAt == null)? 0 :this.createdAt.hashCode()));
        result = ((result* 31)+((this.altitude == null)? 0 :this.altitude.hashCode()));
        result = ((result* 31)+((this.sourceType == null)? 0 :this.sourceType.hashCode()));
        result = ((result* 31)+((this.latitude == null)? 0 :this.latitude.hashCode()));
        result = ((result* 31)+((this.name == null)? 0 :this.name.hashCode()));
        result = ((result* 31)+((this.externalId == null)? 0 :this.externalId.hashCode()));
        result = ((result* 31)+((this.id == null)? 0 :this.id.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.userId == null)? 0 :this.userId.hashCode()));
        result = ((result* 31)+((this.updatedAt == null)? 0 :this.updatedAt.hashCode()));
        result = ((result* 31)+((this.longitude == null)? 0 :this.longitude.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof POSTStationsResponseSchema) == false) {
            return false;
        }
        POSTStationsResponseSchema rhs = ((POSTStationsResponseSchema) other);
        return ((((((((((((this.createdAt == rhs.createdAt)||((this.createdAt!= null)&&this.createdAt.equals(rhs.createdAt)))&&((this.altitude == rhs.altitude)||((this.altitude!= null)&&this.altitude.equals(rhs.altitude))))&&((this.sourceType == rhs.sourceType)||((this.sourceType!= null)&&this.sourceType.equals(rhs.sourceType))))&&((this.latitude == rhs.latitude)||((this.latitude!= null)&&this.latitude.equals(rhs.latitude))))&&((this.name == rhs.name)||((this.name!= null)&&this.name.equals(rhs.name))))&&((this.externalId == rhs.externalId)||((this.externalId!= null)&&this.externalId.equals(rhs.externalId))))&&((this.id == rhs.id)||((this.id!= null)&&this.id.equals(rhs.id))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.userId == rhs.userId)||((this.userId!= null)&&this.userId.equals(rhs.userId))))&&((this.updatedAt == rhs.updatedAt)||((this.updatedAt!= null)&&this.updatedAt.equals(rhs.updatedAt))))&&((this.longitude == rhs.longitude)||((this.longitude!= null)&&this.longitude.equals(rhs.longitude))));
    }

}
