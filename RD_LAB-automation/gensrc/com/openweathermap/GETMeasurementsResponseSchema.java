
package com.openweathermap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Items
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "date",
    "station_id",
    "temp",
    "humidity",
    "wind",
    "pressure",
    "precipitation"
})
public class GETMeasurementsResponseSchema implements Serializable
{

    /**
     * Type
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    private String type = "";
    /**
     * Date
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("date")
    private Integer date = 0;
    /**
     * Station_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("station_id")
    private String stationId = "";
    /**
     * Temp
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("temp")
    private Temp temp;
    /**
     * Humidity
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("humidity")
    private Humidity humidity;
    /**
     * Wind
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("wind")
    private Wind wind;
    /**
     * Pressure
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("pressure")
    private Pressure pressure;
    /**
     * Precipitation
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("precipitation")
    private Precipitation precipitation;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 2123873001799133056L;

    /**
     * Type
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * Type
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public GETMeasurementsResponseSchema withType(String type) {
        this.type = type;
        return this;
    }

    /**
     * Date
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("date")
    public Integer getDate() {
        return date;
    }

    /**
     * Date
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("date")
    public void setDate(Integer date) {
        this.date = date;
    }

    public GETMeasurementsResponseSchema withDate(Integer date) {
        this.date = date;
        return this;
    }

    /**
     * Station_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("station_id")
    public String getStationId() {
        return stationId;
    }

    /**
     * Station_id
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("station_id")
    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public GETMeasurementsResponseSchema withStationId(String stationId) {
        this.stationId = stationId;
        return this;
    }

    /**
     * Temp
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("temp")
    public Temp getTemp() {
        return temp;
    }

    /**
     * Temp
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("temp")
    public void setTemp(Temp temp) {
        this.temp = temp;
    }

    public GETMeasurementsResponseSchema withTemp(Temp temp) {
        this.temp = temp;
        return this;
    }

    /**
     * Humidity
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("humidity")
    public Humidity getHumidity() {
        return humidity;
    }

    /**
     * Humidity
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("humidity")
    public void setHumidity(Humidity humidity) {
        this.humidity = humidity;
    }

    public GETMeasurementsResponseSchema withHumidity(Humidity humidity) {
        this.humidity = humidity;
        return this;
    }

    /**
     * Wind
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("wind")
    public Wind getWind() {
        return wind;
    }

    /**
     * Wind
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("wind")
    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public GETMeasurementsResponseSchema withWind(Wind wind) {
        this.wind = wind;
        return this;
    }

    /**
     * Pressure
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("pressure")
    public Pressure getPressure() {
        return pressure;
    }

    /**
     * Pressure
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("pressure")
    public void setPressure(Pressure pressure) {
        this.pressure = pressure;
    }

    public GETMeasurementsResponseSchema withPressure(Pressure pressure) {
        this.pressure = pressure;
        return this;
    }

    /**
     * Precipitation
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("precipitation")
    public Precipitation getPrecipitation() {
        return precipitation;
    }

    /**
     * Precipitation
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("precipitation")
    public void setPrecipitation(Precipitation precipitation) {
        this.precipitation = precipitation;
    }

    public GETMeasurementsResponseSchema withPrecipitation(Precipitation precipitation) {
        this.precipitation = precipitation;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public GETMeasurementsResponseSchema withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(GETMeasurementsResponseSchema.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("type");
        sb.append('=');
        sb.append(((this.type == null)?"<null>":this.type));
        sb.append(',');
        sb.append("date");
        sb.append('=');
        sb.append(((this.date == null)?"<null>":this.date));
        sb.append(',');
        sb.append("stationId");
        sb.append('=');
        sb.append(((this.stationId == null)?"<null>":this.stationId));
        sb.append(',');
        sb.append("temp");
        sb.append('=');
        sb.append(((this.temp == null)?"<null>":this.temp));
        sb.append(',');
        sb.append("humidity");
        sb.append('=');
        sb.append(((this.humidity == null)?"<null>":this.humidity));
        sb.append(',');
        sb.append("wind");
        sb.append('=');
        sb.append(((this.wind == null)?"<null>":this.wind));
        sb.append(',');
        sb.append("pressure");
        sb.append('=');
        sb.append(((this.pressure == null)?"<null>":this.pressure));
        sb.append(',');
        sb.append("precipitation");
        sb.append('=');
        sb.append(((this.precipitation == null)?"<null>":this.precipitation));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.date == null)? 0 :this.date.hashCode()));
        result = ((result* 31)+((this.precipitation == null)? 0 :this.precipitation.hashCode()));
        result = ((result* 31)+((this.temp == null)? 0 :this.temp.hashCode()));
        result = ((result* 31)+((this.humidity == null)? 0 :this.humidity.hashCode()));
        result = ((result* 31)+((this.pressure == null)? 0 :this.pressure.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.type == null)? 0 :this.type.hashCode()));
        result = ((result* 31)+((this.stationId == null)? 0 :this.stationId.hashCode()));
        result = ((result* 31)+((this.wind == null)? 0 :this.wind.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GETMeasurementsResponseSchema) == false) {
            return false;
        }
        GETMeasurementsResponseSchema rhs = ((GETMeasurementsResponseSchema) other);
        return ((((((((((this.date == rhs.date)||((this.date!= null)&&this.date.equals(rhs.date)))&&((this.precipitation == rhs.precipitation)||((this.precipitation!= null)&&this.precipitation.equals(rhs.precipitation))))&&((this.temp == rhs.temp)||((this.temp!= null)&&this.temp.equals(rhs.temp))))&&((this.humidity == rhs.humidity)||((this.humidity!= null)&&this.humidity.equals(rhs.humidity))))&&((this.pressure == rhs.pressure)||((this.pressure!= null)&&this.pressure.equals(rhs.pressure))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.type == rhs.type)||((this.type!= null)&&this.type.equals(rhs.type))))&&((this.stationId == rhs.stationId)||((this.stationId!= null)&&this.stationId.equals(rhs.stationId))))&&((this.wind == rhs.wind)||((this.wind!= null)&&this.wind.equals(rhs.wind))));
    }

}
